import torchaudio
import torch
import transforms
import loader

transforms.configure_logger(True, 'debug')
loader.configure_logger(True, 'debug', 'loader.log', 'all')

seed_offset = 0
dataset = loader.musdb18hq.Dataset("musdb18hq/train", 96000, seed_offset=seed_offset, no_distort=False)
dataloader = loader.musdb18hq.DataLoader(dataset=dataset, batch_size=5)

for epoch in range(1):
    batches = [batch for batch in dataloader]
    a = torch.stack([batch.a for batch in batches]).view(20 * 5 * 96000)
    b = torch.stack([batch.b for batch in batches]).view(20 * 5 * 96000)
    c = torch.stack([batch.c for batch in batches]).view(20 * 5 * 96000)
    m = torch.stack((a, b, c))
    torchaudio.save("musdb18hq/augmentations/test_offset_%03d_epoch_%05d_distorted.flac" % (seed_offset, epoch), m, 48000)

dataset = loader.musdb18hq.Dataset("musdb18hq/train", 96000, seed_offset=seed_offset, no_distort=True)
dataloader = loader.musdb18hq.DataLoader(dataset=dataset, batch_size=5)

for epoch in range(1):
    batches = [batch for batch in dataloader]
    a = torch.stack([batch.a for batch in batches]).view(20 * 5 * 96000)
    b = torch.stack([batch.b for batch in batches]).view(20 * 5 * 96000)
    c = torch.stack([batch.c for batch in batches]).view(20 * 5 * 96000)
    m = torch.stack((a, b, c))
    torchaudio.save("musdb18hq/augmentations/test_offset_%03d_epoch_%05d.flac" % (seed_offset, epoch), m, 48000)
