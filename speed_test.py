from models.vae1 import AETrainer as Trainer

from functorch.compile import memory_efficient_fusion
from torch.nn import MSELoss
from torch.optim import SGD, Adam

import torch
import time

device = torch.device('cuda', 0)
model = Trainer('tester', checkpointing=False,
                loss_args={'a': 0.6, 'b': 0.4},
                optim_class=Adam, optim_args={'lr': 1e-3}
                ).to(device)
optimizer = model.configure_optimizers()
loss = model.loss

# model.encoder = memory_efficient_fusion(model.encoder)
# model.decoder = memory_efficient_fusion(model.decoder)

# eager-nocheckpointing:
#   Elapsed time: 153.5 ms
#   GPU util: 99%, mem bw: 38%
#   Peak memory usage: 1302.70 MB
#   Memory allocated before forward: 183.72 MB
#   Memory allocated after backward: 211.18 MB

# eager-checkpointing:
#   Elapsed time: 187.20 ms
#   GPU util: 99%, mem bw: 39%
#   Peak memory usage: 448.75 MB
#   Memory allocated before forward: 183.18 MB
#   Memory allocated after backward: 209.61 MB

# inductor-triton:
#   Step time: ~140 ms
#   GPU util: 99%, mem bw: 35%
#   Peak memory usage: 1263.74 MB
#   Memory allocated before forward: 183.06 MB
#   Memory allocated after backward: 209.02 MB

# inductor-triton+loss:
#   Step time: ~140 ms
#   GPU util: 99%, mem bw: 34%
#   Peak memory usage: 1218.07 MB
#   Memory allocated before forward: 183.06 MB
#   Memory allocated after backward: 209.22 MB


model.encoder = torch.compile(model.encoder, backend="inductor")
model.decoder = torch.compile(model.decoder, backend="inductor")
loss = torch.compile(loss, backend="inductor")

for i in range(333):
    if i < 10:
        torch.cuda.empty_cache()

    x = torch.rand((15, 1, 16*1024), device=device)
    # z = torch.rand((20, 256, 8), device=device)

    # Create two CUDA events to measure time
    start_event = torch.cuda.Event(enable_timing=True)
    end_event = torch.cuda.Event(enable_timing=True)
    # Record the start event
    start_event.record()
    start_memory = torch.cuda.memory_allocated()

    # Work to be measured.
    z = model.encoder(x)
    z_shape = z.shape
    y = model.decoder(z)
    del z
    # y = model(x)
    l, _ = loss(y, x)
    l.backward()

    end_memory = torch.cuda.memory_allocated()
    optimizer.step()
    model.zero_grad(set_to_none=True)
    optimizer.zero_grad(set_to_none=True)

    # Record the end event
    end_event.record()
    # Wait for the events to complete
    torch.cuda.synchronize()
    # calculate the elapsed time in milliseconds
    elapsed_time_ms = start_event.elapsed_time(end_event)

    print("Latent space shape:", z_shape)
    print(f"Loss: {l.item()}")
    print(f"Elapsed time: {elapsed_time_ms:.2f} ms")
    print("Peak memory usage: "
          f"{torch.cuda.max_memory_allocated() / 1024**2:.2f} MB")
    print("Memory allocated before forward: "
          f"{start_memory / 1024**2:.2f} MB")
    print("Memory allocated after backward: "
          f"{end_memory / 1024**2:.2f} MB")
