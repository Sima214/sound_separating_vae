// Benchmark function
template<typename DST, typename SRC>
static void BM_scale_cast_map_values(benchmark::State& state) {
    // Allocate memory for input and output
    std::vector<SRC> src(state.range(0));
    std::vector<DST> dst(state.range(0));
    
    // Fill input with random values outside the benchmark timer
    std::srand(std::time(nullptr));
    for (int64_t i = 0; i < state.range(0); ++i) {
        src[i] = static_cast<SRC>(std::rand());
    }
    
    // Benchmark loop
    for (auto _ : state) {
        __scale_cast_map_values(dst.data(), src.data(), state.range(0));
    }
    
    // Set number of elements processed in this run
    state.SetItemsProcessed(state.iterations() * state.range(0));
}

// Use the BENCHMARK_TEMPLATE macro to create benchmarks for all combinations of template parameters.
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, uint8_t, int8_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, uint8_t, int16_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, uint8_t, int)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, uint8_t, int64_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int8_t, uint8_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int8_t, int16_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int8_t, int)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int8_t, int64_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int16_t, uint8_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int16_t, int8_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int16_t, int)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int16_t, int64_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int, uint8_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int, int8_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int, int16_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int, int64_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int64_t, uint8_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int64_t, int8_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int64_t, int16_t)->Range(8, 524288);
BENCHMARK_TEMPLATE(BM_scale_cast_map_values, int64_t, int)->Range(8, 524288);

BENCHMARK_MAIN();
