#ifndef SPEU_MAPPEDWAV_HPP
#define SPEU_MAPPEDWAV_HPP
/**
 * @file
 * @brief mmap()ing wav files.
 */

#include <FileUtils.hpp>
#include <Utils.hpp>

#include <cstddef>
#include <cstdint>
#include <filesystem>
#include <limits>

#include <ATen/Tensor.h>

namespace speu {

class MappedWav : public spec::INonCopyable {
   protected:
    spec::MemoryMapping _hnd;

   public:
    MappedWav(const std::filesystem::path& path, bool lock_header = true,
              bool sequential = false);
    MappedWav(MappedWav&& o) : _hnd(std::move(o._hnd)) {}
    MappedWav& operator=(MappedWav&& o) {
        _hnd = std::move(o._hnd);
        return *this;
    }
    ~MappedWav();

    /** Check for PCM format. */
    bool is_integer();
    /** Check for IEEE PCM format. */
    bool is_float();
    /** Get the count of significant bits per sample. */
    size_t get_bit_width();
    /** Count of interleaved audio channels. */
    size_t get_channel_count();
    /** Get audio sampling rate. */
    size_t get_sampling_rate();
    /** Get sample count, which is related to duration. */
    size_t get_sample_count();

    /**
     * Parse and verify WAVE header information.
     */
    bool validate();

    /**
     * Convert mapped file to pytorch tensor storage and create a tensor to
     * access the data.
     */
    at::Tensor to_torch(uint32_t start = 0,
                        uint32_t end = std::numeric_limits<uint32_t>::max());
};

class MappedWavTensor : public MappedWav {
   protected:
    at::Tensor _torch;

   public:
    MappedWavTensor(const std::filesystem::path& path, bool lock_header = true,
                    bool sequential = true) :
        MappedWav(path, lock_header, sequential) {
        _torch = to_torch();
    }
    MappedWavTensor(MappedWavTensor&& o) :
        MappedWav(std::forward<MappedWav>(o)), _torch(std::move(o._torch)) {}
    MappedWavTensor& operator=(MappedWavTensor&& o) {
        MappedWav::operator=(std::forward<MappedWav>(o));
        _torch = std::move(o._torch);
        return *this;
    }

    at::Tensor get_tensor() const {
        return _torch;
    }
    operator at::Tensor() const {
        return get_tensor();
    }
};

}  // namespace speu

#endif /*SPEU_MAPPEDWAV_HPP*/
