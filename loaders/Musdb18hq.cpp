#include "Musdb18hq.hpp"

#include <Audio.hpp>
#include <General.hpp>
#include <logger/Logger.hpp>

#include <algorithm>
#include <cmath>
#include <filesystem>
#include <numeric>
#include <random>
#include <sstream>
#include <stdexcept>
#include <vector>

#include <macros.h>

namespace speu::musdb18hq {

Dataset::Dataset(const std::filesystem::path& root, size_t element_count,
                 size_t seed_offset, bool no_distort) {
    for (const auto& dir : std::filesystem::directory_iterator(root)) {
        if (std::filesystem::is_directory(dir)) {
            _loaders.emplace_back(dir);
        }
    }
    std::sort(_loaders.begin(), _loaders.end(),
              [](decltype(_loaders)::const_reference a,
                 decltype(_loaders)::const_reference b) {
                  return a.get_name() < b.get_name();
              });
    _loaders.shrink_to_fit();
    _augmenter = Augmenter(length(), element_count, seed_offset, no_distort);
}
Sample::Loader& Dataset::get_loader(size_t index) {
    return _loaders.at(index);
}
Sample Dataset::get(size_t index,
                    const Augmenter::Instance& augmentation_instance) {
    auto& loader = _loaders.at(index);
    return _augmenter.apply(loader, augmentation_instance);
}
Sample Dataset::get(size_t index, size_t epoch) {
    auto& loader = _loaders.at(index);
    auto augmentation_instance = _augmenter.make_instance(epoch, index);
    return _augmenter.apply(loader, augmentation_instance);
}

Sample::Sample(at::Tensor a, at::Tensor b, at::Tensor c, at::Tensor m) :
    _a(a), _b(b), _c(c), _mix_factors(m) {
    if (_a.dim() == 2 && _b.dim() == 2 && _c.dim() == 2 &&
        _mix_factors.dim() == 3) {
        // Batched.
        if (_b.size(0) != _a.size(0) || _c.size(0) != _a.size(0) ||
            _mix_factors.size(0) != _a.size(0)) {
            std::stringstream msg;
            msg << "Batched Sample tensor batch dim size mismatch!"
                << " Received: a=" << _a.size(0) << ", b=" << _b.size(0)
                << ", c=" << _c.size(0) << ", m=" << _mix_factors.size(0)
                << ".";
            throw std::runtime_error(msg.str());
        }
        if (_a.size(1) != _b.size(1) || _a.size(1) != _c.size(1)) {
            std::stringstream msg;
            msg << "Batched Sample track tensor duration mismatch!"
                << " Received shapes: a(" << _a.sizes() << "), b(" << _b.sizes()
                << "), c(" << _c.sizes() << ").";
            throw std::runtime_error(msg.str());
        }
        if (_mix_factors.size(1) != 2 || _mix_factors.size(2) != 1) {
            std::stringstream msg;
            msg << "Batched Sample mix factors tensor "
                << "must be of shape (" << _a.size(0) << ", 2, 1)!"
                << " Received shape: " << _mix_factors.sizes() << ".";
            throw std::runtime_error(msg.str());
        }
    }
    else if (_a.dim() == 1 && _b.dim() == 1 && _c.dim() == 1 &&
             _mix_factors.dim() == 2) {
        if (_a.size(0) != _b.size(0) || _a.size(0) != _c.size(0)) {
            std::stringstream msg;
            msg << "Sample track tensor duration mismatch!"
                << " Received shapes: a(" << _a.sizes() << "), b(" << _b.sizes()
                << "), c(" << _c.sizes() << ").";
            throw std::runtime_error(msg.str());
        }
        if (_mix_factors.size(0) != 2 || _mix_factors.size(1) != 1) {
            std::stringstream msg;
            msg << "Batched Sample mix factors tensor must be of shape (2, 1)!"
                << " Received shape: " << _mix_factors.sizes() << ".";
            throw std::runtime_error(msg.str());
        }
    }
    else {
        std::stringstream msg;
        msg << "Invalid dimensions for Sample's tensors!"
            << " Received dims: "
            << "a=" << _a.dim() << ", b=" << _b.dim() << ", c=" << _c.dim()
            << ", m=" << _mix_factors.dim() << ".";
        throw std::runtime_error(msg.str());
    }
}
at::Tensor Sample::sgn_a() const {
    using namespace torch::indexing;
    if (_mix_factors.dim() == 3) {
        // Batched tensors.
        return _mix_factors.index({Slice(), 0});
    }
    else { /* _mix_factors.dim() == 2 */
        return _mix_factors.index({0});
    }
}
at::Tensor Sample::sgn_b() const {
    using namespace torch::indexing;
    if (_mix_factors.dim() == 3) {
        // Batched tensors.
        return _mix_factors.index({Slice(), 1});
    }
    else { /* _mix_factors.dim() == 2 */
        return _mix_factors.index({1});
    }
}

size_t Sample::get_batch_size() const {
    if (_a.dim() == 1) {
        // non-batched case.
        return 0;
    }
    else {
        // batched case.
        return _a.size(0);
    }
}
Sample Sample::collate(std::span<Sample> samples, bool pin_memory) {
    std::vector<at::Tensor> list(samples.size());

    for (size_t i = 0; i < samples.size(); i++) {
        list[i] = samples[i]._a;
    }
    auto batch_a = torch::stack(list);

    for (size_t i = 0; i < samples.size(); i++) {
        list[i] = samples[i]._b;
    }
    auto batch_b = torch::stack(list);

    for (size_t i = 0; i < samples.size(); i++) {
        list[i] = samples[i]._c;
    }
    auto batch_c = torch::stack(list);

    for (size_t i = 0; i < samples.size(); i++) {
        list[i] = samples[i]._mix_factors;
    }
    auto batch_mix_factors = torch::stack(list);

    if (pin_memory) {
        batch_a = batch_a.pin_memory();
        batch_b = batch_b.pin_memory();
        batch_c = batch_c.pin_memory();
        batch_mix_factors = batch_mix_factors.pin_memory();
    }

    return Sample(batch_a, batch_b, batch_c, batch_mix_factors);
}
std::ostream& operator<<(std::ostream& o, Sample::Loader::TrackFlagBits v) {
    switch (v) {
        case Sample::Loader::TrackFlagBits::Mixture: {
            o << "Mixture";
        } break;
        case Sample::Loader::TrackFlagBits::Bass: {
            o << "Bass";
        } break;
        case Sample::Loader::TrackFlagBits::Drums: {
            o << "Drums";
        } break;
        case Sample::Loader::TrackFlagBits::Vocals: {
            o << "Vocals";
        } break;
        case Sample::Loader::TrackFlagBits::Other: {
            o << "Other";
        } break;
    }
    return o;
}

Sample::Loader::Loader(const std::filesystem::path& root) :
    _name(root.filename()), _channel_count(-1), _sample_count(-1) {
    if (!root.has_filename() || _name.size() == 0) {
        throw std::runtime_error("Musdb18hq loader couldn't retrieve "
                                 "name from sample's path!");
    }
    {
        auto sample_path = root / "mixture.wav";
        if (std::filesystem::is_regular_file(sample_path)) {
            _mixture = speu::MappedWavTensor(sample_path);
            if (_channel_count == -1) {
                _channel_count = _mixture->get_channel_count();
                _sample_count = _mixture->get_sample_count();
            }
            else if ((size_t) _channel_count != _mixture->get_channel_count()) {
                throw std::runtime_error(
                     "Channel count mismatch for mixture track!");
            }
            else if ((size_t) _sample_count != _mixture->get_sample_count()) {
                throw std::runtime_error("Sample count mismatch "
                                         "for mixture track!");
            }
        }
        else {
            logger.logw("Couldn't find mixture track for sample `", _name,
                        "`!");
        }
    }
    {
        auto sample_path = root / "bass.wav";
        if (std::filesystem::is_regular_file(sample_path)) {
            _bass = speu::MappedWavTensor(sample_path);
            if (_channel_count == -1) {
                _channel_count = _bass->get_channel_count();
                _sample_count = _bass->get_sample_count();
            }
            else if ((size_t) _channel_count != _bass->get_channel_count()) {
                throw std::runtime_error(
                     "Channel count mismatch for bass track!");
            }
            else if ((size_t) _sample_count != _bass->get_sample_count()) {
                throw std::runtime_error("Sample count mismatch "
                                         "for bass track!");
            }
        }
        else {
            logger.logw("Couldn't find bass track for sample `", _name, "`!");
        }
    }
    {
        auto sample_path = root / "drums.wav";
        if (std::filesystem::is_regular_file(sample_path)) {
            _drums = speu::MappedWavTensor(sample_path);
            if (_channel_count == -1) {
                _channel_count = _drums->get_channel_count();
                _sample_count = _drums->get_sample_count();
            }
            else if ((size_t) _channel_count != _drums->get_channel_count()) {
                throw std::runtime_error(
                     "Channel count mismatch for drums track!");
            }
            else if ((size_t) _sample_count != _drums->get_sample_count()) {
                throw std::runtime_error("Sample count mismatch "
                                         "for drums track!");
            }
        }
        else {
            logger.logw("Couldn't find drums track for sample `", _name, "`!");
        }
    }
    {
        auto sample_path = root / "vocals.wav";
        if (std::filesystem::is_regular_file(sample_path)) {
            _vocals = speu::MappedWavTensor(sample_path);
            if (_channel_count == -1) {
                _channel_count = _vocals->get_channel_count();
                _sample_count = _vocals->get_sample_count();
            }
            else if ((size_t) _channel_count != _vocals->get_channel_count()) {
                throw std::runtime_error(
                     "Channel count mismatch for vocals track!");
            }
            else if ((size_t) _sample_count != _vocals->get_sample_count()) {
                throw std::runtime_error("Sample count mismatch "
                                         "for vocals track!");
            }
        }
        else {
            logger.logw("Couldn't find vocals track for sample `", _name, "`!");
        }
    }
    {
        auto sample_path = root / "other.wav";
        if (std::filesystem::is_regular_file(sample_path)) {
            _other = speu::MappedWavTensor(sample_path);
            if (_channel_count == -1) {
                _channel_count = _other->get_channel_count();
                _sample_count = _other->get_sample_count();
            }
            else if ((size_t) _channel_count != _other->get_channel_count()) {
                throw std::runtime_error(
                     "Channel count mismatch for other track!");
            }
            else if ((size_t) _sample_count != _other->get_sample_count()) {
                throw std::runtime_error("Sample count mismatch "
                                         "for other track!");
            }
        }
        else {
            logger.logw("Couldn't find other track for sample `", _name, "`!");
        }
    }
    if (get_track_count() < 2) {
        throw std::runtime_error("Couldn't find enough tracks for sample `" +
                                 _name + "`!");
    }
}
Sample::Loader::~Loader() = default;
bool Sample::Loader::has(TrackFlagBits track_enum) const {
    switch (track_enum) {
        case TrackFlagBits::Mixture: return has_mixture();
        case TrackFlagBits::Bass: return has_bass();
        case TrackFlagBits::Drums: return has_drums();
        case TrackFlagBits::Vocals: return has_vocals();
        case TrackFlagBits::Other: return has_other();
    }
    throw std::runtime_error("Unreachable@" FILE_LINE_STR);
}
at::Tensor Sample::Loader::get(TrackFlagBits track_enum) const {
    switch (track_enum) {
        case TrackFlagBits::Mixture: {
            return get_mixture();
        }
        case TrackFlagBits::Bass: {
            return get_bass();
        }
        case TrackFlagBits::Drums: {
            return get_drums();
        }
        case TrackFlagBits::Vocals: {
            return get_vocals();
        }
        case TrackFlagBits::Other: {
            return get_other();
        }
    }
    throw std::runtime_error("Unreachable@" FILE_LINE_STR);
}
size_t Sample::Loader::get_track_count() const {
    size_t c = 0;
    c += _mixture.has_value() ? 1 : 0;
    c += _bass.has_value() ? 1 : 0;
    c += _drums.has_value() ? 1 : 0;
    c += _vocals.has_value() ? 1 : 0;
    c += _other.has_value() ? 1 : 0;
    return c;
}
Sample::Loader::TrackFlag Sample::Loader::get_track_mask() const {
    Sample::Loader::TrackFlag mask;
    if (_mixture) {
        mask |= Sample::Loader::TrackFlagBits::Mixture;
    }
    if (_bass) {
        mask |= Sample::Loader::TrackFlagBits::Bass;
    }
    if (_drums) {
        mask |= Sample::Loader::TrackFlagBits::Drums;
    }
    if (_vocals) {
        mask |= Sample::Loader::TrackFlagBits::Vocals;
    }
    if (_other) {
        mask |= Sample::Loader::TrackFlagBits::Other;
    }
    return mask;
}
Sample::Loader::TrackFlagBits Sample::Loader::TrackFlag::operator[](
     size_t n) const {
    if (*this & TrackFlagBits::Mixture) {
        if (n == 0) {
            return TrackFlagBits::Mixture;
        }
        else {
            n--;
        }
    }
    if (*this & TrackFlagBits::Bass) {
        if (n == 0) {
            return TrackFlagBits::Bass;
        }
        else {
            n--;
        }
    }
    if (*this & TrackFlagBits::Drums) {
        if (n == 0) {
            return TrackFlagBits::Drums;
        }
        else {
            n--;
        }
    }
    if (*this & TrackFlagBits::Vocals) {
        if (n == 0) {
            return TrackFlagBits::Vocals;
        }
        else {
            n--;
        }
    }
    if (*this & TrackFlagBits::Other) {
        if (n == 0) {
            return TrackFlagBits::Other;
        }
        else {
            n--;
        }
    }
    throw std::out_of_range("No " + std::to_string(n) +
                            "-th bit flag for TrackFlag " +
                            std::to_string(static_cast<int>(_v)) + "!");
}

Augmenter::Augmenter(size_t sample_count, size_t element_count,
                     size_t seed_offset, bool no_distort) :
    _sample_count(sample_count),
    _element_count(element_count), _seed_offset(seed_offset),
    _no_distort(no_distort) {}
at::Generator Augmenter::make_random_generator(uint64_t index) const {
    std::seed_seq seq = {index, _seed_offset};
    std::mt19937_64 engine(seq);
    uint64_t seed = engine();
    at::Generator rng = at::make_generator<at::CPUGeneratorImpl>();
    rng.set_current_seed(seed);
    // logger.logv("Created generator for #", index, " with seed 0x", std::hex,
    //             seed, ".");
    return rng;
}
Augmenter::Instance Augmenter::make_instance(size_t epoch,
                                             size_t sample_index) const {
    return {make_random_generator(epoch * _sample_count + sample_index),
            _no_distort};
}
Sample Augmenter::apply(Sample::Loader& loader,
                        const Instance& instance) const {
    using namespace torch::indexing;

    auto track_flag_a = instance.get_first_track(loader);
    auto track_flag_b = instance.get_second_track(loader, track_flag_a);
    auto ch = instance.get_channel(loader);
    auto len = instance.get_raw_sample_count(_element_count);
    // logger.logd("For an output of ", _element_count, " got an input length of
    // ",
    //             len, " for a ratio of ", ((double) len) / _element_count,
    //             '.');
    auto off = instance.get_start_offset(loader, len);
    auto gain = instance.get_gain_distortion();

    auto mix_sgn =
         torch::ones({2, 1}, torch::TensorOptions(torch::Dtype::Char));
    if (track_flag_a == Sample::Loader::TrackFlagBits::Mixture) {
        mix_sgn[1] = -1;
    }
    else if (track_flag_b == Sample::Loader::TrackFlagBits::Mixture) {
        mix_sgn[0] = -1;
    }

    // Load/generate.
    auto a = loader.get(track_flag_a).index({ch, Slice(off, off + len)});
    auto b = loader.get(track_flag_b).index({ch, Slice(off, off + len)});
    auto c = a * mix_sgn.index({0}) + b * mix_sgn.index({1});

    // Complete loading and prepare augmentation batch.
    auto augment_batch = torch::stack({a, b, c});
    augment_batch =
         speu::transforms::scale_cast(augment_batch, torch::Dtype::Float);

    if (!_no_distort) {
        // Augment - resample.
        augment_batch = speu::audio::transforms::resample(augment_batch, len,
                                                          _element_count);

        // Augment - gain.
        augment_batch = speu::audio::transforms::gain(augment_batch, gain);
    }

    // Unpack augmentation batch.
    a = augment_batch.index({0, Slice()});
    b = augment_batch.index({1, Slice()});
    c = augment_batch.index({2, Slice()});

    return {a, b, c, mix_sgn};
}

Augmenter::Instance::Instance() = default;
Augmenter::Instance::Instance(at::Generator rng, bool no_distort) {
    _first_track = torch::rand({1}, rng).item().toFloat();
    _second_track = torch::rand({1}, rng).item().toFloat();
    _channel = torch::rand({1}, rng).item().toFloat();
    _position_offset = torch::rand({1}, rng).item().toFloat();
    if (!no_distort) {
        _resample_distortion =
             torch::normal(0.0, 1.33, {1}, rng).item().toFloat();
        _gain_distortion = torch::normal(0.0, 1, {1}, rng).item().toFloat();
        if (_gain_distortion > 0) {
            _gain_distortion *= 3;
        }
        else {
            _gain_distortion *= 12;
        }
    }
}
Sample::Loader::TrackFlagBits Augmenter::Instance::get_first_track(
     const Sample::Loader& loader) const {
    Sample::Loader::TrackFlag options = loader.get_track_mask();
    int8_t choice = static_cast<int8_t>(_first_track * options.count());
    auto flag = options[choice];
    // logger.logd("Instance select first track: ", flag);
    return flag;
}
Sample::Loader::TrackFlagBits Augmenter::Instance::get_second_track(
     const Sample::Loader& loader,
     Sample::Loader::TrackFlag first_track) const {
    Sample::Loader::TrackFlag options =
         loader.get_track_mask() & (~first_track);
    int8_t choice = static_cast<int8_t>(_second_track * options.count());
    auto flag = options[choice];
    // logger.logd("Instance select second track: ", flag);
    return flag;
}
int16_t Augmenter::Instance::get_channel(const Sample::Loader& loader) const {
    int16_t v = static_cast<int16_t>(_channel * loader.get_channel_count());
    // logger.logd("Instance select channel: ", v);
    return v;
}
size_t Augmenter::Instance::get_raw_sample_count(
     size_t output_sample_count) const {
    if (std::abs(_resample_distortion) <= 1e-4f) {
        return output_sample_count;
    }
    else {
        float factor = std::abs(_resample_distortion);
        // Apply clamp and map to factor.
        factor = std::clamp(factor, 0.0f, 3.0f);
        factor += 1.0;

        size_t target_sample_count;
        if (_resample_distortion >= 0) {
            target_sample_count =
                 static_cast<size_t>(((double) output_sample_count) * factor);
        }
        else { /** _resample_distortion < 0 */
            target_sample_count =
                 static_cast<size_t>(((double) output_sample_count) / factor);
        }
        // logger.logd("Instance select raw sample count: ",
        // target_sample_count,
        //             " for a factor of ", factor,
        //             " when resample_distortion=", _resample_distortion, '.');

        // Find optimal sample count between neighbors.
        constexpr int best_neighbor_search_radius = 11;
        int32_t best_neighbor_offset = 0;
        size_t best_neighbor_gcd = 1;
        for (int32_t neigh = -best_neighbor_search_radius;
             neigh <= best_neighbor_search_radius; neigh++) {
            auto neigh_gcd =
                 std::gcd(target_sample_count + neigh, output_sample_count);
            if (neigh_gcd > best_neighbor_gcd) {
                best_neighbor_offset = neigh;
                best_neighbor_gcd = neigh_gcd;
            }
        }
        // logger.logd("Instance select raw sample count neighbor offset: ",
        //             best_neighbor_offset, " with gcd: ", best_neighbor_gcd);

        return target_sample_count + best_neighbor_offset;
    }
}
size_t Augmenter::Instance::get_start_offset(const Sample::Loader& loader,
                                             size_t raw_sample_count) const {
    size_t total_sample_count = loader.get_sample_count();
    if (raw_sample_count > total_sample_count) {
        std::stringstream msg;
        msg << "Sample count to be loaded (" << raw_sample_count
            << ") is greater than the total sample count ("
            << total_sample_count << ") of the track.";
        throw std::underflow_error(msg.str());
    }
    size_t offset_area_length = total_sample_count - raw_sample_count;
    auto v =
         static_cast<size_t>(((double) offset_area_length) * _position_offset);
    // logger.logd("Instance select start offset: ", v);
    return v;
}

float Augmenter::Instance::mean_first_track(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._first_track;
        t[i] = v;
    }
    return t.mean().item().toFloat();
}
float Augmenter::Instance::var_first_track(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._first_track;
        t[i] = v;
    }
    return t.var().item().toFloat();
}
float Augmenter::Instance::mean_second_track(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._second_track;
        t[i] = v;
    }
    return t.mean().item().toFloat();
}
float Augmenter::Instance::var_second_track(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._second_track;
        t[i] = v;
    }
    return t.var().item().toFloat();
}
float Augmenter::Instance::mean_channel(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._channel;
        t[i] = v;
    }
    return t.mean().item().toFloat();
}
float Augmenter::Instance::var_channel(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._channel;
        t[i] = v;
    }
    return t.var().item().toFloat();
}
float Augmenter::Instance::mean_resample_distortion(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._resample_distortion;
        t[i] = v;
    }
    return t.mean().item().toFloat();
}
float Augmenter::Instance::var_resample_distortion(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._resample_distortion;
        t[i] = v;
    }
    return t.var().item().toFloat();
}
float Augmenter::Instance::mean_position_offset(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._position_offset;
        t[i] = v;
    }
    return t.mean().item().toFloat();
}
float Augmenter::Instance::var_position_offset(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._position_offset;
        t[i] = v;
    }
    return t.var().item().toFloat();
}
float Augmenter::Instance::mean_gain_distortion(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._gain_distortion;
        t[i] = v;
    }
    return t.mean().item().toFloat();
}
float Augmenter::Instance::var_gain_distortion(std::vector<Instance>& l) {
    int64_t n = l.size();
    auto t = torch::empty({n}, torch::TensorOptions(torch::Dtype::Float));
    for (int64_t i = 0; i < n; i++) {
        float v = l[i]._gain_distortion;
        t[i] = v;
    }
    return t.var().item().toFloat();
}
std::string Augmenter::Instance::pretty_string() const {
    std::stringstream ss;
    ss.precision(3);
    ss << "Augmenter::Instance("
       << "first_track=" << _first_track << ", second_track=" << _second_track
       << ", channel=" << _channel
       << ", resample_distortion=" << _resample_distortion
       << ", position_offset=" << _position_offset
       << ", gain_distortion=" << _gain_distortion << ')';
    return ss.str();
}

}  // namespace speu::musdb18hq
