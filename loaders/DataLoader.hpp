#ifndef SPEU_DATALOADER_HPP
#define SPEU_DATALOADER_HPP
/**
 * @file
 * @brief Multi-threaded dataset loader and random samplers.
 */

#include <Musdb18hq.hpp>
#include <Utils.hpp>

#include <atomic>
#include <condition_variable>
#include <cstddef>
#include <cstdint>
#include <functional>
#include <limits>
#include <memory>
#include <mutex>
#include <optional>
#include <semaphore>
#include <stdexcept>
#include <thread>
#include <utility>
#include <vector>

namespace speu {

class AugmentationSampler : public spec::INonCopyable {
   protected:
    std::reference_wrapper<const speu::musdb18hq::Augmenter> _augmenter;
    std::vector<std::pair<int64_t, musdb18hq::Augmenter::Instance>>
         _index_instances;

   public:
    AugmentationSampler(const musdb18hq::Augmenter& augmenter) :
        _augmenter(std::cref(augmenter)), _index_instances(length()) {}
    AugmentationSampler(musdb18hq::Dataset& dataset) :
        AugmentationSampler(dataset.get_augmenter()) {}

    AugmentationSampler(AugmentationSampler&& o) :
        _augmenter(o._augmenter),
        _index_instances(std::move(o._index_instances)) {}
    AugmentationSampler& operator=(AugmentationSampler&& o) {
        _augmenter = o._augmenter;
        _index_instances = std::move(o._index_instances);
        return *this;
    }

    ~AugmentationSampler();

    /* State getters */
    size_t length() const {
        return _augmenter.get().length();
    }

    /* Sample id getters */
    std::pair<size_t, musdb18hq::Augmenter::Instance> get(size_t index) const;
    std::pair<size_t, musdb18hq::Augmenter::Instance> operator[](
         size_t index) const {
        return get(index);
    }

    /* Nice to have for checking,validation,debugging... */
    std::vector<int64_t> get_shuffled_indexes() const;
    std::vector<musdb18hq::Augmenter::Instance> get_instances() const;

    /* Lifecycle */
    void prepare_epoch(size_t epoch);
};

class DataLoader : public spec::INonCopyable {
   protected:
    class BatchSlot : public spec::INonCopyable {
       public:
        using work_token_t = int32_t;
        static constexpr work_token_t INVALID_WORK_TOKEN = -1;
        static constexpr size_t INVALID_SAMPLE_TOKEN = -1ULL;

       protected:
        std::unique_ptr<musdb18hq::Sample[]> _samples;
        /** Offset into _sampler of parent object. */
        size_t _sample_offset;
        const work_token_t _max_sample_count;
        work_token_t _sample_count;

        /** Counts from 0 to _sample_offset_length. */
        std::atomic<work_token_t> _next_work_index;
        std::counting_semaphore<std::numeric_limits<work_token_t>::max()>
             _work_tokens;
        std::counting_semaphore<std::numeric_limits<work_token_t>::max()>
             _done_tokens;

       public:
        explicit BatchSlot(musdb18hq::Sample* samples, work_token_t batch_size);
        explicit BatchSlot(work_token_t batch_size);
        BatchSlot(BatchSlot&&);
        BatchSlot& operator=(BatchSlot&&);
        ~BatchSlot();

        /**
         * Called from main thread while monitor mutex is locked.
         *
         * Initializes internal state, by setting work start index and length,
         * and puts the appropriate amount of tokens into _work_tokens.
         *
         * Calling this function is invalid if _samples and _next_work_index
         * have not been reset and/or _done_tokens has available tokens.
         */
        void post_work(size_t offset, work_token_t length);
        /**
         * Called from main thread while monitor mutex is unlocked.
         *
         * Wait and collect all of _done_tokens.
         * Reset everything except _samples container.
         */
        void wait_work_done();
        /**
         * Called from main thread while monitor mutex is unlocked,
         * right after wait_work_done is called.
         *
         * Collates samples into batch tensor (which is what is returned)
         * and completes reset by resetting _samples container.
         */
        musdb18hq::Sample retrieve_work(bool pin_memory);

        /**
         * Called from worker threads while monitor mutex is locked.
         *
         * Tests if object is initialized and work is available.
         */
        bool has_work();
        /**
         * Called from worker threads while monitor mutex is not locked.
         *
         * Atomically acquires next work token and returns the work as a
         * _sampler's index.
         */
        size_t get_next_work();
        /**
         * Called from worker threads while monitor mutex is not locked.
         *
         * Stores the loaded sample data to the internal container
         * and returns the work token acquired
         * by get_next_work to the done token queue.
         */
        void complete_work(size_t index, musdb18hq::Sample&& data);

        bool is_reset();
    };

   protected:
    musdb18hq::Dataset& _dataset;
    AugmentationSampler _sampler;

    std::vector<BatchSlot> _batch_slots;
    const BatchSlot::work_token_t _max_batch_size;
    /**
     * The index of the slot that workers threads are currently processing.
     */
    size_t _current_loading_slot = 0;
    /**
     * The index of the slot that will be returned from \ref get_next_batch.
     */
    size_t _next_batch_slot = 0;

    int64_t _epoch = -1;
    size_t _posted_sample_count = 0;
    size_t _returned_sample_count = 0;
    std::atomic<size_t> _loaded_sample_count = 0;

    std::vector<std::thread> _workers;
    bool _worker_exit_cond = false;
    uint8_t __reserved0[3];
    const bool _pin_memory;
    uint8_t __reserved1[3];
    std::mutex _monitor;
    std::condition_variable _cond;

   public:
    DataLoader(musdb18hq::Dataset& dataset, BatchSlot::work_token_t batch_size,
               std::optional<AugmentationSampler> sampler = {},
               size_t batch_queue_size = 3, int32_t worker_thread_count = -1,
               bool pin_memory = false);
    DataLoader(musdb18hq::Dataset& dataset, BatchSlot::work_token_t batch_size,
               size_t batch_queue_size = 3, int32_t worker_thread_count = -1,
               bool pin_memory = false) :
        DataLoader(dataset, batch_size, {}, batch_queue_size,
                   worker_thread_count, pin_memory) {}
    DataLoader(musdb18hq::Dataset& dataset, BatchSlot::work_token_t batch_size,
               AugmentationSampler& sampler, size_t batch_queue_size = 3,
               int32_t worker_thread_count = -1, bool pin_memory = false) :
        DataLoader(dataset, batch_size, std::move(sampler), batch_queue_size,
                   worker_thread_count, pin_memory) {}

    ~DataLoader();

    /* Getters for invariant state. */
    size_t length() const {
        return _dataset.length();
    }
    musdb18hq::Dataset& get_dataset() const {
        return _dataset;
    }
    const AugmentationSampler& get_sampler() const {
        return _sampler;
    }
    BatchSlot::work_token_t get_batch_size() const {
        return _max_batch_size;
    }
    BatchSlot::work_token_t get_batch_count() const {
        auto full_count = length() / get_batch_size();
        auto remainder = length() % get_batch_size();
        return full_count + (remainder != 0 ? 1 : 0);
    }
    size_t get_worker_count() const {
        return _workers.size();
    }
    size_t get_batch_queue_size() const {
        return _batch_slots.size();
    }
    size_t get_sample_queue_size() const {
        return get_batch_queue_size() * get_batch_size();
    }

    /* Getters regarding current batch */
    int64_t get_epoch() const {
        return _epoch;
    }
    /**
     * @return The total number of samples received as batches in the current
     * epoch.
     */
    size_t get_iteration() const {
        return _returned_sample_count;
    }
    bool has_reached_epoch_end() const {
        return get_iteration() == length();
    }

    /* Getters for worker pool state. */
    bool is_work_fully_posted() const {
        return _posted_sample_count == length();
    }
    bool is_done_loading() const {
        return _loaded_sample_count == length();
    }

    /* Iteration operations */
    musdb18hq::Sample get_next_batch();
    /**
     * Starts loading for next epoch.
     *
     * @return the new epoch counter.
     */
    int64_t next_epoch();

   protected:
    musdb18hq::Sample _load_sample(size_t sample_index);

    /* For main thread */
    bool _post_work(BatchSlot& slot);
    BatchSlot& _get_next_slot();
    musdb18hq::Sample _get_next_batch_sync();
    musdb18hq::Sample _get_next_batch_async();

    /* For worker threads */
    void _init_workers();
    void _worker_main();
    static void _worker_main(DataLoader& obj, size_t uid);
};

}  // namespace speu

#endif /*SPEU_DATALOADER_HPP*/
