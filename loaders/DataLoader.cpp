#include "DataLoader.hpp"

#include <logger/Logger.hpp>

#include <limits>
#include <memory>
#include <span>
#include <sstream>
#include <stdexcept>

#include <macros.h>

namespace speu {

AugmentationSampler::~AugmentationSampler() = default;
void AugmentationSampler::prepare_epoch(size_t epoch) {
    // Reset state.
    _index_instances.clear();
    _index_instances.reserve(length());

    // Shuffle samples.
    at::Generator index_shuffler = _augmenter.get().make_random_generator(
         std::numeric_limits<uint64_t>::max() - epoch * length());
    auto shuffled_indexes = torch::randperm(length(), index_shuffler);

    // Copy indexes and generate instances.
    for (size_t i = 0; i < length(); i++) {
        int64_t index = shuffled_indexes[i].item().toLong();
        auto instance = _augmenter.get().make_instance(epoch, index);
        _index_instances.emplace_back(index, instance);
    }

    // Post cleanup and sanity checks.
    _index_instances.shrink_to_fit();
    if (_index_instances.size() != length()) {
        throw std::runtime_error("Unreachable@" FILE_LINE_STR);
    }
}
std::pair<size_t, musdb18hq::Augmenter::Instance> AugmentationSampler::get(
     size_t index) const {
    return _index_instances.at(index);
}
std::vector<int64_t> AugmentationSampler::get_shuffled_indexes() const {
    std::vector<int64_t> l;
    l.reserve(length());
    for (const auto& [index, instance] : _index_instances) {
        l.push_back(index);
    }
    l.shrink_to_fit();
    return l;
}
std::vector<musdb18hq::Augmenter::Instance> AugmentationSampler::get_instances()
     const {
    std::vector<musdb18hq::Augmenter::Instance> l;
    l.reserve(length());
    for (const auto& [index, instance] : _index_instances) {
        l.push_back(instance);
    }
    l.shrink_to_fit();
    return l;
}

DataLoader::BatchSlot::BatchSlot(musdb18hq::Sample* samples,
                                 work_token_t batch_size) :
    _samples(samples),
    _sample_offset(INVALID_SAMPLE_TOKEN), _max_sample_count(batch_size),
    _sample_count(INVALID_WORK_TOKEN), _next_work_index(INVALID_WORK_TOKEN),
    _work_tokens(0), _done_tokens(0) {
    if (samples == nullptr) {
        throw std::runtime_error("Unreachable@" FILE_LINE_STR);
    }
}
DataLoader::BatchSlot::BatchSlot(work_token_t batch_size) :
    BatchSlot(new musdb18hq::Sample[batch_size], batch_size) {}
DataLoader::BatchSlot::BatchSlot(BatchSlot&& o) :
    BatchSlot(o._samples.release(), o._max_sample_count) {
    // Object must be at fully reset state.
    if (!o.is_reset()) {
        throw std::runtime_error(
             "Cannot move from a BatchSlot which isn't in a reset state!");
    }
}
DataLoader::BatchSlot& DataLoader::BatchSlot::operator=(BatchSlot&& o) {
    // Objects must be at fully reset state.
    if (!o.is_reset()) {
        throw std::runtime_error(
             "Cannot move from a BatchSlot which isn't in a reset state!");
    }
    if (!is_reset()) {
        throw std::runtime_error(
             "Cannot move into a BatchSlot which isn't in a reset state!");
    }

    /*
     * This is really questionable,
     * but it should be fine,
     * as this is an public type.
     */
    std::destroy_at(this);
    std::construct_at(this, o._samples.release(), o._max_sample_count);

    return *this;
}
DataLoader::BatchSlot::~BatchSlot() = default;

void DataLoader::BatchSlot::post_work(size_t offset, work_token_t length) {
    if (length <= 0 || length > _max_sample_count) [[unlikely]] {
        std::stringstream msg;
        msg << "Invalid length #" << length << ". Expected max #"
            << _max_sample_count << ".";
        throw std::runtime_error(msg.str());
    }
    if (!is_reset()) {
        throw std::runtime_error("Cannot post work on a non-reset BatchSlot!");
    }
    // Sanity checks for wrong state.
    bool warn_once = false;
    for (work_token_t i = 0; i < _max_sample_count; i++) {
        if (_samples[i]) {
            warn_once = true;
            _samples[i] = musdb18hq::Sample();
        }
    }
    if (warn_once) {
        logger.logw("Wrong BatchSlot state: posting work when previous results "
                    "haven't been retrieved!");
    }
    // Validate state.
    _sample_offset = offset;
    _sample_count = length;
    _next_work_index = 0;
    // Post work tokens.
    for (work_token_t i = 0; i < _sample_count; i++) {
        _work_tokens.release();
    }
}
void DataLoader::BatchSlot::wait_work_done() {
    // Sanity checks.
    if (_sample_offset == INVALID_SAMPLE_TOKEN ||
        _sample_count == INVALID_WORK_TOKEN ||
        _next_work_index == INVALID_WORK_TOKEN) [[unlikely]] {
        throw std::runtime_error("Unreachable@" FILE_LINE_STR);
    }
    // Acquire all work done tokens.
    for (work_token_t i = 0; i < _sample_count; i++) {
        _done_tokens.acquire();
    }
    // Now it is ensured that no worker thread has access to this object.
    // logger.logv("BatchSlot loading complete #", _sample_offset);
    // Do partial reset.
    _sample_offset = INVALID_SAMPLE_TOKEN;
    _next_work_index = INVALID_WORK_TOKEN;
    if (!is_reset()) {
        throw std::runtime_error("Unreachable@" FILE_LINE_STR);
    }
}
musdb18hq::Sample DataLoader::BatchSlot::retrieve_work(bool pin_memory) {
    auto batch = musdb18hq::Sample::collate(
         std::span<musdb18hq::Sample>(_samples.get(), _sample_count),
         pin_memory);
    // logger.logv("Slot to batch.");

    // Reset container.
    for (work_token_t i = 0; i < _sample_count; i++) {
        _samples[i] = musdb18hq::Sample();
    }
    _sample_count = INVALID_WORK_TOKEN;

    return batch;
}
bool DataLoader::BatchSlot::has_work() {
    if (_sample_count != INVALID_WORK_TOKEN) [[likely]] {
        if (_next_work_index < _sample_count) {
            return true;
        }
    }
    return false;
}
size_t DataLoader::BatchSlot::get_next_work() {
    if (_work_tokens.try_acquire()) {
        auto token = _next_work_index.fetch_add(1);
        // logger.logv("Got #", token, " work.");
        return _sample_offset + token;
    }
    else {
        // logger.logd("No more work.");
        return DataLoader::BatchSlot::INVALID_WORK_TOKEN;
    }
}
void DataLoader::BatchSlot::complete_work(size_t index,
                                          musdb18hq::Sample&& data) {
    work_token_t subindex = index - _sample_offset;
    if (subindex < 0 || subindex >= _sample_count) [[unlikely]] {
        std::stringstream msg;
        msg << "Global work index #" << index << " results in local index #"
            << subindex << " which is out of range!";
        throw std::out_of_range(msg.str());
    }
    if (_samples[subindex]) [[unlikely]] {
        throw std::out_of_range("Tried to complete loading of a sample which "
                                "has already been loaded!");
    }
    _samples[subindex] = std::move(data);
    _done_tokens.release();
}
bool DataLoader::BatchSlot::is_reset() {
    if (_work_tokens.try_acquire()) [[unlikely]] {
        _work_tokens.release();
        return false;
    }
    if (_done_tokens.try_acquire()) [[unlikely]] {
        _done_tokens.release();
        return false;
    }
    if (_sample_offset == INVALID_SAMPLE_TOKEN &&
        /* _sample_count and _samples are ignored */
        _next_work_index == INVALID_WORK_TOKEN) [[likely]] {
        return true;
    }
    return false;
}

DataLoader::DataLoader(musdb18hq::Dataset& dataset,
                       BatchSlot::work_token_t batch_size,
                       std::optional<AugmentationSampler> sampler,
                       size_t batch_queue_size, int32_t worker_thread_count,
                       bool pin_memory) :
    _dataset(dataset),
    _sampler(sampler ? std::move(*sampler) : AugmentationSampler(dataset)),
    _max_batch_size(batch_size),
    _workers(worker_thread_count >= 0 ?
                  worker_thread_count :
                  std::thread::hardware_concurrency() - 1),
    _pin_memory(pin_memory) {
    if (_max_batch_size <= 0) {
        throw std::invalid_argument(
             "DataLoader's batch_size must be positive!");
    }
    // Initialize batch slots (equivalent to prefetch buffers).
    if (get_worker_count() != 0) {
        if (batch_queue_size == 0) {
            throw std::invalid_argument(
                 "DataLoader's batch_queue_size cannot be 0"
                 " when using asynchronous loading.");
        }
        _batch_slots.reserve(batch_queue_size);
        for (size_t i = 0; i < batch_queue_size; i++) {
            _batch_slots.emplace_back(get_batch_size());
        }
        _batch_slots.shrink_to_fit();
    }
    else {
        logger.logw("DataLoader has been constructed with no worker threads! "
                    "Synchronous loading will be used.");
    }
    /*
     * Test and warn if dataset length is less
     * than the total of prefetch sample buffers.
     */
    if (length() < get_sample_queue_size()) {
        logger.logw("Constructed DataLoader's Dataset size(#", length(),
                    ") is less than the count of sample prefetch buffers(#",
                    get_sample_queue_size(), ").");
    }
    else {
        logger.logi("Constructed DataLoader for Dataset with #", length(),
                    " samples and #", get_sample_queue_size(),
                    " sample prefetch buffers.");
    }
    // Initialize worker pool.
    _init_workers();
}

void DataLoader::_init_workers() {
    // Allocate worker threads.
    logger.logi("Starting DataLoader's worker pool with #", _workers.size(),
                " threads...");
    for (size_t i = 0; i < _workers.size(); i++) {
        _workers[i] = std::thread(static_cast<void (*)(DataLoader&, size_t)>(
                                       &DataLoader::_worker_main),
                                  std::ref(*this), i);
    }
}

DataLoader::~DataLoader() {
    // Wait until all workers complete work on all batches.
    for (auto& batch_slot : _batch_slots) {
        if (!batch_slot.is_reset()) {
            batch_slot.wait_work_done();
            // Also clear state.
            batch_slot.retrieve_work(false);
        }
    }
    {
        // Acquire state lock.
        std::unique_lock<std::mutex> lock(_monitor);
        // Mark exit condition.
        _worker_exit_cond = true;
        // Notify workers.
        _cond.notify_all();
    }
    // Join with worker threads.
    for (size_t i = 0; i < _workers.size(); i++) {
        _workers[i].join();
    }
}

int64_t DataLoader::next_epoch() {
    // Wait until all workers complete work on all batches.
    for (auto& batch_slot : _batch_slots) {
        if (!batch_slot.is_reset()) {
            batch_slot.wait_work_done();
            // Also clear state.
            batch_slot.retrieve_work(false);
        }
    }

    {
        // Acquire state lock.
        std::unique_lock<std::mutex> lock(_monitor);
        // Reset state.
        _posted_sample_count = 0;
        _returned_sample_count = 0;
        _loaded_sample_count = 0;
        _current_loading_slot = 0;
        _next_batch_slot = 0;
        // Increment state.
        _epoch++;
        _sampler.prepare_epoch(_epoch);
        // Distribute work to prefetch queues.
        for (auto& batch_slot : _batch_slots) {
            if (!_post_work(batch_slot)) {
                break;
            }
        }
        logger.logi("DataLoader starting load for epoch #", _epoch, "...");
        // Wake up worker threads and release state lock.
        _cond.notify_all();
    }

    return _epoch;
}

bool DataLoader::_post_work(BatchSlot& slot) {
    size_t samples_remain = length() - _posted_sample_count;
    if (samples_remain == 0) [[unlikely]] {
        throw std::runtime_error("Unreachable@" FILE_LINE_STR);
    }
    BatchSlot::work_token_t batch_size =
         std::min<size_t>(samples_remain, _max_batch_size);
    slot.post_work(_posted_sample_count, batch_size);
    _posted_sample_count += batch_size;
    if (_posted_sample_count >= length()) {
        if (_posted_sample_count > length()) [[unlikely]] {
            throw std::runtime_error("Unreachable@" FILE_LINE_STR);
        }
        return false;
    }
    return true;
}
DataLoader::BatchSlot& DataLoader::_get_next_slot() {
    // Called while monitor lock mutex is locked.
    // When this is called, the batch slot at index _next_batch_slot is empty.
    auto& slot = _batch_slots[_next_batch_slot];
    // Notify, when the mutex is unlocked, potential threads that new work is
    // available.
    _cond.notify_all();
    return slot;
}

musdb18hq::Sample DataLoader::get_next_batch() {
    if (get_batch_queue_size() == 0) {
        return _get_next_batch_sync();
    }
    else {
        return _get_next_batch_async();
    }
}
musdb18hq::Sample DataLoader::_get_next_batch_sync() {
    // Get remaing samples and batch size.
    size_t samples_remain = length() - _posted_sample_count;
    if (samples_remain == 0) [[unlikely]] {
        throw std::runtime_error("No more samples!");
    }
    BatchSlot::work_token_t batch_size =
         std::min<size_t>(samples_remain, _max_batch_size);
    // Allocate samples for batch.
    std::vector<musdb18hq::Sample> samples;
    samples.reserve(batch_size);
    // Fill batch.
    for (BatchSlot::work_token_t i = 0; i < batch_size; i++) {
        samples.push_back(_load_sample(_posted_sample_count + i));
    }
    // Increment sample counters.
    _posted_sample_count += batch_size;
    _returned_sample_count += batch_size;

    return musdb18hq::Sample::collate(samples, _pin_memory);
}
musdb18hq::Sample DataLoader::_get_next_batch_async() {
    // Wait for all processing to finish on next batch slot.
    _batch_slots[_next_batch_slot].wait_work_done();
    // Get batch data.
    musdb18hq::Sample samples = _batch_slots[_next_batch_slot].retrieve_work(_pin_memory);
    // logger.logd("Retrieved batched sample data from #", _next_batch_slot,
    //             " slot.");
    {
        // Acquire state lock.
        std::unique_lock<std::mutex> lock(_monitor);
        // Check if all work has already been posted.
        if (!is_work_fully_posted()) {
            // Select next batch.
            _post_work(_get_next_slot());
        }
        // Increment state.
        _next_batch_slot = (_next_batch_slot + 1) % get_batch_queue_size();
        _returned_sample_count += samples.get_batch_size();
    }
    return samples;
}

void DataLoader::_worker_main() {
    std::unique_lock<std::mutex> lock(_monitor);
    while (true) {
        _cond.wait(lock, [this]() {
            // We have the lock here.
            if (this->_worker_exit_cond) {
                return true;
            }
            else {
                /**
                 * Try to find work from loading slot up to the current slot.
                 * If no work is found, the loading slot index should be left
                 * on the next slot that is going to be used in get_next_batch
                 * (aka _current_loading_slot).
                 *
                 * Efficiency is dropped when no work is available in any slot,
                 * as each thread will always check each slot for work.
                 */
                do {
                    // logger.logd("Testing if batch slot #",
                    //             _current_loading_slot, " has available
                    //             work.");
                    if (_batch_slots[_current_loading_slot].has_work()) {
                        return true;
                    }
                    _current_loading_slot =
                         (_current_loading_slot + 1) % get_batch_queue_size();
                } while (_current_loading_slot != _next_batch_slot);
                // logger.logd("Waiting on batch slot #", _current_loading_slot,
                //             ".");
                return false;
            }
        });
        if (this->_worker_exit_cond) {
            return;  // Auto unlocks.
        }
        else {
            //
            auto& slot = _batch_slots[_current_loading_slot];
            // Receive and process work.
            lock.unlock();
            size_t work_sample_index;
            while (work_sample_index = slot.get_next_work(),
                   work_sample_index != BatchSlot::INVALID_SAMPLE_TOKEN) {
                // logger.logd("load sample start: ", work_sample_index);
                // Process work and post work completion.
                slot.complete_work(work_sample_index,
                                   _load_sample(work_sample_index));
                _loaded_sample_count++;
                // logger.logd("load sample end: ", work_sample_index);
            }
            // Batch slot has no more work.
            lock.lock();
        }
    }
}

void DataLoader::_worker_main(DataLoader& obj, size_t uid) {
    {
        std::stringstream name;
        name << "dlw#";
        name << std::setfill('0') << std::setw(3) << uid;
        pthread_setname_np(pthread_self(), name.str().c_str());
    }
    logger.logi("Process worker thread in startup...");
    obj._worker_main();
    logger.logi("Process worker thread exiting...");
}

musdb18hq::Sample DataLoader::_load_sample(size_t sample_index) {
    auto [augmented_index, augmented_instance] = _sampler[sample_index];
    return _dataset.get(augmented_index, augmented_instance);
}

}  // namespace speu
