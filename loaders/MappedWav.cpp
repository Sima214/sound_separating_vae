#include "FileUtils.hpp"
#include "MappedWav.hpp"

#include <logger/Logger.hpp>

#include <bit>
#include <cstddef>
#include <ios>
#include <ostream>
#include <stdexcept>

#include <torch/data.h>
#include <torch/types.h>

static constexpr size_t PAGE_SIZE = 4096;

namespace speu {

namespace riff::chunks {
extern "C" {

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"

enum class FormatCode : uint16_t {
    Pcm = 0x0001,
    IEEEFloat = 0x0003,
    ALaw = 0x0006,
    MULaw = 0x0007,
    Extensible = 0xFFFE,
};

typedef union {
    char str[4];
    uint32_t raw;
} ChunkID;

static constexpr ChunkID RIFF_ID = {.str = {'R', 'I', 'F', 'F'}};
static constexpr ChunkID WAVE_ID = {.str = {'W', 'A', 'V', 'E'}};
static constexpr ChunkID FMT_ID = {.str = {'f', 'm', 't', ' '}};
static constexpr ChunkID DATA_ID = {.str = {'d', 'a', 't', 'a'}};

typedef struct {
    ChunkID ch_id;
    uint32_t ck_size;
} Common;

typedef struct {
    FormatCode fmt;
    uint16_t channel_count;
    uint32_t sampling_rate;
    uint32_t avg_bytes_per_second;
    uint16_t block_align;
    uint16_t bits_per_sample;
    uint8_t next_chunk[];
} Format;

typedef struct {
    FormatCode fmt;
    uint16_t channel_count;
    uint32_t sampling_rate;
    uint32_t avg_bytes_per_second;
    uint16_t block_align;
    uint16_t bits_per_sample;
    uint16_t extension_size;
    uint8_t next_chunk[];
} FormatExtra;

// typedef struct {
//     FormatCode fmt;
//     uint16_t channel_count;
//     uint32_t sampling_rate;
//     uint32_t avg_bytes_per_second;
//     uint16_t block_align;
//     uint16_t bits_per_sample;
//     uint16_t extension_size;
//     union {} extensions;
// } FormatExt;

typedef struct {
    uint8_t data[1];
} Data;

typedef struct {
    Common type;
    union {
        Format fmt;
        FormatExtra fmt_extra;
        Data data;
    } subchunk;
} SubChunk;

typedef struct {
    Common signature;
    ChunkID wave_id;
    uint8_t chunks[];
} Header;

static_assert(sizeof(Common) == 8);
static_assert(sizeof(Header) == 12);
static_assert(sizeof(Format) == 16);
static_assert(offsetof(FormatExtra, next_chunk) == 18);

#pragma GCC diagnostic pop
}

std::ostream& operator<<(std::ostream& o, const ChunkID& v) {
    o << "0x" << std::hex << v.raw << std::dec;
    return o;
}
std::ostream& operator<<(std::ostream& o, const FormatCode& v) {
    o << "0x" << std::hex << static_cast<uint16_t>(v) << std::dec;
    return o;
}

}  // namespace riff::chunks

static constexpr bool __test_id(const riff::chunks::ChunkID& a,
                                const riff::chunks::ChunkID& b) {
    return a.raw == b.raw;
}

static uint16_t __load_little(uint16_t& p) {
    if constexpr (std::endian::native == std::endian::little) {
        return p;
    }
    else {
        uint16_t v = p;
        v = ((v & 0x00FF) << 8) | ((v & 0xFF00) >> 8);
        return v;
    }
}
static uint32_t __load_little(uint32_t& p) {
    if constexpr (std::endian::native == std::endian::little) {
        return p;
    }
    else {
        uint32_t v = p;
        v = ((v & 0x000000FF) << 24) | ((v & 0x0000FF00) << 8) |
            ((v & 0x00FF0000) >> 8) | ((v & 0xFF000000) >> 24);
        return v;
    }
}

static riff::chunks::FormatCode __load_fmt_tag(riff::chunks::FormatCode& pfmt) {
    uint16_t v = __load_little(reinterpret_cast<uint16_t&>(pfmt));
    return static_cast<riff::chunks::FormatCode>(v);
}

static bool __validate_fmt_tag(riff::chunks::FormatCode& pfmt) {
    riff::chunks::FormatCode fmt = __load_fmt_tag(pfmt);
    switch (fmt) {
        case riff::chunks::FormatCode::Pcm:
        case riff::chunks::FormatCode::IEEEFloat: {
            logger.logv("Found supported wave data format tag(", fmt, ").");
            return true;
        } break;
        default: {
            logger.logw("Found unsupported wave data format tag(", fmt, ")!");
            return false;
        } break;
    }
}

static bool __validate_block_size(riff::chunks::Format* fmt) {
    size_t channel_count = __load_little(fmt->channel_count);
    size_t bit_width = __load_little(fmt->bits_per_sample);
    size_t block_size = __load_little(fmt->block_align);
    size_t expected_block_size = channel_count * (bit_width / 8);
    if (block_size != expected_block_size) {
        logger.logw("Found unsupported sample block alignment (got: ",
                    block_size, ", expected: ", expected_block_size, ")!");
        return false;
    }
    return true;
}
static bool __validate_block_size(riff::chunks::FormatExtra* fmt) {
    return __validate_block_size(reinterpret_cast<riff::chunks::Format*>(fmt));
}

MappedWav::MappedWav(const std::filesystem::path& path, bool lock_header,
                     bool sequential) {
    logger.logd("Loading and mapping wav file `", path, "`...");
    spec::FileDescriptor fd(path, std::ios_base::in | std::ios_base::binary);
    if (!fd) {
        throw std::runtime_error("Couldn't open wav file for mapping!");
    }
    _hnd = fd.map();
    if (!_hnd) {
        throw std::runtime_error("Couldn't map wav file!");
    }
    if (lock_header) {
        if (!_hnd.lock(0, PAGE_SIZE)) {
            logger.logw("Couldn't lock header of mapped wav file!");
        }
    }
    // TODO: convert to tri-state.
    if (!_hnd.advice(sequential ? spec::MemoryMapping::Advice::Sequential :
                                  spec::MemoryMapping::Advice::Random)) {
        logger.logw("Couldn't apply access advice on mapped wav file!");
    }
    if (!validate()) {
        throw std::runtime_error("Failed mapped wav file verification!");
    }
}
MappedWav::~MappedWav() = default;

bool MappedWav::validate() {
    // Test file signature/main chunk.
    riff::chunks::Header* header = _hnd;
    size_t remaining = _hnd.size();
    if (remaining < sizeof(riff::chunks::Header)) {
        logger.logw(
             "Reached end-of-file before being able to read wav signature!");
        return false;
    }
    if (!__test_id(header->signature.ch_id, riff::chunks::RIFF_ID)) {
        logger.logw("Invalid wav signature: RIFF id (got: 0x",
                    header->signature.ch_id, ")!");
        return false;
    }
    size_t header_size = __load_little(header->signature.ck_size);
    if (header_size != (remaining - 8)) {
        logger.logw("Invalid wav signature: length mismatch (got: ",
                    header_size, ", expected: ", (remaining - 8), ")!");
        return false;
    }
    if (!__test_id(header->wave_id, riff::chunks::WAVE_ID)) {
        logger.logw("Invalid wav signature: WAVE id (got: 0x", header->wave_id,
                    ")!");
        return false;
    }

    auto* subchunk = (riff::chunks::SubChunk*) &header->chunks;
    remaining -= sizeof(riff::chunks::Header);

    size_t fmt_chunk_count = 0;
    size_t data_chunk_count = 0;

    // Iterate over subchunks.
    while (remaining >= sizeof(riff::chunks::Common)) {
        // Subchunk bounds checking.
        size_t subchunk_size = __load_little(subchunk->type.ck_size) + 8;
        if (subchunk_size > remaining) {
            logger.logw("Reached end-of-file while parsing subchunk, got: ",
                        remaining, ", expected: ", subchunk_size, "!");
            return false;
        }
        // Handle subchunk types case by case.
        if (__test_id(subchunk->type.ch_id, riff::chunks::FMT_ID)) {
            if (subchunk_size == sizeof(riff::chunks::Format) + 8) {
                auto* subchunkfmt = &subchunk->subchunk.fmt;
                fmt_chunk_count++;
                // Test for unsupported fields.
                if (!__validate_fmt_tag(subchunkfmt->fmt)) {
                    return false;
                }
                if (!__validate_block_size(subchunkfmt)) {
                    return false;
                }
                // Prepare for next chunk.
                subchunk = (riff::chunks::SubChunk*) &subchunkfmt->next_chunk;
                remaining -= subchunk_size;
            }
            else if (subchunk_size ==
                     offsetof(riff::chunks::FormatExtra, next_chunk) + 8) {
                auto* subchunkfmt = &subchunk->subchunk.fmt_extra;
                fmt_chunk_count++;
                // Test for unsupported fields.
                if (!__validate_fmt_tag(subchunkfmt->fmt)) {
                    return false;
                }
                if (!__validate_block_size(subchunkfmt)) {
                    return false;
                }
                if (subchunkfmt->extension_size != 0) {
                    logger.logw("Got invalid fmt extension size (got: ",
                                __load_little(subchunkfmt->extension_size),
                                ", expected: ", 0, ")!");
                    return false;
                }
                // Prepare for next chunk.
                subchunk = (riff::chunks::SubChunk*) &subchunkfmt->next_chunk;
                remaining -= subchunk_size;
            }
            else {
                // Extension handling is not implemented.
                logger.logw("Encountered an unsupported fmt chunk extension "
                            "during header validation!");
                return false;
            }
        }
        else if (__test_id(subchunk->type.ch_id, riff::chunks::DATA_ID)) {
            data_chunk_count++;
            // Data chunk bounds have already been checked.
            // Wave formats are padded on 2 bytes.
            size_t padded_subchunk_size =
                 (subchunk_size % 2 == 0) ? subchunk_size : (subchunk_size + 1);
            // Prepare for next chunk (or the end).
            subchunk = (riff::chunks::SubChunk*) (((uintptr_t) subchunk) +
                                                  padded_subchunk_size);
            remaining -= padded_subchunk_size;
        }
        else {
            // Skip unknown subchunk.
            logger.logd("Skipping unknown wave chunk with id `",
                        subchunk->type.ch_id, "` and size of ",
                        subchunk->type.ck_size, "...");

            // Wave formats are padded on 2 bytes.
            size_t padded_subchunk_size =
                 (subchunk_size % 2 == 0) ? subchunk_size : (subchunk_size + 1);
            subchunk = (riff::chunks::SubChunk*) (((uintptr_t) subchunk) +
                                                  padded_subchunk_size);
            remaining -= padded_subchunk_size;
        }
    }

    if (fmt_chunk_count != 1) {
        logger.loge("Unsupported fmt chunk count(got: ", fmt_chunk_count,
                    ", expected: ", 1, ")!");
        return false;
    }
    if (data_chunk_count != 1) {
        logger.loge("Unsupported data chunk count(got: ", data_chunk_count,
                    ", expected: ", 1, ")!");
        return false;
    }

    return remaining == 0;
}

static riff::chunks::SubChunk* _retrieve_fmt_chunk(spec::MemoryMapping& m) {
    riff::chunks::Header* header = m;
    auto* subchunk = (riff::chunks::SubChunk*) &header->chunks;
    while (true) {
        if (__test_id(subchunk->type.ch_id, riff::chunks::FMT_ID)) {
            return subchunk;
        }
        else {
            size_t subchunk_size = __load_little(subchunk->type.ck_size) + 8;
            size_t padded_subchunk_size =
                 (subchunk_size % 2 == 0) ? subchunk_size : (subchunk_size + 1);
            subchunk = (riff::chunks::SubChunk*) (((uintptr_t) subchunk) +
                                                  padded_subchunk_size);
        }
    }
}

static riff::chunks::SubChunk* _retrieve_data_chunk(spec::MemoryMapping& m) {
    riff::chunks::Header* header = m;
    auto* subchunk = (riff::chunks::SubChunk*) &header->chunks;
    while (true) {
        if (__test_id(subchunk->type.ch_id, riff::chunks::DATA_ID)) {
            return subchunk;
        }
        else {
            size_t subchunk_size = __load_little(subchunk->type.ck_size) + 8;
            size_t padded_subchunk_size =
                 (subchunk_size % 2 == 0) ? subchunk_size : (subchunk_size + 1);
            subchunk = (riff::chunks::SubChunk*) (((uintptr_t) subchunk) +
                                                  padded_subchunk_size);
        }
    }
}

bool MappedWav::is_integer() {
    auto fmt = _retrieve_fmt_chunk(_hnd);
    riff::chunks::FormatCode fmt_tag = __load_fmt_tag(fmt->subchunk.fmt.fmt);
    return fmt_tag == riff::chunks::FormatCode::Pcm;
}
bool MappedWav::is_float() {
    auto fmt = _retrieve_fmt_chunk(_hnd);
    riff::chunks::FormatCode fmt_tag = __load_fmt_tag(fmt->subchunk.fmt.fmt);
    return fmt_tag == riff::chunks::FormatCode::IEEEFloat;
}
size_t MappedWav::get_bit_width() {
    auto fmt = _retrieve_fmt_chunk(_hnd);
    return __load_little(fmt->subchunk.fmt.bits_per_sample);
}
size_t MappedWav::get_channel_count() {
    auto fmt = _retrieve_fmt_chunk(_hnd);
    return __load_little(fmt->subchunk.fmt.channel_count);
}
size_t MappedWav::get_sampling_rate() {
    auto fmt = _retrieve_fmt_chunk(_hnd);
    return __load_little(fmt->subchunk.fmt.sampling_rate);
}
static size_t __get_sample_count(riff::chunks::SubChunk* fmt,
                                 riff::chunks::SubChunk* data) {
    size_t data_size = __load_little(data->type.ck_size);
    size_t block_size = __load_little(fmt->subchunk.fmt.block_align);
    if ((data_size % block_size) != 0) {
        logger.logw("Detected data chunk block misalignment! Dropping last "
                    "incomplete sample!");
    }
    return data_size / block_size;
}
size_t MappedWav::get_sample_count() {
    auto fmt = _retrieve_fmt_chunk(_hnd);
    auto data = _retrieve_data_chunk(_hnd);
    return __get_sample_count(fmt, data);
}

at::Tensor MappedWav::to_torch(uint32_t start, uint32_t end) {
    auto fmt = _retrieve_fmt_chunk(_hnd);
    auto data = _retrieve_data_chunk(_hnd);
    // Parse information from headers.
    riff::chunks::FormatCode fmt_tag = __load_fmt_tag(fmt->subchunk.fmt.fmt);
    size_t bit_width = __load_little(fmt->subchunk.fmt.bits_per_sample);
    size_t channel_count = __load_little(fmt->subchunk.fmt.channel_count);
    size_t block_size = __load_little(fmt->subchunk.fmt.block_align);
    size_t sample_count = __get_sample_count(fmt, data);
    uint8_t* rawdata = data->subchunk.data.data;
    // size_t rawdata_len = __load_little(data->type.ck_size);
    // Check index bounds.
    if (start > sample_count) {
        start = sample_count;
    }
    if (end > sample_count) {
        end = sample_count;
    }
    if (end < start) {
        end = start;
    }
    // Tensor storage creation.
    void* rawdata_start = rawdata + (start * block_size);
    // void* rawdata_end = rawdata + (end * block_size);
    int64_t region_sample_count = end - start;
    // at::DataPtr t_s_dp(rawdata, at::DeviceType::CPU);
    // at::Storage t_s(at::Storage::use_byte_size_t(), rawdata_len,
    //                 std::move(t_s_dp));
    // Wave format tag to pytorch dtype and other tensor options.
    at::TensorOptions tensor_opts =
         at::TensorOptions().device(at::DeviceType::CPU);
    switch (fmt_tag) {
        case riff::chunks::FormatCode::Pcm: {
            switch (bit_width) {
                case 8: {
                    tensor_opts = tensor_opts.dtype(at::ScalarType::Byte);
                } break;
                case 16: {
                    tensor_opts = tensor_opts.dtype(at::ScalarType::Short);
                } break;
                case 32: {
                    tensor_opts = tensor_opts.dtype(at::ScalarType::Int);
                } break;
                case 64: {
                    tensor_opts = tensor_opts.dtype(at::ScalarType::Long);
                } break;
                default: {
                    throw std::runtime_error(
                         "Unsupported bit width " + std::to_string(bit_width) +
                         " for creating integer tensor view.");
                } break;
            }
        } break;
        case riff::chunks::FormatCode::IEEEFloat: {
            switch (bit_width) {
                case 16: {
                    tensor_opts = tensor_opts.dtype(at::ScalarType::Half);
                } break;
                case 32: {
                    tensor_opts = tensor_opts.dtype(at::ScalarType::Float);
                } break;
                case 64: {
                    tensor_opts = tensor_opts.dtype(at::ScalarType::Double);
                } break;
                default: {
                    throw std::runtime_error(
                         "Unsupported bit width " + std::to_string(bit_width) +
                         " for creating float tensor view.");
                } break;
            }
        } break;
        default: {
            throw std::runtime_error("Unsupported format tag "
                                     "for creating tensor view.");
        } break;
    }
    // Tensor creation.
    at::Tensor t =
         torch::from_blob(
              rawdata_start,
              {region_sample_count, static_cast<int64_t>(channel_count)},
              tensor_opts)
              .view({static_cast<int64_t>(channel_count), region_sample_count});
    return t;
}

}  // namespace speu
