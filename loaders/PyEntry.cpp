#include <DataLoader.hpp>
#include <MappedWav.hpp>
#include <Musdb18hq.hpp>
#include <logger/Logger.hpp>

#include <algorithm>
#include <cctype>
#include <functional>
#include <optional>
#include <stdexcept>
#include <type_traits>
#include <vector>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <torch/python.h>
#include <torch/version.h>

static bool __iequals(const std::string& a, const std::string& b) {
    return std::equal(a.begin(), a.end(), b.begin(), b.end(),
                      [](char a, char b) { return tolower(a) == tolower(b); });
}

static spec::Logger::Level __str2level(const std::string& str) {
    if (__iequals(str, "all")) {
        return spec::Logger::Level::ALL;
    }
    else if (__iequals(str, "verbose")) {
        return spec::Logger::Level::VERBOSE;
    }
    else if (__iequals(str, "debug")) {
        return spec::Logger::Level::DEBUG;
    }
    else if (__iequals(str, "info")) {
        return spec::Logger::Level::INFO;
    }
    else if (__iequals(str, "warn")) {
        return spec::Logger::Level::WARN;
    }
    else if (__iequals(str, "error")) {
        return spec::Logger::Level::ERROR;
    }
    else if (__iequals(str, "fatal")) {
        return spec::Logger::Level::FATAL;
    }
    else if (__iequals(str, "off")) {
        return spec::Logger::Level::OFF;
    }
    else {
        throw std::invalid_argument(
             "Logger level must be one of "
             "all,verbose,debug,info,warn,error,fatal,off. Got `" +
             str + "`.");
    }
}

PYBIND11_MODULE(loader, m) {
    m.doc() = "Resource efficient wav audio loaders "
              "and augmenters for pytorch.";

    m.def(
         "configure_logger",
         [](bool console, std::string console_level,
            std::optional<std::string> file, std::string file_level) {
             const char* filename = nullptr;
             if (file) {
                 filename = file->c_str();
             }
             logger.configure(console, __str2level(console_level), filename,
                              __str2level(file_level));
         },
         pybind11::arg("enable_console") = false,
         pybind11::arg("console_level") = "off",
         pybind11::arg("log_file") = pybind11::none(),
         pybind11::arg("log_file_level") = "off");

    m.def("get_torch_version", []() {
         return std::string(TORCH_VERSION);
     }).doc() = "Returns the pytorch version this extension was built with.";

    pybind11::class_<std::filesystem::path>(m, "Path").def(
         pybind11::init<std::string>());
    pybind11::implicitly_convertible<std::string, std::filesystem::path>();

    pybind11::class_<speu::MappedWav>(m, "MappedWav")
         .def(pybind11::init<const std::filesystem::path&, bool, bool>(),
              pybind11::arg("path"), pybind11::arg("lock_header") = true,
              pybind11::arg("sequential") = false)
         .def("validate", &speu::MappedWav::validate)
         .def("to_torch", &speu::MappedWav::to_torch,
              pybind11::arg("start") = 0,
              pybind11::arg("end") = std::numeric_limits<uint32_t>::max())
         .def_property_readonly("is_integer", &speu::MappedWav::is_integer)
         .def_property_readonly("is_float", &speu::MappedWav::is_float)
         .def_property_readonly("bit_width", &speu::MappedWav::get_bit_width)
         .def_property_readonly("channel_count",
                                &speu::MappedWav::get_channel_count)
         .def_property_readonly("sampling_rate",
                                &speu::MappedWav::get_sampling_rate)
         .def_property_readonly("sample_count",
                                &speu::MappedWav::get_sample_count);

    pybind11::class_<speu::MappedWavTensor, speu::MappedWav>(m,
                                                             "MappedWavTensor")
         .def(pybind11::init<const std::filesystem::path&, bool, bool>(),
              pybind11::arg("path"), pybind11::arg("lock_header") = true,
              pybind11::arg("sequential") = false)
         .def_property_readonly("tensor", &speu::MappedWavTensor::get_tensor);

    auto musdb18hq = m.def_submodule(
         "musdb18hq", "Dataset loader and augmenters for musdb18hq"
                      "and similarly structured datasets.");

    pybind11::class_<speu::musdb18hq::Sample>(musdb18hq, "Sample")
         .def_property_readonly("a", &speu::musdb18hq::Sample::a)
         .def_property_readonly("b", &speu::musdb18hq::Sample::b)
         .def_property_readonly("c", &speu::musdb18hq::Sample::c)
         .def_property_readonly("sgn_a", &speu::musdb18hq::Sample::sgn_a)
         .def_property_readonly("sgn_b", &speu::musdb18hq::Sample::sgn_b)
         .def_static(
              "collate",
              pybind11::overload_cast<std::vector<speu::musdb18hq::Sample>&,
                                      bool>(&speu::musdb18hq::Sample::collate),
              pybind11::arg("tensors"), pybind11::arg("pin_memory") = false);

    pybind11::class_<speu::musdb18hq::Sample::Loader>(musdb18hq, "SampleLoader")
         .def_property_readonly("name",
                                &speu::musdb18hq::Sample::Loader::get_name)
         .def_property_readonly(
              "track_count", &speu::musdb18hq::Sample::Loader::get_track_count)
         .def_property_readonly(
              "channel_count",
              &speu::musdb18hq::Sample::Loader::get_channel_count)
         .def_property_readonly(
              "sample_count",
              &speu::musdb18hq::Sample::Loader::get_sample_count)
         .def_property_readonly("mixture",
                                [](const speu::musdb18hq::Sample::Loader& o)
                                     -> std::optional<at::Tensor> {
                                    if (o.has_mixture()) {
                                        return {o.get_mixture()};
                                    }
                                    return {};
                                })
         .def_property_readonly("bass",
                                [](const speu::musdb18hq::Sample::Loader& o)
                                     -> std::optional<at::Tensor> {
                                    if (o.has_bass()) {
                                        return {o.get_bass()};
                                    }
                                    return {};
                                })
         .def_property_readonly("drums",
                                [](const speu::musdb18hq::Sample::Loader& o)
                                     -> std::optional<at::Tensor> {
                                    if (o.has_drums()) {
                                        return {o.get_drums()};
                                    }
                                    return {};
                                })
         .def_property_readonly("vocals",
                                [](const speu::musdb18hq::Sample::Loader& o)
                                     -> std::optional<at::Tensor> {
                                    if (o.has_vocals()) {
                                        return {o.get_vocals()};
                                    }
                                    return {};
                                })
         .def_property_readonly("other",
                                [](const speu::musdb18hq::Sample::Loader& o)
                                     -> std::optional<at::Tensor> {
                                    if (o.has_other()) {
                                        return {o.get_other()};
                                    }
                                    return {};
                                })
         .def("__str__", [](const speu::musdb18hq::Sample::Loader& obj) {
             return "SampleLoader(" + obj.get_name() + ')';
         });

    pybind11::class_<speu::musdb18hq::Augmenter::Instance>(
         musdb18hq, "AugmentationInstance")
         .def("__str__",
              [](const speu::musdb18hq::Augmenter::Instance& obj) {
                  return obj.pretty_string();
              })
         .def_static("calc_first_track_mean",
                     &speu::musdb18hq::Augmenter::Instance::mean_first_track)
         .def_static("calc_first_track_var",
                     &speu::musdb18hq::Augmenter::Instance::var_first_track)
         .def_static("calc_second_track_mean",
                     &speu::musdb18hq::Augmenter::Instance::mean_second_track)
         .def_static("calc_second_track_var",
                     &speu::musdb18hq::Augmenter::Instance::var_second_track)
         .def_static("calc_channel_mean",
                     &speu::musdb18hq::Augmenter::Instance::mean_channel)
         .def_static("calc_channel_var",
                     &speu::musdb18hq::Augmenter::Instance::var_channel)
         .def_static(
              "calc_resample_distortion_mean",
              &speu::musdb18hq::Augmenter::Instance::mean_resample_distortion)
         .def_static(
              "calc_resample_distortion_var",
              &speu::musdb18hq::Augmenter::Instance::var_resample_distortion)
         .def_static(
              "calc_position_offset_mean",
              &speu::musdb18hq::Augmenter::Instance::mean_position_offset)
         .def_static("calc_position_offset_var",
                     &speu::musdb18hq::Augmenter::Instance::var_position_offset)
         .def_static(
              "calc_gain_distortion_mean",
              &speu::musdb18hq::Augmenter::Instance::mean_gain_distortion)
         .def_static(
              "calc_gain_distortion_var",
              &speu::musdb18hq::Augmenter::Instance::var_gain_distortion);

    pybind11::class_<speu::musdb18hq::Augmenter>(musdb18hq, "Augmenter")
         .def("make_instance", &speu::musdb18hq::Augmenter::make_instance,
              pybind11::arg("epoch"), pybind11::arg("sample_index"))
         .def("apply", &speu::musdb18hq::Augmenter::apply,
              pybind11::arg("loader"), pybind11::arg("instance"));

    pybind11::class_<speu::musdb18hq::Dataset>(musdb18hq, "Dataset")
         .def(pybind11::init<const std::filesystem::path&, size_t, size_t,
                             bool>(),
              pybind11::arg("root"), pybind11::arg("sample_count"),
              pybind11::arg("seed_offset") = 0,
              pybind11::arg("no_distort") = false)
         .def("__len__", &speu::musdb18hq::Dataset::length)
         .def_property_readonly("augmenter",
                                &speu::musdb18hq::Dataset::get_augmenter)
         .def("__getitem__", &speu::musdb18hq::Dataset::get_loader,
              pybind11::return_value_policy::reference_internal)
         .def("get",
              pybind11::overload_cast<size_t, size_t>(
                   &speu::musdb18hq::Dataset::get),
              pybind11::arg("index"), pybind11::arg("epoch"))
         .def("get",
              pybind11::overload_cast<
                   size_t, const speu::musdb18hq::Augmenter::Instance&>(
                   &speu::musdb18hq::Dataset::get),
              pybind11::arg("index"), pybind11::arg("augmentation_instance"))
         .def("__getitem__",
              [](speu::musdb18hq::Dataset& obj,
                 std::tuple<size_t, size_t> args) {
                  auto [epoch, index] = args;
                  return obj.get(epoch, index);
              })
         .def("__getitem__",
              [](speu::musdb18hq::Dataset& obj,
                 std::tuple<size_t, const speu::musdb18hq::Augmenter::Instance&>
                      args) {
                  auto [index, augmentation_instance] = args;
                  return obj.get(index, augmentation_instance);
              });

    pybind11::class_<speu::AugmentationSampler>(musdb18hq, "Sampler")
         .def(pybind11::init<const speu::musdb18hq::Augmenter&>(),
              pybind11::arg("augmenter"))
         .def(pybind11::init<speu::musdb18hq::Dataset&>(),
              pybind11::arg("dataset"))
         .def("__len__", &speu::AugmentationSampler::length)
         .def("prepare_epoch", &speu::AugmentationSampler::prepare_epoch)
         .def("get", &speu::AugmentationSampler::get, pybind11::arg("index"))
         .def("__getitem__", &speu::AugmentationSampler::get)
         .def("get_shuffled_indexes",
              &speu::AugmentationSampler::get_shuffled_indexes)
         .def("get_instances", &speu::AugmentationSampler::get_instances);

    pybind11::class_<speu::DataLoader>(musdb18hq, "DataLoader")
         .def(pybind11::init<speu::musdb18hq::Dataset&, int32_t, size_t,
                             int32_t, bool>(),
              pybind11::arg("dataset"), pybind11::arg("batch_size"),
              pybind11::arg("batch_queue_size") = 3,
              pybind11::arg("worker_thread_count") = -1,
              pybind11::arg("pin_memory") = false)
         .def(pybind11::init<speu::musdb18hq::Dataset&, int32_t,
                             speu::AugmentationSampler&, size_t, int32_t,
                             bool>(),
              pybind11::arg("dataset"), pybind11::arg("batch_size"),
              pybind11::arg("sampler"), pybind11::arg("batch_queue_size") = 3,
              pybind11::arg("worker_thread_count") = -1,
              pybind11::arg("pin_memory") = false)
         .def("__len__", &speu::DataLoader::get_batch_count)
         .def_property_readonly("length", &speu::DataLoader::length)
         .def_property_readonly("dataset", &speu::DataLoader::get_dataset)
         .def_property_readonly("sampler", &speu::DataLoader::get_sampler)
         .def_property_readonly("batch_size", &speu::DataLoader::get_batch_size)
         .def_property_readonly("worker_count",
                                &speu::DataLoader::get_worker_count)
         .def_property_readonly("batch_queue_size",
                                &speu::DataLoader::get_batch_queue_size)
         .def_property_readonly("sample_queue_size",
                                &speu::DataLoader::get_sample_queue_size)
         .def_property_readonly("epoch", &speu::DataLoader::get_epoch)
         .def_property_readonly("iteration", &speu::DataLoader::get_iteration)
         .def_property_readonly("has_reached_epoch_end",
                                &speu::DataLoader::has_reached_epoch_end)
         .def_property_readonly("is_work_fully_posted",
                                &speu::DataLoader::is_work_fully_posted)
         .def_property_readonly("is_done_loading",
                                &speu::DataLoader::is_done_loading)
         .def("get_next_batch", &speu::DataLoader::get_next_batch)
         .def("next_epoch", &speu::DataLoader::next_epoch)
         .def("__iter__",
              [](speu::DataLoader& o) -> speu::DataLoader& {
                  o.next_epoch();
                  return o;
              })
         .def("__next__", [](speu::DataLoader& o) {
             if (o.has_reached_epoch_end()) [[unlikely]] {
                 throw pybind11::stop_iteration();
             }
             return o.get_next_batch();
         });
}
