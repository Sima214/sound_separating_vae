#ifndef SPEU_MUSDB18HQ_HPP
#define SPEU_MUSDB18HQ_HPP
/**
 * @file
 * @brief Augmenting loader for the musdb18hq dataset.
 */

#include <MappedWav.hpp>
#include <Utils.hpp>

#include <bit>
#include <cstddef>
#include <cstdint>
#include <filesystem>
#include <optional>
#include <ostream>
#include <span>
#include <utility>

#include <ATen/Generator.h>
#include <ATen/Tensor.h>
#include <torch/data.h>
#include <torch/types.h>

namespace speu::musdb18hq {

class Sample {
   public:
    class Loader : public spec::INonCopyable {
       public:
        enum class TrackFlagBits : uint_fast8_t {
            Mixture = MASK_CREATE(0),
            Bass = MASK_CREATE(1),
            Drums = MASK_CREATE(2),
            Vocals = MASK_CREATE(3),
            Other = MASK_CREATE(4)
        };
        class TrackFlag {
           protected:
            uint_fast8_t _v;

           public:
            TrackFlag() : _v(0) {}
            TrackFlag(uint_fast8_t v) : _v(v) {}
            TrackFlag(TrackFlagBits f) : _v(static_cast<uint_fast8_t>(f)) {}

            TrackFlag operator|(TrackFlagBits f) const {
                return TrackFlag(_v | static_cast<uint_fast8_t>(f));
            }
            TrackFlag operator&(TrackFlagBits f) const {
                return TrackFlag(_v & static_cast<uint_fast8_t>(f));
            }
            TrackFlag operator^(TrackFlagBits f) const {
                return TrackFlag(_v ^ static_cast<uint_fast8_t>(f));
            }
            TrackFlag operator~() const {
                return TrackFlag(~_v);
            }

            TrackFlag& operator|=(TrackFlagBits f) {
                _v |= static_cast<uint_fast8_t>(f);
                return *this;
            }
            TrackFlag& operator&=(TrackFlagBits f) {
                _v &= static_cast<uint_fast8_t>(f);
                return *this;
            }
            TrackFlag& operator^=(TrackFlagBits f) {
                _v ^= static_cast<uint_fast8_t>(f);
                return *this;
            }

            bool operator==(TrackFlagBits f) const {
                return _v == static_cast<uint_fast8_t>(f);
            }
            bool operator!=(TrackFlagBits f) const {
                return _v != static_cast<uint_fast8_t>(f);
            }

            operator TrackFlagBits() const {
                return static_cast<TrackFlagBits>(_v);
            }
            operator bool() const {
                return _v != 0;
            }
            int count() const {
                return std::popcount(_v);
            }
            /** Get n-th set flag. */
            TrackFlagBits operator[](size_t) const;
        };

       protected:
        /** Sample's name. */
        std::string _name;
        std::optional<speu::MappedWavTensor> _mixture;
        std::optional<speu::MappedWavTensor> _bass;
        std::optional<speu::MappedWavTensor> _drums;
        std::optional<speu::MappedWavTensor> _vocals;
        std::optional<speu::MappedWavTensor> _other;
        int16_t _channel_count;
        int64_t _sample_count : 48;

       public:
        Loader(const std::filesystem::path& root);
        Loader(Loader&& o) :
            _name(std::move(o._name)), _mixture(std::exchange(o._mixture, {})),
            _bass(std::exchange(o._bass, {})),
            _drums(std::exchange(o._drums, {})),
            _vocals(std::exchange(o._vocals, {})),
            _other(std::exchange(o._other, {})),
            _channel_count(o._channel_count), _sample_count(o._sample_count) {}
        Loader& operator=(Loader&& o) {
            _name = std::move(o._name);
            _mixture = std::exchange(o._mixture, {});
            _bass = std::exchange(o._bass, {});
            _drums = std::exchange(o._drums, {});
            _vocals = std::exchange(o._vocals, {});
            _other = std::exchange(o._other, {});
            _channel_count = o._channel_count;
            _sample_count = o._sample_count;
            return *this;
        }
        ~Loader();

        const std::string& get_name() const {
            return _name;
        }

        bool has_mixture() const {
            return _mixture.has_value();
        }
        bool has_bass() const {
            return _bass.has_value();
        }
        bool has_drums() const {
            return _drums.has_value();
        }
        bool has_vocals() const {
            return _vocals.has_value();
        }
        bool has_other() const {
            return _other.has_value();
        }

        at::Tensor get_mixture() const {
            return _mixture->get_tensor();
        }
        at::Tensor get_bass() const {
            return _bass->get_tensor();
        }
        at::Tensor get_drums() const {
            return _drums->get_tensor();
        }
        at::Tensor get_vocals() const {
            return _vocals->get_tensor();
        }
        at::Tensor get_other() const {
            return _other->get_tensor();
        }

        bool has(TrackFlagBits track_enum) const;
        at::Tensor get(TrackFlagBits track_enum) const;

        /** Calculates the amount of valid track variations. */
        size_t get_track_count() const;
        TrackFlag get_track_mask() const;
        size_t get_channel_count() const {
            return _channel_count;
        }
        size_t get_sample_count() const {
            return _sample_count;
        }
    };

   protected:
    at::Tensor _a;
    at::Tensor _b;
    at::Tensor _c;
    at::Tensor _mix_factors;

   public:
    Sample() = default;
    Sample(at::Tensor a, at::Tensor b, at::Tensor c, at::Tensor m);

    // Sample(const Sample&) = default;
    // Sample& operator=(const Sample&) = default;
    // Sample& operator=(Sample&) = default;
    // Sample(Sample&&) = default;
    // Sample& operator=(Sample&&) = default;
    // ~Sample() = default;

    at::Tensor a() const {
        return _a;
    }
    at::Tensor b() const {
        return _b;
    }
    at::Tensor c() const {
        return _c;
    }

    at::Tensor sgn_a() const;
    at::Tensor sgn_b() const;

    size_t get_batch_size() const;
    operator bool() {
        return _a.defined() && _b.defined() && _c.defined() &&
               _mix_factors.defined();
    }

    static Sample collate(std::vector<Sample>& samples, bool pin_memory) {
        return collate(std::span<Sample>(samples.begin(), samples.end()), pin_memory);
    }
    static Sample collate(std::span<Sample> samples, bool pin_memory);
};

inline std::ostream& operator<<(std::ostream& o, Sample::Loader::TrackFlagBits v);

class Augmenter : public spec::INonCopyable {
   public:
    class Instance {
       protected:
        float _first_track = 0;
        float _second_track = 0;
        float _channel = 0;
        /** Relative scaling to sampling rate. In the range of [-3, 3]. */
        float _resample_distortion = 0.0f;
        /** Uniform variable for start position relative offset. In the range of
         * [0, 1). */
        float _position_offset = 0.0;
        /** Gain adjustment in decibels. In the range of [-12, +3] */
        float _gain_distortion = 0.0f;

       public:
        /** Use default values. */
        Instance();
        /** Use random values. */
        Instance(at::Generator rng, bool no_distort);

        Sample::Loader::TrackFlagBits get_first_track(
             const Sample::Loader& loader) const;
        Sample::Loader::TrackFlagBits get_second_track(
             const Sample::Loader& loader,
             Sample::Loader::TrackFlag first_track) const;

        int16_t get_channel(const Sample::Loader& loader) const;
        /**
         * Get sample count that will be read, before resample distortion is
         * applied.
         */
        size_t get_raw_sample_count(size_t output_sample_count) const;
        size_t get_start_offset(const Sample::Loader& loader,
                                size_t raw_sample_count) const;
        float get_gain_distortion() const {
            return _gain_distortion;
        }

        /*
         * Methods to aid checking the random
         * distribution of the generated instances.
         */
        static float mean_first_track(std::vector<Instance>& l);
        static float var_first_track(std::vector<Instance>& l);
        static float mean_second_track(std::vector<Instance>& l);
        static float var_second_track(std::vector<Instance>& l);
        static float mean_channel(std::vector<Instance>& l);
        static float var_channel(std::vector<Instance>& l);
        static float mean_resample_distortion(std::vector<Instance>& l);
        static float var_resample_distortion(std::vector<Instance>& l);
        static float mean_position_offset(std::vector<Instance>& l);
        static float var_position_offset(std::vector<Instance>& l);
        static float mean_gain_distortion(std::vector<Instance>& l);
        static float var_gain_distortion(std::vector<Instance>& l);

        /* User interfacing */
        std::string pretty_string() const;
    };

   protected:
    /** Dataset size */
    size_t _sample_count;
    /** Dataset sample shape. */
    size_t _element_count;
    /** Deterministically change randomness between instances. */
    size_t _seed_offset = 0;
    bool _no_distort = false;

   public:
    Augmenter() = default;
    Augmenter(size_t sample_count, size_t element_count, size_t seed_offset, bool no_distort);
    Augmenter(Augmenter&& o) = default;
    Augmenter& operator=(Augmenter&& o) = default;

    size_t length() const {
        return _sample_count;
    }
    size_t shape() const {
        return _element_count;
    }
    at::Generator make_random_generator(uint64_t index) const;

    Instance make_instance(size_t epoch, size_t sample_index) const;
    Sample apply(Sample::Loader& loader,
                 const Instance& augmentation_instance) const;
};

class Dataset : public spec::INonCopyable {
   protected:
    std::vector<Sample::Loader> _loaders;
    Augmenter _augmenter;

   public:
    Dataset(const std::filesystem::path& root, size_t element_count,
            size_t seed_offset = 0, bool no_distort = false);
    Dataset(Dataset&& o) :
        _loaders(std::move(o._loaders)), _augmenter(std::move(o._augmenter)) {}
    Dataset& operator=(Dataset&& o) {
        _loaders = std::move(o._loaders);
        _augmenter = std::move(o._augmenter);
        return *this;
    }

    size_t length() const {
        return _loaders.size();
    }
    const Augmenter& get_augmenter() const {
        return _augmenter;
    }

    Sample::Loader& get_loader(size_t index);
    Sample get(size_t index, size_t epoch);
    Sample get(size_t index, const Augmenter::Instance& augmentation_instance);

    Sample::Loader& operator[](size_t i) {
        return get_loader(i);
    }
};

}  // namespace speu::musdb18hq

#endif /*SPEU_MUSDB18HQ_HPP*/