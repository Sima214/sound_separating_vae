from models.vae1 import AETrainer as Trainer

import pytorch_lightning as pl
import torch.optim as optim
import torch.nn as nn
import torchaudio
import torch

from pathlib import Path
import transforms
import loader

transforms.configure_logger(True, 'info')
loader.configure_logger(True, 'warn', 'loader.log', 'all')

GRADIENT_CLIP_VALUE = 1e6
TRAIN_SAMPLE_LENGTH = 32*1024
TRAIN_BATCH_SIZE = 5
TEST_SAMPLE_LENGTH = 32*1024
TEST_BATCH_SIZE = 5


class VisualizationSample:
    def __init__(self, path: Path):
        self._name = path.stem
        self._wav = loader.MappedWavTensor(str(path), sequential=True)
        if self._wav.channel_count != 1:
            raise ValueError("Wav file for visualization must be mono!")
        if self._wav.sample_count % (TEST_BATCH_SIZE * TEST_SAMPLE_LENGTH) != 0:
            raise ValueError("Wav file for visualization must be at "
                             f"multiples of {TEST_BATCH_SIZE * TEST_SAMPLE_LENGTH} samples!")

    @property
    def name(self):
        return self._name

    @property
    def sampling_rate(self):
        return self._wav.sampling_rate

    def get_batches(self):
        batch_count = self._wav.sample_count // (TEST_BATCH_SIZE * TEST_SAMPLE_LENGTH)
        batches = transforms.scale_cast(self._wav.tensor, torch.float32).view((batch_count, TEST_BATCH_SIZE, TEST_SAMPLE_LENGTH))
        return [batches[i].pin_memory() for i in range(batch_count)]


class Visualizer(pl.callbacks.Callback):
    def __init__(self, samples=[], every_n_epochs=4):
        super().__init__()
        self.every_n_epochs = every_n_epochs
        self.samples = samples

    @torch.no_grad()
    def on_validation_end(self, trainer: pl.Trainer, module):
        if trainer.current_epoch % self.every_n_epochs == 0:
            for i, sample in enumerate(self.samples):
                module.visualize_step(sample, i, trainer.logger, step=trainer.global_step)


class ParallelLightTrainer(pl.LightningModule):
    def __init__(self, models, quantization_config=None):
        super().__init__()
        self.automatic_optimization = False
        if quantization_config is not None:
            for i in range(len(models)):
                models[i].qconfig = quantization_config
                models[i] = torch.quantization.prepare_qat(
                    models[i], inplace=True)
        self.models = nn.ModuleDict([(m.name, m) for m in models])
        for model in self.models.values():
            model.automatic_optimization = False
        # TODO: streamline
        self.compile = False
        self.first_run = True

    def training_step(self, sample, batch_idx):
        self.compile_models()
        opts = self.optimizers()
        if not isinstance(opts, list):
            opts = [opts]
        for i, model in enumerate(self.models.values()):
            try:
                opt = opts[i]
                opt.zero_grad()
                l = model.training_step(sample, batch_idx, self.log)
                self.manual_backward(l)
                # self.clip_gradients(opt, gradient_clip_val=GRADIENT_CLIP_VALUE,
                #                     gradient_clip_algorithm='norm')
                with torch.no_grad():
                    total_grads = torch.zeros(
                        1, dtype=torch.float64, device=model.device)
                    for param in model.parameters():
                        if param.grad is not None:
                            total_grads += param.grad.abs().sum()
                opt.step()
                self.log(model.name + "/grads", total_grads)
            except Exception as e:
                print(
                    f"The following error occured while training '{model.name}':")
                raise e

    def validation_step(self, sample, batch_idx):
        self.compile_models()
        for model in self.models.values():
            model.validation_step(sample, batch_idx, self.log)

    def visualize_step(self, sample, sample_idx, logger, step):
        self.compile_models()
        for model in self.models.values():
            model.visualize_step(sample, sample_idx, logger, step)

    def configure_optimizers(self):
        opts = []
        for model in self.models.values():
            opts.append(model.configure_optimizers())
        return opts

    def compile_models(self):
        if self.first_run:
            self.first_run = False
            if self.compile:
                for model in self.models.values():
                    print(f"Compiling model `{model.name}`...")
                    model.compile()


if __name__ == '__main__':
    seed_offset = 346
    trainset = loader.musdb18hq.Dataset(
        "musdb18hq/train", TRAIN_SAMPLE_LENGTH,
        seed_offset=seed_offset, no_distort=False
    )
    validset = loader.musdb18hq.Dataset(
        "musdb18hq/test", TEST_SAMPLE_LENGTH,
        seed_offset=seed_offset + 9999999, no_distort=True
    )

    # TODO: deadlock behaviour when using 1 thread and/or less then 3 prefetch slots.
    trainloader = loader.musdb18hq.DataLoader(
        trainset, batch_size=TRAIN_BATCH_SIZE, worker_thread_count=2, pin_memory=True)
    validloader = loader.musdb18hq.DataLoader(
        validset, batch_size=TEST_BATCH_SIZE, worker_thread_count=3, pin_memory=True)

    # torch.backends.quantized.engine = 'onednn'
    # quant_config = torch.quantization.get_default_qat_qconfig('onednn')

    models = ParallelLightTrainer([
        Trainer(
            'ae1', model_seed=457434674,
            checkpointing=False,
            loss_args={'a': 0.6, 'b': 0.4, 'b_a': 0.0},
            optim_class=optim.Adam, optim_args={'lr': 1e-3}
        ),
        Trainer(
            'ae1', model_seed=457434674,
            checkpointing=False,
            loss_args={'a': 0.6, 'b': 0.4, 'b_a': 0.0},
            optim_class=optim.Adam, optim_args={'lr': 1e-4} # Winner for vae1
        ),
        # Trainer(
        #     'ae1', model_seed=3456934679,
        #     checkpointing=False,
        #     loss_args={'a': 0.6, 'b': 0.4, 'b_a': 0.0},
        #     optim_class=optim.Adam, optim_args={'lr': 1e-3}
        # ),
        # Trainer(
        #     'ae1', model_seed=3456934679,
        #     checkpointing=False,
        #     loss_args={'a': 0.6, 'b': 0.4, 'b_a': 0.0},
        #     optim_class=optim.Adam, optim_args={'lr': 1e-4}
        # ),
    ])

    checkpointer = pl.callbacks.ModelCheckpoint(
        save_top_k=-1, every_n_epochs=20, filename="ae1_{epoch:05d}",
        save_weights_only=False, save_on_train_epoch_end=False
    )
    visualizations_dir = Path('musdb18hq/visualizations')
    visualizations_list = [VisualizationSample(wav_fn) for wav_fn in visualizations_dir.iterdir()]
    visualizer = Visualizer(visualizations_list, every_n_epochs=20)
    trainer = pl.Trainer(
        callbacks=[checkpointer, visualizer], detect_anomaly=True, max_epochs=-1,
        default_root_dir='artifacts/astral2', log_every_n_steps=20,
        check_val_every_n_epoch=1, num_sanity_val_steps=0
    )
    trainer.fit(model=models, train_dataloaders=trainloader,
                val_dataloaders=validloader)
