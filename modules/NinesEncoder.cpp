#include "Nines.hpp"

namespace speu::modules::nines {

static torch::nn::Conv1dOptions __make_encoder_conv_options(
     int64_t input_channels, int64_t output_channels) {
    return torch::nn::Conv1dOptions(input_channels, output_channels, 9)
         .bias(true)
         .padding(3)
         .padding_mode(torch::kReflect);
}

static torch::nn::Conv1dOptions __make_encoder_pointwise_options(
     int64_t input_channels, int64_t output_channels) {
    return torch::nn::Conv1dOptions(input_channels, output_channels, 1)
         .bias(true);
}

static torch::nn::AvgPool1dOptions __make_encoder_avg_pool_options(
     int64_t scale) {
    return torch::nn::AvgPool1dOptions(scale).stride(scale);
}

static torch::nn::PReLUOptions __make_encoder_activation_options(
     int64_t output_channels) {
    return torch::nn::PReLUOptions().num_parameters(output_channels);
}

EncoderImpl::EncoderImpl(int64_t input_channels, int64_t internal_channels,
                         int64_t output_channels, int64_t scale) :
    _conv(register_module("conv", torch::nn::Conv1d(__make_encoder_conv_options(
                                       input_channels, internal_channels)))),
    _pointwise(register_module(
         "pointwise", torch::nn::Conv1d(__make_encoder_pointwise_options(
                           internal_channels, output_channels)))),
    _act(register_module(
         "activation",
         torch::nn::PReLU(__make_encoder_activation_options(output_channels)))),
    _pool(register_module(
         "pool",
         torch::nn::AvgPool1d(__make_encoder_avg_pool_options(scale)))) {}

torch::Tensor EncoderImpl::forward(torch::Tensor x) {
    auto y = _conv(x);
    y = _act(_pointwise(y));
    return _pool(y);
}

}  // namespace speu::modules::nines
