#include <Nines.hpp>
#include <logger/Logger.hpp>

#include <algorithm>
#include <cctype>
#include <stdexcept>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <torch/python.h>
#include <torch/version.h>

static bool __iequals(const std::string& a, const std::string& b) {
    return std::equal(
         a.begin(), a.end(), b.begin(), b.end(),
         [](char a, char b) { return std::tolower(a) == std::tolower(b); });
}

static spec::Logger::Level __str2level(const std::string& str) {
    if (__iequals(str, "all")) {
        return spec::Logger::Level::ALL;
    }
    else if (__iequals(str, "verbose")) {
        return spec::Logger::Level::VERBOSE;
    }
    else if (__iequals(str, "debug")) {
        return spec::Logger::Level::DEBUG;
    }
    else if (__iequals(str, "info")) {
        return spec::Logger::Level::INFO;
    }
    else if (__iequals(str, "warn")) {
        return spec::Logger::Level::WARN;
    }
    else if (__iequals(str, "error")) {
        return spec::Logger::Level::ERROR;
    }
    else if (__iequals(str, "fatal")) {
        return spec::Logger::Level::FATAL;
    }
    else if (__iequals(str, "off")) {
        return spec::Logger::Level::OFF;
    }
    else {
        throw std::invalid_argument(
             "Logger level must be one of "
             "all,verbose,debug,info,warn,error,fatal,off. Got `" +
             str + "`.");
    }
}

PYBIND11_MODULE(modules, m) {
    m.doc() = "Base modules/blocks for building sound VAEs.";

    m.def(
         "configure_logger",
         [](bool console, std::string console_level,
            std::optional<std::string> file, std::string file_level) {
             const char* filename = nullptr;
             if (file) {
                 filename = file->c_str();
             }
             logger.configure(console, __str2level(console_level), filename,
                              __str2level(file_level));
         },
         pybind11::arg("enable_console") = false,
         pybind11::arg("console_level") = "off",
         pybind11::arg("log_file") = pybind11::none(),
         pybind11::arg("log_file_level") = "off");

    m.def("get_torch_version", []() {
         return std::string(TORCH_VERSION);
     }).doc() = "Returns the pytorch version this extension was built with.";

    // torch::python::init_bindings();
    py::class_<torch::nn::Module>(m, "_Module");

    auto nines = m.def_submodule("nines");
    speu::modules::nines::_register_modules(nines);
}
