#ifndef SPEU_NINES_HPP
#define SPEU_NINES_HPP
/**
 * @file
 * @brief Basic modules centered around convolutions with kernel size 9.
 * TODO: test with activation per conv.
 */

#include <memory>

#include <pybind11/pybind11.h>
#include <torch/nn/module.h>
#include <torch/nn/modules/activation.h>
#include <torch/nn/modules/conv.h>
#include <torch/nn/modules/pooling.h>
#include <torch/nn/modules/upsampling.h>

namespace speu::modules::nines {

class EncoderImpl : public torch::nn::Module {
   protected:
    torch::nn::Conv1d _conv;
    torch::nn::Conv1d _pointwise;
    torch::nn::PReLU _act;
    torch::nn::AvgPool1d _pool;

   public:
    EncoderImpl(int64_t input_channels, int64_t internal_channels,
                int64_t output_channels, int64_t scale = 2);

    torch::Tensor forward(torch::Tensor x);
};

class DecoderImpl : public torch::nn::Module {
   protected:
    torch::nn::Upsample _upsample;
    torch::nn::Conv1d _pointwise;
    torch::nn::Conv1d _conv;
    torch::nn::PReLU _act;

   public:
    DecoderImpl(int64_t input_channels, int64_t internal_channels,
                int64_t output_channels, int64_t scale = 2);

    torch::Tensor forward(torch::Tensor x);
};

std::pair<std::shared_ptr<EncoderImpl>, std::shared_ptr<DecoderImpl>> make_pair(
     int64_t input_channels, int64_t internal_channels, int64_t output_channels,
     int64_t scale = 2);

/**
 * Library internal function used to register the above pytorch modules with
 * pybind11.
 */
void _register_modules(pybind11::module& m);

}  // namespace speu::modules::nines

#endif /*SPEU_NINES_HPP*/