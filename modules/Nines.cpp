#include "Nines.hpp"

#include <memory>

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <torch/python.h>

namespace speu::modules::nines {

std::pair<std::shared_ptr<EncoderImpl>, std::shared_ptr<DecoderImpl>> make_pair(
     int64_t input_channels, int64_t internal_channels, int64_t output_channels,
     int64_t scale) {
    return {std::make_shared<EncoderImpl>(input_channels, internal_channels,
                                          output_channels, scale),
            std::make_shared<DecoderImpl>(output_channels, internal_channels,
                                          input_channels, scale)};
}

void _register_modules(pybind11::module& m) {
    torch::python::bind_module<EncoderImpl>(m, "Encoder")
         .def(py::init<int64_t, int64_t, int64_t, int64_t>(),
              pybind11::arg("input_channels"),
              pybind11::arg("internal_channels"),
              pybind11::arg("output_channels"), pybind11::arg("scale") = 2);
    torch::python::bind_module<DecoderImpl>(m, "Decoder")
         .def(py::init<int64_t, int64_t, int64_t, int64_t>(),
              pybind11::arg("input_channels"),
              pybind11::arg("internal_channels"),
              pybind11::arg("output_channels"), pybind11::arg("scale") = 2);

    m.def("make_pair", &make_pair, pybind11::arg("input_channels"),
          pybind11::arg("internal_channels"), pybind11::arg("output_channels"),
          pybind11::arg("scale") = 2);
}

}  // namespace speu::modules::nines