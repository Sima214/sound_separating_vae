#include "Nines.hpp"

namespace speu::modules::nines {

static torch::nn::UpsampleOptions __make_decoder_upsample_options(
     int64_t scale) {
    return torch::nn::UpsampleOptions()
         .scale_factor(std::vector({(double) scale}))
         .mode(torch::kLinear)
         .align_corners(false);
}

static torch::nn::Conv1dOptions __make_decoder_pointwise_options(
     int64_t input_channels, int64_t output_channels) {
    return torch::nn::Conv1dOptions(input_channels, output_channels, 1)
         .bias(true);
}

static torch::nn::Conv1dOptions __make_decoder_conv_options(
     int64_t input_channels, int64_t output_channels) {
    return torch::nn::Conv1dOptions(input_channels, output_channels, 9)
         .bias(true)
         .padding(3)
         .padding_mode(torch::kReflect);
}

static torch::nn::PReLUOptions __make_decoder_activation_options(
     int64_t output_channels) {
    return torch::nn::PReLUOptions().num_parameters(output_channels);
}

DecoderImpl::DecoderImpl(int64_t input_channels, int64_t internal_channels,
                         int64_t output_channels, int64_t scale) :
    _upsample(register_module(
         "upsample",
         torch::nn::Upsample(__make_decoder_upsample_options(scale)))),
    _pointwise(register_module(
         "pointwise", torch::nn::Conv1d(__make_decoder_pointwise_options(
                           input_channels, internal_channels)))),
    _conv(register_module("conv", torch::nn::Conv1d(__make_decoder_conv_options(
                                       internal_channels, output_channels)))),
    _act(register_module("activation",
                         torch::nn::PReLU(__make_decoder_activation_options(
                              output_channels)))) {}

torch::Tensor DecoderImpl::forward(torch::Tensor x) {
    auto y = _upsample(x);
    y = _pointwise(y);
    y = _act(_conv(y));
    return y;
}

}  // namespace speu::modules::nines