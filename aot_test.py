import models.nines as nines
import functorch.compile as compile
import auraloss as audioloss
import torch

# encoder, decoder = nines.make_pair(3, 12, 8)
# encoder_aot = compile.aot_module(encoder, fw_compiler=compile.print_compile, bw_compiler=compile.print_compile, partition_fn=compile.min_cut_rematerialization_partition)
# decoder_aot = compile.aot_module(decoder, fw_compiler=compile.print_compile, bw_compiler=compile.print_compile, partition_fn=compile.min_cut_rematerialization_partition)
loss = compile.aot_module(
    audioloss.freq.STFTLoss(),
    fw_compiler=compile.print_compile,
    bw_compiler=compile.print_compile,
    partition_fn=compile.min_cut_rematerialization_partition
)

x = torch.rand((5, 1, 32768))
m = torch.nn.Conv1d(1, 1, 3, padding=1)
y = m(x)

l = loss(x, y)
l.backward()


# Testing Nine's prototype Encoder/Decoder modules with default compile:

# def forward(self, primals_1, primals_2, primals_3, primals_4, primals_5, primals_6):
#     reflection_pad1d = torch.ops.aten.reflection_pad1d.default(primals_6, [4, 4]);  primals_6 = None
#     convolution = torch.ops.aten.convolution.default(reflection_pad1d, primals_1, primals_2, [1], [0], [1], False, [0], 1);  primals_2 = None
#     convolution_1 = torch.ops.aten.convolution.default(convolution, primals_3, primals_4, [1], [0], [1], False, [0], 1);  primals_4 = None
#     prelu = torch.ops.aten.prelu.default(convolution_1, primals_5)
#     unsqueeze = torch.ops.aten.unsqueeze.default(prelu, -2);  prelu = None
#     avg_pool2d = torch.ops.aten.avg_pool2d.default(unsqueeze, [1, 2], [1, 2])
#     squeeze = torch.ops.aten.squeeze.dim(avg_pool2d, -2);  avg_pool2d = None
#     return [squeeze, reflection_pad1d, primals_5, unsqueeze, convolution_1, primals_1, convolution, primals_3]
# 
# def forward(self, reflection_pad1d, primals_5, unsqueeze, convolution_1, primals_1, convolution, primals_3, tangents_1):
#     unsqueeze_1 = torch.ops.aten.unsqueeze.default(tangents_1, 2);  tangents_1 = None
#     avg_pool2d_backward = torch.ops.aten.avg_pool2d_backward.default(unsqueeze_1, unsqueeze, [1, 2], [1, 2], [0, 0], False, True, None);  unsqueeze_1 = unsqueeze = None
#     squeeze_1 = torch.ops.aten.squeeze.dim(avg_pool2d_backward, -2);  avg_pool2d_backward = None
#     prelu_backward = torch.ops.aten.prelu_backward.default(squeeze_1, convolution_1, primals_5);  squeeze_1 = convolution_1 = primals_5 = None
#     getitem = prelu_backward[0]
#     getitem_1 = prelu_backward[1];  prelu_backward = None
#     convolution_backward = torch.ops.aten.convolution_backward.default(getitem, convolution, primals_3, [8], [1], [0], [1], False, [0], 1, [True, True, True]);  getitem = convolution = primals_3 = None
#     getitem_2 = convolution_backward[0]
#     getitem_3 = convolution_backward[1]
#     getitem_4 = convolution_backward[2];  convolution_backward = None
#     convolution_backward_1 = torch.ops.aten.convolution_backward.default(getitem_2, reflection_pad1d, primals_1, [12], [1], [0], [1], False, [0], 1, [False, True, True]);  getitem_2 = reflection_pad1d = primals_1 = None
#     getitem_6 = convolution_backward_1[1]
#     getitem_7 = convolution_backward_1[2];  convolution_backward_1 = None
#     return [getitem_6, getitem_7, getitem_3, getitem_4, getitem_1, None]

# def forward(self, primals_1, primals_2, primals_3, primals_4, primals_5, primals_6):
#     upsample_linear1d = torch.ops.aten.upsample_linear1d.vec(primals_6, None, False, [2.0]);  primals_6 = None
#     convolution = torch.ops.aten.convolution.default(upsample_linear1d, primals_1, primals_2, [1], [0], [1], False, [0], 1);  primals_2 = None
#     reflection_pad1d = torch.ops.aten.reflection_pad1d.default(convolution, [4, 4])
#     convolution_1 = torch.ops.aten.convolution.default(reflection_pad1d, primals_3, primals_4, [1], [0], [1], False, [0], 1);  primals_4 = None
#     prelu = torch.ops.aten.prelu.default(convolution_1, primals_5)
#     return [prelu, upsample_linear1d, reflection_pad1d, convolution_1, primals_1, convolution, primals_3, primals_5]
# 
# def backward(self, upsample_linear1d, reflection_pad1d, convolution_1, primals_1, convolution, primals_3, primals_5, tangents_1):
#     prelu_backward = torch.ops.aten.prelu_backward.default(tangents_1, convolution_1, primals_5);  tangents_1 = convolution_1 = primals_5 = None
#     getitem = prelu_backward[0]
#     getitem_1 = prelu_backward[1];  prelu_backward = None
#     convolution_backward = torch.ops.aten.convolution_backward.default(getitem, reflection_pad1d, primals_3, [3], [1], [0], [1], False, [0], 1, [True, True, True]);  getitem = reflection_pad1d = primals_3 = None
#     getitem_2 = convolution_backward[0]
#     getitem_3 = convolution_backward[1]
#     getitem_4 = convolution_backward[2];  convolution_backward = None
#     reflection_pad1d_backward = torch.ops.aten.reflection_pad1d_backward.default(getitem_2, convolution, [4, 4]);  getitem_2 = convolution = None
#     convolution_backward_1 = torch.ops.aten.convolution_backward.default(reflection_pad1d_backward, upsample_linear1d, primals_1, [12], [1], [0], [1], False, [0], 1, [True, True, True]);  reflection_pad1d_backward = upsample_linear1d = primals_1 = None
#     getitem_5 = convolution_backward_1[0]
#     getitem_6 = convolution_backward_1[1]
#     getitem_7 = convolution_backward_1[2];  convolution_backward_1 = None
#     upsample_linear1d_backward = torch.ops.aten.upsample_linear1d_backward.vec(getitem_5, None, [5, 8, 8000], False, [2.0]);  getitem_5 = None
#     return [getitem_6, getitem_7, getitem_3, getitem_4, getitem_1, upsample_linear1d_backward]
