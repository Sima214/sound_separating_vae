#include "FileUtils.hpp"

#include <logger/Logger.hpp>

#include <algorithm>
#include <cstring>

#include <fcntl.h>
#include <unistd.h>

namespace spec {

size_t count_lines(std::istream& i) {
    // Skip errored streams.
    if (!i.good()) {
        return 0;
    }
    // Save/reset state,
    auto old_state = i.rdstate();
    auto old_pos = i.tellg();
    i.clear();
    i.seekg(0);

    // Count newline characters.
    size_t c = std::count(std::istreambuf_iterator<char>(i),
                          std::istreambuf_iterator<char>(), '\n');
    // Correct for first line.
    c++;
    // Clear eof.
    i.clear();

    // Restore state,
    i.seekg(old_pos);
    i.setstate(old_state);

    logger.logv("Found #", c, " lines.");

    return c;
}

bool is_string_blank(const std::string& str) {
    for (char c : str) {
        if (!std::isspace(c)) {
            return false;
        }
    }
    return true;
}

bool LineIterator::get_next_line() {
    if (_stream == nullptr) {
        throw std::runtime_error("Cannot increment end iterator!");
    }
    if (_stream->eof()) {
        _count = -1ULL;
        _line.clear();
        return false;
    }
    if (!_stream->good()) {
        throw std::runtime_error("Stream fail, but not eof!");
    }
    std::getline(*_stream, _line, '\n');
    _count++;
    return true;
}

inline int __memory_mapping_prot_conv(uint32_t prot) {
    int flags = 0;
    if (MASK_TEST(prot, MemoryMapping::FLAG_PROT_EXEC)) {
        flags |= PROT_EXEC;
    }
    if (MASK_TEST(prot, MemoryMapping::FLAG_PROT_READ)) {
        flags |= PROT_READ;
    }
    if (MASK_TEST(prot, MemoryMapping::FLAG_PROT_WRITE)) {
        flags |= PROT_WRITE;
    }
    return flags;
}
inline int __memory_mapping_flags_conv(uint32_t flags) {
    int f = 0;
    if (MASK_TEST(flags, MemoryMapping::FLAG_FIXED)) {
        f |= MAP_FIXED;
    }
    if (MASK_TEST(flags, MemoryMapping::FLAG_HUGETLB)) {
        f |= MAP_HUGETLB;
    }
    if (MASK_TEST(flags, MemoryMapping::FLAG_LOCKED)) {
        f |= MAP_LOCKED;
    }
    if (MASK_TEST(flags, MemoryMapping::FLAG_32BIT)) {
        f |= MAP_32BIT;
    }
    if (MASK_TEST(flags, MemoryMapping::FLAG_GROWSDOWN)) {
        f |= MAP_GROWSDOWN;
    }
    if (MASK_TEST(flags, MemoryMapping::FLAG_NONBLOCK)) {
        f |= MAP_NONBLOCK;
    }
    if (MASK_TEST(flags, MemoryMapping::FLAG_POPULATE)) {
        f |= MAP_POPULATE;
    }
    if (MASK_TEST(flags, MemoryMapping::FLAG_NORESERVE)) {
        f |= MAP_NORESERVE;
    }
    return f;
}

MemoryMapping::MemoryMapping(void* addr, uint32_t prot, size_t length,
                             uint32_t flags) :
    _len(length) {
    errno = 0;
    void* ptr =
         mmap(addr, length, __memory_mapping_prot_conv(prot),
              __memory_mapping_flags_conv(flags) | MAP_ANONYMOUS | MAP_PRIVATE,
              -1, 0);
    if (ptr == MAP_FAILED) {
        logger.loge("Failure to allocate anonymous mapping (",
                    std::strerror(errno), ")!");
    }
    else {
        _ptr = ptr;
    }
}
MemoryMapping::MemoryMapping(int fd, uint32_t prot, size_t offset,
                             size_t length, bool shared, uint32_t flags) :
    _len(length) {
    errno = 0;
    void* ptr = mmap(nullptr, length, __memory_mapping_prot_conv(prot),
                     __memory_mapping_flags_conv(flags) |
                          (shared ? MAP_SHARED : MAP_PRIVATE),
                     fd, offset);
    if (ptr == MAP_FAILED) {
        logger.loge("Failure to allocate mapped storage (",
                    std::strerror(errno), ")!");
    }
    else {
        _ptr = ptr;
    }
}
MemoryMapping::~MemoryMapping() {
    if (has_ptr()) {
        unmap();
        // Invalidate object.
        _ptr = nullptr;
        _len = 0;
    }
}
bool MemoryMapping::protect(uint32_t prot, size_t offset, size_t length) const {
    if (offset > _len) {
        offset = _len;
    }
    if (length > (_len - offset)) {
        length = _len - offset;
    }
    errno = 0;
    int status = mprotect((char*) _ptr + offset, length,
                          __memory_mapping_prot_conv(prot));
    if (status != 0) {
        logger.loge("Failure to change access protections to mapped region (",
                    std::strerror(errno), ")!");
    }
    return status == 0;
}
bool MemoryMapping::advice(Advice advice, size_t offset, size_t length) const {
    if (offset > _len) {
        offset = _len;
    }
    if (length > (_len - offset)) {
        length = _len - offset;
    }
    errno = 0;
    int status =
         madvise((char*) _ptr + offset, length, static_cast<int>(advice));
    if (status != 0) {
        logger.loge("Failure to apply advice to mapped region (",
                    std::strerror(errno), ")!");
    }
    return status == 0;
}
bool MemoryMapping::lock(size_t offset, size_t length) const {
    if (offset > _len) {
        offset = _len;
    }
    if (length > (_len - offset)) {
        length = _len - offset;
    }
    errno = 0;
    int status = mlock((char*) _ptr + offset, length);
    if (status != 0) {
        logger.loge("Failure to lock mapped region (", std::strerror(errno),
                    ")!");
    }
    return status == 0;
}
bool MemoryMapping::unlock(size_t offset, size_t length) const {
    if (offset > _len) {
        offset = _len;
    }
    if (length > (_len - offset)) {
        length = _len - offset;
    }
    errno = 0;
    int status = munlock((char*) _ptr + offset, length);
    if (status != 0) {
        logger.loge("Failure to unlock mapped region (", std::strerror(errno),
                    ")!");
    }
    return status == 0;
}
bool MemoryMapping::sync(size_t offset, size_t length, bool async,
                         bool invalidate) const {
    int flags = async ? MS_ASYNC : MS_SYNC;
    if (invalidate) {
        flags |= MS_INVALIDATE;
    }
    if (offset > _len) {
        offset = _len;
    }
    if (length > (_len - offset)) {
        length = _len - offset;
    }
    errno = 0;
    int status = msync((char*) _ptr + offset, length, flags);
    if (status != 0) {
        logger.loge("Failure to sync mapped region (", std::strerror(errno),
                    ")!");
    }
    return status == 0;
}
bool MemoryMapping::unmap(size_t offset, size_t length) const {
    if (offset > _len) {
        offset = _len;
    }
    if (length > (_len - offset)) {
        length = _len - offset;
    }
    errno = 0;
    int status = munmap((char*) _ptr + offset, length);
    if (status != 0) {
        logger.loge("Failure to unmap mapped region (", std::strerror(errno),
                    ")!");
    }
    return status == 0;
}

FileDescriptor::FileDescriptor(const std::filesystem::path& path,
                               std::ios_base::openmode open_mode,
                               mode_t create_mode) {
    open(path, open_mode, create_mode);
}
FileDescriptor::~FileDescriptor() {
    if (is_open()) {
        if (!close()) {
            logger.loge("Couldn't close file descriptor `", _fd, "`!");
        }
    }
}

bool FileDescriptor::open(const std::filesystem::path& path,
                          std::ios_base::openmode open_mode,
                          mode_t create_mode) {
    int open_flags = 0;
    if (open_mode & std::ios_base::app) {
        throw std::invalid_argument("Cannot open a file descriptor with "
                                    "`std::ios_base::app` flag!");
    }
    if (open_mode & std::ios_base::binary) {
        /* Ignored */
    }
    if (open_mode & std::ios_base::in && open_mode & std::ios_base::out) {
        open_flags |= O_RDWR | O_CREAT;
    }
    else {
        if (open_mode & std::ios_base::in) {
            open_flags |= O_RDONLY;
        }
        if (open_mode & std::ios_base::out) {
            open_flags |= O_WRONLY | O_CREAT;
        }
    }
    if (open_mode & std::ios_base::trunc && open_mode & std::ios_base::out) {
        open_flags |= O_TRUNC;
    }

    errno = 0;
    int fd = -1;
    if (open_mode & std::ios_base::out) {
        fd = ::open(path.generic_string().c_str(), open_flags, create_mode);
    }
    else {
        fd = ::open(path.generic_string().c_str(), open_flags);
    }
    if (fd < 0) {
        logger.loge("Couldn't open ", path, " for writing(",
                    std::strerror(errno), ")!");
        return false;
    }
    else {
        _fd = fd;
    }

    if (open_mode & std::ios_base::ate) {
        seek(0, std::ios_base::end);
    }

    return true;
}
bool FileDescriptor::seek(off_t off, std::ios_base::seekdir dir) {
    int whence = 0;
    switch (dir) {
        case std::ios_base::beg: {
            whence = SEEK_SET;
        } break;
        case std::ios_base::cur: {
            whence = SEEK_CUR;
        } break;
        case std::ios_base::end: {
            whence = SEEK_END;
        } break;
        default: {
            throw std::invalid_argument(
                 "Invalid `std::ios_base::seekdir` enumeration value!");
        }
    }

    constexpr off_t ERR_STATUS = (off_t) -1;
    errno = 0;
    off_t status = lseek(_fd, off, whence);
    if (status == ERR_STATUS) {
        logger.loge("Couldn't perform seek for file descriptor ", _fd, "(",
                    std::strerror(errno), ")!");
        return false;
    }
    return true;
}
size_t FileDescriptor::size() {
    struct stat stats;
    errno = 0;
    int status = fstat(_fd, &stats);
    if (status != 0) {
        logger.loge("Couldn't stat file descriptor ", _fd, "(",
                    std::strerror(errno), ")!");
        return std::numeric_limits<size_t>::max();
    }
    return stats.st_size;
}
FileDescriptor::Type FileDescriptor::type() {
    struct stat stats;
    errno = 0;
    int status = fstat(_fd, &stats);
    if (status != 0) {
        logger.loge("Couldn't stat file descriptor ", _fd, "(",
                    std::strerror(errno), ")!");
        return FileDescriptor::Type::Invalid;
    }
    FileDescriptor::Type type = FileDescriptor::Type::Invalid;
    if (S_ISREG(stats.st_mode)) {
        type = FileDescriptor::Type::Regular;
    }
    else if (S_ISDIR(stats.st_mode)) {
        type = FileDescriptor::Type::Directory;
    }
    else if (S_ISCHR(stats.st_mode)) {
        type = FileDescriptor::Type::CharacterDevice;
    }
    else if (S_ISBLK(stats.st_mode)) {
        type = FileDescriptor::Type::BlockDevice;
    }
    else if (S_ISFIFO(stats.st_mode)) {
        type = FileDescriptor::Type::NamedPipe;
    }
    else if (S_ISLNK(stats.st_mode)) {
        type = FileDescriptor::Type::SymbolicLink;
    }
    else if (S_ISSOCK(stats.st_mode)) {
        type = FileDescriptor::Type::Socket;
    }
    else {
        logger.loge("Couldn't parse type from "
                    "stats.st_mode for file descriptor ",
                    _fd, "(", std::strerror(errno), ")!");
    }
    return type;
}
MemoryMapping FileDescriptor::map(size_t off, size_t len, uint32_t prot,
                                  bool shared, uint32_t flags) {
    if (type() == Type::Regular) {
        size_t file_size = size();
        // Clamp offset.
        if (off > file_size) {
            off = file_size;
        }
        // Clamp length.
        if (len > (file_size - off)) {
            len = file_size - off;
        }
    }
    return MemoryMapping(_fd, prot, off, len, shared, flags);
}
bool FileDescriptor::close() {
    if (_fd >= 0) {
        errno = 0;
        int status = ::close(_fd);
        if (status < 0) {
            logger.loge("Couldn't close file descriptor ", _fd, "(",
                        std::strerror(errno), ")!");
        }
        _fd = -1;
        return status >= 0;
    }
    return true;
}

}  // namespace spec
