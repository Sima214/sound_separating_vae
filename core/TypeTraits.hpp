#ifndef SPEC_TYPETRAITS_HPP
#define SPEC_TYPETRAITS_HPP
/**
 * @file
 * @brief
 */

#include <cstdint>
#include <type_traits>

namespace spec {

namespace detail {

template<typename T> struct widen_integral;

template<> struct widen_integral<std::int8_t> {
    using type = std::int16_t;
};

template<> struct widen_integral<std::uint8_t> {
    using type = std::uint16_t;
};

template<> struct widen_integral<std::int16_t> {
    using type = std::int32_t;
};

template<> struct widen_integral<std::uint16_t> {
    using type = std::uint32_t;
};

template<> struct widen_integral<std::int32_t> {
    using type = std::int64_t;
};

template<> struct widen_integral<std::uint32_t> {
    using type = std::uint64_t;
};

template<> struct widen_integral<std::int64_t> {
    using type = __int128;
};

template<> struct widen_integral<std::uint64_t> {
    using type = __uint128_t;
};

}  // namespace detail

template<typename T> struct widen_integral {
    using type = typename detail::widen_integral<T>::type;
};

template<typename T> using widen_integral_t = typename widen_integral<T>::type;

}  // namespace spec

#endif /*SPEC_TYPETRAITS_HPP*/