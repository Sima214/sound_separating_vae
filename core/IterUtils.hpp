#ifndef SPEC_ITERUTILS_HPP
#define SPEC_ITERUTILS_HPP
/**
 * @file
 * @brief Pythonic & STL style iterators.
 */

#include <cstdint>
#include <iterator>
#include <stdexcept>
#include <utility>

namespace spec {

template<typename C, typename V> class IndexIterator {
   public:
    using iterator_category = std::random_access_iterator_tag;
    using value_type = V;
    using difference_type = size_t;
    using pointer = V*;
    using reference = V&;

   protected:
    C& _handle;
    size_t _pos;

   public:
    IndexIterator(C& container, size_t position = 0) :
        _handle(container), _pos(position) {}

    reference operator*() const {
        return (*_handle)[_pos];
    }
    reference operator[](int) const {
        return (*_handle)[_pos];
    }

    IndexIterator& operator++() {
        ++_pos;
        return *this;
    }
    IndexIterator operator++(int) {
        IndexIterator copy = *this;
        ++_pos;
        return copy;
    }
    IndexIterator& operator--() {
        --_pos;
        return *this;
    }
    IndexIterator operator--(int) {
        IndexIterator copy = *this;
        --_pos;
        return copy;
    }

    IndexIterator operator+(size_t off) const {
        return IndexIterator(_handle, _pos + off);
    }
    IndexIterator operator+(intmax_t off) const {
        return IndexIterator(_handle, _pos + off);
    }
    IndexIterator operator-(size_t off) const {
        return IndexIterator(_handle, _pos - off);
    }
    IndexIterator operator-(intmax_t off) const {
        return IndexIterator(_handle, _pos - off);
    }

    IndexIterator& operator+=(size_t off) {
        _pos += off;
        return *this;
    }
    IndexIterator& operator+=(intmax_t off) {
        _pos += off;
        return *this;
    }
    IndexIterator& operator-=(size_t off) {
        _pos -= off;
        return *this;
    }
    IndexIterator& operator-=(intmax_t off) {
        _pos -= off;
        return *this;
    }

    bool operator==(const IndexIterator& o) const {
        return _pos == o._pos;
    }
    bool operator!=(const IndexIterator& o) const {
        return _pos != o._pos;
    }
    bool operator<=(const IndexIterator& o) const {
        return _pos <= o._pos;
    }
    bool operator>=(const IndexIterator& o) const {
        return _pos >= o._pos;
    }
    bool operator<(const IndexIterator& o) const {
        return _pos < o._pos;
    }
    bool operator>(const IndexIterator& o) const {
        return _pos > o._pos;
    }

    template<typename _C, typename _V>
    friend size_t operator-(const IndexIterator<_C, _V>& i,
                            const IndexIterator<_C, _V>& j);
};

template<typename C, typename V>
inline size_t operator-(const IndexIterator<C, V>& i,
                        const IndexIterator<C, V>& j) {
    return i._pos - j._pos;
}

template<typename C, typename V> class IndexerProxy {
   public:
    using iterator = IndexIterator<C, V>;

   protected:
    C& _handle;

   public:
    IndexerProxy(C container) : _handle(container) {}

    iterator begin() {
        return IndexIterator(_handle, 0);
    }
    iterator end() {
        return IndexIterator(_handle, _handle.size());
    }
    size_t size() {
        return _handle.size();
    }
};

template<typename A, typename B> class ZipIterator {
   public:
    using iterator_category = std::random_access_iterator_tag;
    using value_type = std::pair<typename std::iterator_traits<A>::value_type,
                                 typename std::iterator_traits<B>::value_type>;
    using difference_type = size_t;
    using pointer = std::pair<typename std::iterator_traits<A>::pointer,
                              typename std::iterator_traits<B>::pointer>;
    using reference = std::pair<typename std::iterator_traits<A>::reference,
                                typename std::iterator_traits<B>::reference>;

   protected:
    A _iter_a;
    B _iter_b;

   public:
    ZipIterator(A a, B b) : _iter_a(a), _iter_b(b) {}

    ZipIterator& operator++() {
        ++_iter_a;
        ++_iter_b;
        return *this;
    }
    ZipIterator& operator--() {
        --_iter_a;
        --_iter_b;
        return *this;
    }

    ZipIterator operator++(int) {
        ZipIterator copy = *this;
        ++*this;
        return copy;
    }
    ZipIterator operator--(int) {
        ZipIterator copy = *this;
        --*this;
        return copy;
    }

    ZipIterator operator+(size_t v) const {
        ZipIterator copy = *this;
        copy += v;
        return copy;
    }
    ZipIterator operator-(size_t v) const {
        ZipIterator copy = *this;
        copy -= v;
        return copy;
    }

    ZipIterator& operator+=(size_t v) {
        _iter_a += v;
        _iter_b += v;
        return *this;
    }
    ZipIterator& operator-=(size_t v) {
        _iter_a -= v;
        _iter_b -= v;
        return *this;
    }

    bool operator==(const ZipIterator& other) const {
        bool a = _iter_a == other._iter_a;
        bool b = _iter_b == other._iter_b;
        return a && b;
    }
    bool operator!=(const ZipIterator& other) const {
        return !(*this == other);
    }
    reference operator*() const {
        typename std::iterator_traits<A>::reference a = *_iter_a;
        typename std::iterator_traits<B>::reference b = *_iter_b;
        return std::pair(a, b);
    }

    template<typename _A, typename _B>
    friend difference_type operator-(const ZipIterator<_A, _B>&,
                                     const ZipIterator<_A, _B>&);
};

template<typename A, typename B>
inline size_t operator-(const ZipIterator<A, B>& i,
                        const ZipIterator<A, B>& j) {
    size_t a = i._iter_a - j._iter_a;
    size_t b = i._iter_b - j._iter_b;
    if (a == b) {
        return a;
    }
    else {
        throw std::runtime_error("Zip iterators contains mismatched sizes!");
    }
}

template<typename A, typename B> class ZipContainerProxy {
   protected:
    A& _a;
    B& _b;

   public:
    using iterator = ZipIterator<decltype(_a.begin()), decltype(_b.begin())>;

   public:
    ZipContainerProxy(A& a, B& b) : _a(a), _b(b) {
        _validate();
    }

    iterator begin() {
        return ZipIterator(_a.begin(), _b.begin());
    }
    iterator end() {
        return ZipIterator(_a.end(), _b.end());
    }
    size_t size() {
        _validate();
        return _a.size();
    }

   protected:
    void _validate() {
        if (_a.size() != _b.size()) {
            throw std::runtime_error(
                 "A and B containers have non-equal sizes!");
        }
    }
};

}  // namespace spec

#endif /*SPEC_ITERUTILS_HPP*/