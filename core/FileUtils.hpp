#ifndef SPEC_FILE_UTILS_HPP
#define SPEC_FILE_UTILS_HPP
/**
 * @file
 * @brief File object and I/O utilities.
 */

#include <Utils.hpp>

#include <cstdint>
#include <filesystem>
#include <iterator>
#include <limits>
#include <string>
#include <utility>

#include <macros.h>
#include <sys/mman.h>

namespace spec {

size_t count_lines(std::istream& i);

/**
 * Tests if string contains only whitespace characters.
 */
bool is_string_blank(const std::string& str);

class LineIterator {
   public:
    using iterator_category = std::input_iterator_tag;
    using value_type = std::pair<std::string, size_t>;
    using difference_type = size_t;
    using pointer = std::pair<std::string const*, size_t>;
    using reference = std::pair<const std::string&, size_t>;

   protected:
    std::istream* _stream;
    size_t _count;
    std::string _line;

   public:
    LineIterator(size_t size) : _stream(nullptr), _count(size) {}  // End mark.
    LineIterator(std::istream& stream) : _stream(&stream), _count(-1ULL) {
        get_next_line();
    }

    LineIterator& operator++() {
        get_next_line();
        return *this;
    }
    bool operator==(const LineIterator& other) const {
        if (other._stream != nullptr) {
            throw std::runtime_error("Can only compare with end iterator!");
        }
        return !has_value();
    }
    bool operator!=(const LineIterator& other) const {
        if (other._stream != nullptr) {
            throw std::runtime_error("Can only compare with end iterator!");
        }
        return has_value();
    }
    std::pair<const std::string&, size_t> operator*() const {
        if (!has_value()) {
            throw std::runtime_error(
                 "Tried to get value of empty LineIterator!");
        }
        return {_line, _count};
    }

   protected:
    bool has_value() const {
        return _stream != nullptr && _count != -1ULL;
    }
    bool get_next_line();

    friend size_t operator-(const LineIterator&, const LineIterator&);
};

inline size_t operator-(const LineIterator& a, const LineIterator& b) {
    if (a._stream != nullptr) {
        throw std::runtime_error(
             "Difference's 1st operand must be end iterator!");
    }
    if (b._stream == nullptr) {
        // Case of two end iterators.
        return 0;
    }
    if (!b.has_value()) {
        throw std::runtime_error(
             "Difference's 2st operand doesn't have a value!");
    }
    return a._count - b._count;
}

class Lines {
   public:
    using iterator = LineIterator;

   protected:
    std::istream& _stream;
    size_t _count;

   public:
    Lines(std::istream& stream) :
        _stream(stream), _count(count_lines(stream)) {}

    LineIterator begin() {
        return LineIterator(_stream);
    }
    LineIterator end() {
        return LineIterator(_count);
    }
    size_t size() {
        return _count;
    }
};

class MemoryMapping : public ::spec::INonCopyable {
   public:
    static constexpr uint32_t FLAG_FIXED = MASK_CREATE(0);
    static constexpr uint32_t FLAG_HUGETLB = MASK_CREATE(1);
    static constexpr uint32_t FLAG_LOCKED = MASK_CREATE(2);
    static constexpr uint32_t FLAG_32BIT = MASK_CREATE(3);
    static constexpr uint32_t FLAG_GROWSDOWN = MASK_CREATE(4);
    static constexpr uint32_t FLAG_NONBLOCK = MASK_CREATE(5);
    static constexpr uint32_t FLAG_POPULATE = MASK_CREATE(6);
    static constexpr uint32_t FLAG_NORESERVE = MASK_CREATE(7);

    static constexpr uint32_t FLAG_PROT_EXEC = MASK_CREATE(0);
    static constexpr uint32_t FLAG_PROT_READ = MASK_CREATE(1);
    static constexpr uint32_t FLAG_PROT_WRITE = MASK_CREATE(2);

    enum class Advice : int {
        Normal = MADV_NORMAL,
        Random = MADV_RANDOM,
        Sequential = MADV_SEQUENTIAL,
        Willneed = MADV_WILLNEED,
        Dontneed = MADV_DONTNEED,
        Free = MADV_FREE,
        Remove = MADV_REMOVE,
        Dontfork = MADV_DONTFORK,
        Dofork = MADV_DOFORK,
        Mergeable = MADV_MERGEABLE,
        Unmergeable = MADV_UNMERGEABLE,
        Hugepage = MADV_HUGEPAGE,
        Nohugepage = MADV_NOHUGEPAGE,
        Dontdump = MADV_DONTDUMP,
        Dodump = MADV_DODUMP,
        Wipeonfork = MADV_WIPEONFORK,
        Keeponfork = MADV_KEEPONFORK,
        Cold = MADV_COLD,
        Pageout = MADV_PAGEOUT,
        PopulateEead = MADV_POPULATE_READ,
        PopulateWrite = MADV_POPULATE_WRITE,
        DontneedLocked = MADV_DONTNEED_LOCKED,
        Collapse = MADV_COLLAPSE
    };

   protected:
    void* _ptr = nullptr;
    size_t _len = 0;

   public:
    MemoryMapping() = default;
    // Create an anonymous memory mapping.
    MemoryMapping(uint32_t prot, size_t length, uint32_t flags = 0) :
        MemoryMapping(nullptr, prot, length, flags) {}
    // Create an anonymous memory mapping with placement hinting.
    MemoryMapping(void* addr, uint32_t prot, size_t length, uint32_t flags = 0);
    // Create a file-backed memory mapping.
    explicit MemoryMapping(int fd, uint32_t prot, size_t offset, size_t length,
                           bool shared, uint32_t flags = 0);
    MemoryMapping(MemoryMapping&& obj) :
        _ptr(std::exchange(obj._ptr, nullptr)), _len(obj._len) {}
    MemoryMapping& operator=(MemoryMapping&& obj) {
        _ptr = std::exchange(obj._ptr, nullptr);
        _len = obj._len;
        return *this;
    }
    ~MemoryMapping();

    bool has_ptr() const {
        return _ptr != nullptr;
    }
    operator bool() const {
        return has_ptr();
    }

    template<typename T = void> T* get_ptr() const {
        return static_cast<T*>(_ptr);
    }
    template<typename T = void> operator T*() const {
        return get_ptr<T>();
    }

    size_t get_length() const {
        return _len;
    }
    size_t size() const {
        return _len;
    }

    template<typename T = void> T* begin() const {
        return static_cast<T*>(_ptr);
    }
    template<typename T = void> T* end() const {
        return static_cast<T*>((void*) (((uintptr_t) _ptr) + _len));
    }

    bool protect(uint32_t prot, size_t offset = 0,
                 size_t length = std::numeric_limits<size_t>::max()) const;
    bool advice(Advice advice, size_t offset = 0,
                size_t length = std::numeric_limits<size_t>::max()) const;
    bool lock(size_t offset = 0,
              size_t length = std::numeric_limits<size_t>::max()) const;
    bool unlock(size_t offset = 0,
                size_t length = std::numeric_limits<size_t>::max()) const;
    bool sync(size_t offset = 0,
              size_t length = std::numeric_limits<size_t>::max(),
              bool async = false, bool invalidate = false) const;
    bool unmap(size_t offset = 0,
               size_t length = std::numeric_limits<size_t>::max()) const;
};

class FileDescriptor : public ::spec::INonCopyable {
   public:
    static constexpr mode_t DEFAULT_CREATE_FD_MODE =
         S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;

    enum class Type {
        Regular,
        Directory,
        CharacterDevice,
        BlockDevice,
        /** Synonym for FIFO. */
        NamedPipe,
        SymbolicLink,
        Socket,
        /** Indicates an error while trying to stat the fd. */
        Invalid = 255
    };

   protected:
    int _fd = -1;

   public:
    /* Lifecycle */
    FileDescriptor() = default;
    FileDescriptor(const std::filesystem::path& path,
                   std::ios_base::openmode mode,
                   mode_t create_mode = DEFAULT_CREATE_FD_MODE);
    FileDescriptor(FileDescriptor&& obj) : _fd(std::exchange(obj._fd, -1)) {}
    FileDescriptor& operator=(FileDescriptor&& obj) {
        _fd = std::exchange(obj._fd, -1);
        return *this;
    }
    ~FileDescriptor();

    /* Getters */
    int get_fd() {
        return _fd;
    }
    operator int() {
        return get_fd();
    }
    bool is_open() {
        return _fd > 0;
    }
    operator bool() {
        return is_open();
    }

    /* Operations */
    bool open(const std::filesystem::path& path, std::ios_base::openmode mode,
              mode_t create_mode = DEFAULT_CREATE_FD_MODE);
    bool seek(off_t, std::ios_base::seekdir);
    /**
     * Returns max value of size_t on error.
     */
    size_t size();
    Type type();
    MemoryMapping map(size_t off = 0,
                      size_t len = std::numeric_limits<size_t>::max(),
                      uint32_t prot = MemoryMapping::FLAG_PROT_READ,
                      bool shared = false, uint32_t flags = 0);
    bool close();
};

}  // namespace spec

#endif /*SPEC_FILE_UTILS_HPP*/
