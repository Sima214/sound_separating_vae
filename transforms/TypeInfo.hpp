#ifndef SPEU_TYPEINFO_HPP
#define SPEU_TYPEINFO_HPP
/**
 * @file
 * @brief Helper functions for retreiving the numerical limits of at::ScalarType
 * dynamically.
 */

#include <cstddef>
#include <limits>
#include <stdexcept>

#include <torch/types.h>

namespace speu::numerical_limits {

#define CASE_MAX(ctype, name)                                                  \
    case at::ScalarType::name: {                                               \
        return static_cast<T>(std::numeric_limits<ctype>::max());              \
    } break;

#define CASE_MIN(ctype, name)                                                  \
    case at::ScalarType::name: {                                               \
        return static_cast<T>(std::numeric_limits<ctype>::min());              \
    } break;

template<typename T> constexpr T max(at::ScalarType t) {
    switch (t) {
        AT_FORALL_SCALAR_TYPES_AND3(Half, Bool, BFloat16, CASE_MAX);
        default: {
            throw std::invalid_argument("Cannot retrieve max value for type `" +
                                        std::string(torch::toString(t)) + "`!");
        } break;
    }
}

template<typename T> constexpr T min(at::ScalarType t) {
    switch (t) {
        AT_FORALL_SCALAR_TYPES_AND3(Half, Bool, BFloat16, CASE_MIN);
        default: {
            throw std::invalid_argument("Cannot retrieve min value for type `" +
                                        std::string(torch::toString(t)) + "`!");
        } break;
    }
}

#undef CASE_MIN
#undef CASE_MAX

}  // namespace speu::numerical_limits

#endif /*SPEU_TYPEINFO_HPP*/