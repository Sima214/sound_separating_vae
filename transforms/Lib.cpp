#include "Lib.hpp"

namespace speu::transforms {

bool configure_logger(bool console, spec::Logger::Level console_level, const char* filename, spec::Logger::Level file_level) {
    return logger.configure(console, console_level, filename, file_level);
}

}
