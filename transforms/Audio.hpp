#ifndef SPEU_AUDIO_HPP
#define SPEU_AUDIO_HPP
/**
 * @file
 * @brief Ports from torchaudio.functional.*
 * https://pytorch.org/audio/stable/functional.html
 */

#include <cstddef>
#include <cstdint>
#include <optional>

#include <ATen/Tensor.h>
#include <macros.h>

namespace speu::audio::transforms {

enum class ResamplingMethod { SincInterpolation, KaiserWindow };

EXPORT_API at::Tensor resample(
     at::Tensor waveform, int64_t orig_freq, int64_t new_freq,
     int64_t lowpass_filter_width = 6, double rolloff = 0.99,
     ResamplingMethod resampling_method = ResamplingMethod::SincInterpolation,
     std::optional<double> beta = {});
EXPORT_API at::Tensor gain(at::Tensor waveform, double gain_db = 1);

}  // namespace speu::audio::transforms

#endif /*SPEU_AUDIO_HPP*/