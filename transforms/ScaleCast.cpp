#include "General.hpp"

#include <TypeInfo.hpp>
#include <TypeTraits.hpp>
#include <logger/Logger.hpp>

#include <cmath>
#include <cstdlib>
#include <limits>
#include <stdexcept>
#include <type_traits>

#include <c10/core/Backend.h>
#include <macros.h>
#include <torch/data.h>
#include <torch/enum.h>
#include <torch/types.h>
#include <torch/utils.h>

namespace speu::transforms {

#define SCALE_CAST_DISPATCH_L1(ctype, name)                                    \
    case at::ScalarType::name: {                                               \
        return __scale_cast_map_values<ctype>(t, dst_dtype);                   \
    } break;

#define SCALE_CAST_DISPATCH_L2(ctype, name)                                    \
    case at::ScalarType::name: {                                               \
        if constexpr (!std::is_same_v<ctype, SRC>) {                           \
            __scale_cast_map_values<ctype, SRC>(dst, src);                     \
        }                                                                      \
        else {                                                                 \
            throw std::runtime_error("Unreachable@" FILE_LINE_STR);            \
        }                                                                      \
    } break;

using COM = std::common_type_t<int32_t, uint32_t>;

template<typename DST, typename SRC>
inline std::enable_if_t<!std::is_same_v<DST, SRC> && std::is_integral_v<DST> &&
                        std::is_integral_v<SRC>>
__scale_cast_map_values(DST* dst, const SRC* src, int64_t numel) {
    using dst_t = DST;
    using src_t = SRC;
    using common_t = std::common_type_t<dst_t, src_t>;
    using widest_t = spec::widen_integral_t<common_t>;
    using wider_dst_t = spec::widen_integral_t<dst_t>;
    // using wider_src_t = spec::widen_integral_t<src_t>;
    using factor_t = widest_t;

    constexpr auto dst_min = std::numeric_limits<dst_t>::min();
    constexpr auto src_min = std::numeric_limits<src_t>::min();
    constexpr auto dst_range =
         std::numeric_limits<std::make_unsigned_t<dst_t>>::max();
    constexpr auto src_range =
         std::numeric_limits<std::make_unsigned_t<src_t>>::max();

    constexpr bool is_dst_wider = dst_range > src_range;

    constexpr factor_t factor =
         is_dst_wider ? dst_range / src_range : src_range / dst_range;

    // TODO: Consider targeting 0 as the center value when casting signed to
    // signed.
    for (int64_t i = 0; i < numel; i++) {
        src_t l_v = src[i];
        wider_dst_t s_v;
        factor_t scaled_l_v = l_v - src_min;
        if constexpr (is_dst_wider) {
            s_v = scaled_l_v * factor;
        }
        else {
            s_v = scaled_l_v / factor;
        }
        s_v = s_v + dst_min;
        dst[i] = s_v;
    }
}

template<typename DST, typename SRC>
inline void __scale_cast_map_values(at::Tensor dst, at::Tensor src) {
    if (src.numel() < 0) {
        throw std::runtime_error("numel invalid!");
    }
    if (src.numel() != dst.numel()) {
        throw std::runtime_error("src & dst numel mismatch!");
    }
    if (!src.is_contiguous()) {
        throw std::runtime_error("src not contiguous!");
    }
    if (!dst.is_contiguous()) {
        throw std::runtime_error("dst not contiguous!");
    }
    __scale_cast_map_values(dst.data_ptr<DST>(), src.data_ptr<SRC>(),
                            src.numel());
}

template<typename SRC>
inline at::Tensor __scale_cast_map_values(at::Tensor src,
                                          torch::Dtype dst_dtype) {
    src = src.contiguous();
    auto dst =
         torch::empty_like(src, torch::TensorOptions(dst_dtype).memory_format(
                                     torch::MemoryFormat::Contiguous));
    switch (dst_dtype) {
        AT_FORALL_INT_TYPES(SCALE_CAST_DISPATCH_L2);
        default: throw std::runtime_error("Unreachable@" FILE_LINE_STR);
    }
    return dst;
}

at::Tensor scale_cast(at::Tensor t, torch::Dtype dst_dtype) {
    if (!at::isFloatingType(dst_dtype) &&
        !at::isIntegralType(dst_dtype, false)) {
        throw std::runtime_error("scale_cast(): Input tensor of dtype `" +
                                 std::string(torch::toString(dst_dtype)) +
                                 "` is not supported!");
    }
    // Gradient calculation is not supported.
    torch::NoGradGuard _grad_guard;

    torch::Dtype src_dtype = t.dtype().toScalarType();
    if (src_dtype == dst_dtype) {
        logger.logv("scale_cast: skip(", torch::toString(src_dtype), "->",
                    torch::toString(dst_dtype), ")");
        return t;
    }

    if (at::isFloatingType(src_dtype)) {
        if (at::isFloatingType(dst_dtype)) {
            // float to float
            logger.logv("scale_cast: float to float");
            return t.to(dst_dtype);
        }
        else {
            // float to integer
            const double eps = 1e-3;
            if (at::isSignedType(dst_dtype)) {
                logger.logv("scale_cast: float to signed integer");
                double min_val = speu::numerical_limits::min<double>(dst_dtype);
                double max_val = speu::numerical_limits::max<double>(dst_dtype);

                // Clone
                t = t.clone();

                // Positives
                auto m_p = t > 0;
                t.index_put_({m_p}, t.index({m_p}).mul_(max_val + (1. - eps)));
                // Negatives
                auto m_n = t < 0;
                t.index_put_({m_n}, t.index({m_n}).mul_(std::abs(min_val) +
                                                        (1. - eps)));

                return t.to(dst_dtype);
            }
            else {
                logger.logv("scale_cast: float to unsigned integer");
                double max_val = speu::numerical_limits::max<double>(dst_dtype);
                t = t.mul(max_val + (1. - eps));
                return t.to(dst_dtype);
            }
        }
    }
    else if (at::isIntegralType(src_dtype, false)) {
        if (at::isFloatingType(dst_dtype)) {
            // integer to float
            if (at::isSignedType(src_dtype)) {
                logger.logv("scale_cast: signed integer to float");
                double min_val = speu::numerical_limits::min<double>(src_dtype);
                double max_val = speu::numerical_limits::max<double>(src_dtype);

                // Cast
                t = t.to(dst_dtype);

                // Positives
                auto m_p = t > 0;
                t.index_put_({m_p}, t.index({m_p}).div_(max_val));
                // Negatives
                auto m_n = t < 0;
                t.index_put_({m_n}, t.index({m_n}).div_(std::abs(min_val)));

                return t;
            }
            else {
                logger.logv("scale_cast: unsigned integer to float");
                double max_val = speu::numerical_limits::max<double>(src_dtype);
                t = t.to(dst_dtype);
                return t / max_val;
            }
        }
        else {
            // integer to integer
            logger.logv("scale_cast: int(", torch::toString(src_dtype),
                        ") to int(", torch::toString(dst_dtype), ")");
            switch (src_dtype) {
                AT_FORALL_INT_TYPES(SCALE_CAST_DISPATCH_L1);
                default: throw std::runtime_error("Unreachable@" FILE_LINE_STR);
            }
        }
    }
    else {
        throw std::runtime_error("scale_cast(): Input tensor of dtype `" +
                                 std::string(torch::toString(src_dtype)) +
                                 "` is not supported!");
    }
}

#undef SCALE_CAST_DISPATCH_L1
#undef SCALE_CAST_DISPATCH_L2

}  // namespace speu::transforms
