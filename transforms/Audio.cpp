#include "Audio.hpp"

#include <logger/Logger.hpp>

#include <cmath>
#include <numbers>
#include <numeric>
#include <ostream>
#include <stdexcept>
#include <utility>

#include <torch/autograd.h>
#include <torch/data.h>
#include <torch/enum.h>
#include <torch/linalg.h>
#include <torch/nn.h>
#include <torch/special.h>
#include <torch/types.h>
#include <torch/utils.h>

namespace speu::audio::transforms {

std::ostream& operator<<(std::ostream& o, ResamplingMethod v) {
    switch (v) {
        case ResamplingMethod::SincInterpolation: {
            o << "SincInterpolation";
        } break;
        case ResamplingMethod::KaiserWindow: {
            o << "KaiserWindow";
        } break;
    }
    return o;
}

static std::pair<at::Tensor, int64_t> __resample_get_sinc_resample_kernel(
     int64_t orig_freq, int64_t new_freq, int64_t gcd,
     int64_t lowpass_filter_width, double rolloff,
     ResamplingMethod resampling_method, std::optional<double> beta,
     torch::Device device = torch::DeviceType::CPU,
     std::optional<torch::Dtype> dtype = {}) {
    using namespace torch::indexing;
    const at::Scalar pi(std::numbers::pi_v<double>);

    orig_freq /= gcd;
    new_freq /= gcd;

    // This will perform antialiasing filtering by removing the highest
    // frequencies.
    double base_freq = std::min(orig_freq, new_freq) * rolloff;

    /*
     * We see here that y[j] is the convolution of x[i] with a specific filter,
     * for which we take an FIR approximation, stopping when we see at least
     * `lowpass_filter_width` zeros crossing.
     */
    int64_t width =
         std::ceil(lowpass_filter_width * orig_freq / base_freq) + 0.5;

    logger.logv("Calculating ", resampling_method,
                " resampling kernel for frequency ratio ", orig_freq, "/",
                new_freq, " at ", width, " width.");

    /*
     * If orig_freq is still big after GCD reduction, most filters will be very
     * unbalanced, i.e., they will have a lot of almost zero values to the left
     * or to the right...
     */
    torch::TensorOptions idx_options(dtype.value_or(torch::Dtype::Double));
    idx_options = idx_options.device(device);
    auto idx = torch::arange(-width, width + orig_freq, idx_options)
                    .index({None, None}) /
               orig_freq;

    torch::TensorOptions options(dtype.value_or(torch::Dtype::Float));
    options = options.device(device);
    auto t =
         torch::arange(0, -new_freq, -1, options).index({Slice(), None, None});
    t = (t / new_freq + idx) * base_freq;
    t = t.clamp_(-lowpass_filter_width, lowpass_filter_width);

    /*
     * We do not use built in torch windows here as we need to evaluate the
     * window at specific positions, not over a regular grid.
     */
    at::Tensor window;
    switch (resampling_method) {
        case ResamplingMethod::SincInterpolation: {
            window =
                 torch::pow(torch::cos(t * pi / lowpass_filter_width / 2), 2);
        } break;
        case ResamplingMethod::KaiserWindow: {
            auto beta_tensor = torch::tensor(beta.value_or(14.769656459379492));
            window = torch::i0(beta_tensor *
                               torch::sqrt(torch::pow(
                                    1 - (t / lowpass_filter_width), 2))) /
                     torch::i0(beta_tensor);
        } break;
        default:
            throw std::invalid_argument(
                 "Invalid enum value for ResamplingMethod!");
    }

    t *= pi;

    auto scale = base_freq / orig_freq;
    auto kernels = torch::where(t == 0, torch::tensor(1.0).to(t), t.sin() / t);
    kernels *= window * scale;

    if (!dtype) {
        kernels = kernels.to(torch::Dtype::Float);
    }

    return {kernels, width};
}

static at::Tensor __resample_apply_sinc_resample_kernel(
     at::Tensor waveform, int64_t orig_freq, int64_t new_freq, int64_t gcd,
     at::Tensor kernel, int64_t width) {
    if (!waveform.is_floating_point()) {
        throw std::invalid_argument("Expected floating point type for waveform "
                                    "tensor, but received type " +
                                    std::string(waveform.dtype().name()) + "!");
    }

    using namespace torch::indexing;

    orig_freq /= gcd;
    new_freq /= gcd;

    // logger.logd("Applying resampling kernel for frequency ratio ", orig_freq,
    //             "/", new_freq, " at waveform of shape ", waveform.sizes(), ".");

    // pack batch
    auto dims = waveform.dim();
    auto shape = waveform.sizes();
    auto length = waveform.size(dims - 1);
    waveform = waveform.view({-1, length});
    auto num_wavs = waveform.size(0);

    waveform = torch::nn::functional::pad(
         waveform,
         torch::nn::functional::PadFuncOptions({width, width + orig_freq})
              .mode(torch::kConstant));
    auto resampled = torch::nn::functional::conv1d(
         waveform.index({Slice(), None}), kernel,
         torch::nn::functional::Conv1dFuncOptions().stride(orig_freq));
    resampled = resampled.transpose(1, 2).reshape({num_wavs, -1});
    auto target_length =
         static_cast<int64_t>(std::ceil(new_freq * length / orig_freq));
    resampled = resampled.index({"...", Slice(None, target_length)});

    // unpack batch
    std::vector<int64_t> output_shape(dims, -1);
    for (int64_t dim = 0; dim < dims - 1; dim++) {
        output_shape[dim] = shape[dim];
    }
    output_shape[output_shape.size() - 1] = resampled.size(resampled.dim() - 1);
    resampled = resampled.view(output_shape);
    return resampled;
}

EXPORT_API at::Tensor resample(at::Tensor waveform, int64_t orig_freq,
                               int64_t new_freq, int64_t lowpass_filter_width,
                               double rolloff,
                               ResamplingMethod resampling_method,
                               std::optional<double> beta) {
    if (orig_freq <= 0 || new_freq <= 0) {
        throw std::invalid_argument(
             "Original frequency and desired frequecy should be positive!");
    }
    if (lowpass_filter_width <= 0) {
        throw std::invalid_argument(
             "Low pass filter width should be positive.");
    }
    auto gcd = std::gcd(orig_freq, new_freq);
    auto [kernel, width] = __resample_get_sinc_resample_kernel(
         orig_freq, new_freq, gcd, lowpass_filter_width, rolloff,
         resampling_method, beta, waveform.device(),
         waveform.dtype().toScalarType());
    auto resampled = __resample_apply_sinc_resample_kernel(
         waveform, orig_freq, new_freq, gcd, kernel, width);
    return resampled;
}

at::Tensor gain(at::Tensor waveform, double gain_db) {
    if (gain_db == 0) {
        return waveform;
    }

    float ratio = std::pow(10, gain_db / 20);
    return waveform * ratio;
}

}  // namespace speu::audio::transforms
