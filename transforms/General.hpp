#ifndef SPEU_GENERAL_HPP
#define SPEU_GENERAL_HPP
/**
 * @file
 * @brief Transforms to be applied generally on tensor - regardless of the
 * nature of the data.
 */

#include <cstddef>
#include <cstdint>

#include <ATen/Tensor.h>
#include <macros.h>
#include <torch/types.h>

namespace speu::transforms {

/**
 * For now this is based on the implementation of torchvision's convert_image_dtype transform.
 * TODO: Test converted value quantization and distribution and improve code.
 */
EXPORT_API at::Tensor scale_cast(at::Tensor t, torch::Dtype dtype);

}  // namespace speu::transforms

#endif /*SPEU_GENERAL_HPP*/