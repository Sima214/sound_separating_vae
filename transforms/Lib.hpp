#ifndef SPEU_LIB_HPP
#define SPEU_LIB_HPP
/**
 * @file
 * @brief Special entries for managing shared object's internal state.
 */

#include <logger/Logger.hpp>

#include <macros.h>

namespace speu::transforms {

EXPORT_API bool configure_logger(bool console,
                                 spec::Logger::Level console_level,
                                 const char* filename,
                                 spec::Logger::Level file_level);

}

#endif /*SPEU_LIB_HPP*/