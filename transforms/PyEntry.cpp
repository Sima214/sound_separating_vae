#include <Audio.hpp>
#include <General.hpp>
#include <Lib.hpp>
#include <logger/Logger.hpp>

#include <algorithm>
#include <cctype>
#include <filesystem>
#include <functional>
#include <optional>
#include <type_traits>

#include <pybind11/pybind11.h>
#include <torch/python.h>
#include <torch/version.h>

static bool __iequals(const std::string& a, const std::string& b) {
    return std::equal(a.begin(), a.end(), b.begin(), b.end(),
                      [](char a, char b) { return tolower(a) == tolower(b); });
}

static spec::Logger::Level __str2level(const std::string& str) {
    if (__iequals(str, "all")) {
        return spec::Logger::Level::ALL;
    }
    else if (__iequals(str, "verbose")) {
        return spec::Logger::Level::VERBOSE;
    }
    else if (__iequals(str, "debug")) {
        return spec::Logger::Level::DEBUG;
    }
    else if (__iequals(str, "info")) {
        return spec::Logger::Level::INFO;
    }
    else if (__iequals(str, "warn")) {
        return spec::Logger::Level::WARN;
    }
    else if (__iequals(str, "error")) {
        return spec::Logger::Level::ERROR;
    }
    else if (__iequals(str, "fatal")) {
        return spec::Logger::Level::FATAL;
    }
    else if (__iequals(str, "off")) {
        return spec::Logger::Level::OFF;
    }
    else {
        throw std::invalid_argument(
             "Logger level must be one of "
             "all,verbose,debug,info,warn,error,fatal,off. Got `" +
             str + "`.");
    }
}

PYBIND11_MODULE(transforms, m) {
    m.doc() = "Implementation of some python torch/audio/vision transforms.";

    m.def(
         "configure_logger",
         [](bool console, std::string console_level,
            std::optional<std::string> file, std::string file_level) {
             const char* filename = nullptr;
             if (file) {
                 filename = file->c_str();
             }
             speu::transforms::configure_logger(
                  console, __str2level(console_level), filename,
                  __str2level(file_level));
         },
         pybind11::arg("enable_console") = false,
         pybind11::arg("console_level") = "off",
         pybind11::arg("log_file") = pybind11::none(),
         pybind11::arg("log_file_level") = "off");

    m.def("get_torch_version", []() {
         return std::string(TORCH_VERSION);
     }).doc() = "Returns the pytorch version this extension was built with.";

    m.def(
         "scale_cast",
         [](at::Tensor t, pybind11::object dtype) {
             return speu::transforms::scale_cast(
                  t, torch::python::detail::py_object_to_dtype(dtype));
         },
         pybind11::arg("t"), pybind11::arg("dtype"));

    auto audio = m.def_submodule("audio", "Ports from torchaudio.functional.*");
    audio.def(
         "resample",
         [](at::Tensor waveform, int64_t orig_freq, int64_t new_freq,
            int64_t lowpass_filter_width, double rolloff,
            std::string resampling_method, std::optional<double> beta) {
             speu::audio::transforms::ResamplingMethod resampling_method_enum;
             if (resampling_method == "sinc_interpolation") {
                 resampling_method_enum = speu::audio::transforms::
                      ResamplingMethod::SincInterpolation;
             }
             else if (resampling_method == "kaiser_window") {
                 resampling_method_enum =
                      speu::audio::transforms::ResamplingMethod::KaiserWindow;
             }
             else {
                 throw pybind11::type_error(
                      "Invalid enumeration value for resampling_method `" +
                      resampling_method + "`!");
             }
             return speu::audio::transforms::resample(
                  waveform, orig_freq, new_freq, lowpass_filter_width, rolloff,
                  resampling_method_enum, beta);
         },
         pybind11::arg("waveform"), pybind11::arg("orig_freq"),
         pybind11::arg("new_freq"), pybind11::arg("lowpass_filter_width") = 6L,
         pybind11::arg("rolloff") = 0.99,
         pybind11::arg("resampling_method") = std::string("sinc_interpolation"),
         pybind11::arg("beta") = pybind11::none());

    audio.def("gain", speu::audio::transforms::gain, pybind11::arg("waveform"),
              pybind11::arg("gain_db") = 1);
}
