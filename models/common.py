from typing import Union, List
import hashlib

from torch.nn.common_types import _size_1_t
from torch.utils.checkpoint import checkpoint
import torch.nn as nn
import torch


def get_padding_for_same_conv(kernel_size, stride=1, dilation=1):
    # return (-stride + dilation * (kernel_size - 1) + 1) // 2
    return (dilation * (kernel_size - 1) + 1) // 2


def get_config_hash_string(conf):
    # Should generate the same string, as long as
    # the key insertion order doesn't change.
    conf_data = str(conf).encode('utf8')
    hasher = hashlib.shake_128()
    hasher.update(conf_data)
    digest = hasher.hexdigest(8)
    return digest

def musdb18hq_triplet_sample_to_batch(sample, device):
    B, N = sample.a.shape
    x_a = sample.a.to(device=device,
                      non_blocking=True).view((B, 1, N))
    x_b = sample.b.to(device=device,
                      non_blocking=True).view((B, 1, N))
    x_c = sample.c.to(device=device,
                      non_blocking=True).view((B, 1, N))
    x = torch.concat((x_a, x_b, x_c), dim=0)
    return x


# class Affine(nn.Module):
#     def __init__(self, num_features: int, weight_init=1.0, bias_init=0.0, device=None, dtype=None):
#         super().__init__()
#         factory_kwargs = {'device': device, 'dtype': dtype}
#         self.weight = Parameter(torch.empty(num_features, **factory_kwargs))
#         self.bias = Parameter(torch.empty(num_features, **factory_kwargs))


class Scaler(nn.Module):
    __constants__ = ['inplace']

    def __init__(self, init=1.0, inplace=False, device=None, dtype=None):
        super().__init__()
        factory_kwargs = {'device': device, 'dtype': dtype}
        self.inplace = inplace
        self.weight = nn.Parameter(torch.empty(
            (1), **factory_kwargs).fill_(init))

    def forward(self, x):
        if self.inplace:
            x *= self.weight
        else:
            x = x * self.weight
        return x


class Conv1DActivation(nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: _size_1_t,
        stride: _size_1_t = 1,
        padding: Union[str, _size_1_t] = 0,
        dilation: _size_1_t = 1,
        groups: int = 1,
        bias: bool = True,
        padding_mode: str = 'zeros',
        activation=nn.Identity,
        device=None,
        dtype=None,
        **activation_kwargs
    ):
        super().__init__()
        self.conv = nn.Conv1d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            dilation=dilation,
            groups=groups,
            bias=bias,
            padding_mode=padding_mode,
            device=device,
            dtype=dtype
        )
        self.act = activation(**activation_kwargs, device=device, dtype=dtype)

    def forward(self, x):
        return self.act(self.conv(x))


class ConvTranspose1DActivation(nn.Module):
    def __init__(
        self,
        in_channels: int,
        out_channels: int,
        kernel_size: _size_1_t,
        stride: _size_1_t = 1,
        padding: _size_1_t = 0,
        output_padding: _size_1_t = 0,
        groups: int = 1,
        bias: bool = True,
        dilation: _size_1_t = 1,
        activation=nn.Identity,
        device=None,
        dtype=None,
        **activation_kwargs
    ):
        super().__init__()
        self.conv = nn.ConvTranspose1d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            output_padding=output_padding,
            groups=groups,
            bias=bias,
            dilation=dilation,
            device=device,
            dtype=dtype
        )
        self.act = activation(**activation_kwargs, device=device, dtype=dtype)

    def forward(self, x):
        return self.act(self.conv(x))


class CheckpointableModule(nn.Module):
    __constants__ = ['checkpointed']

    def __init__(self, checkpointed=False):
        super().__init__()
        self.checkpointed = checkpointed

    def forward(self, *inputs):
        if self.training and self.checkpointed:
            # Apply checkpointing during training.
            return checkpoint(self._forward, *inputs, use_reentrant=False, preserve_rng_state=False)
        else:
            # Standard forward pass during inference.
            return self._forward(*inputs)
