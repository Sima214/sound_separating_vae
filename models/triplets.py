from functorch.compile import memory_efficient_fusion
import torchmetrics.audio as audiometric
import functorch.compile as compile
import pytorch_lightning as pl
from models.loss import AstralLoss
import torch.nn as nn
import torch


class Encoder(nn.Module):
    def __init__(self,  input_channels, growth_channels,
                 output_channels, kernel_size=3, scale=2):
        super(Encoder, self).__init__()
        internal_channels = input_channels
        self.conv1 = nn.Conv1d(input_channels, growth_channels,
                               kernel_size, bias=True, padding='same', padding_mode='reflect')
        self.activation1 = nn.ReLU(inplace=True)
        internal_channels += growth_channels
        self.conv2 = nn.Conv1d(internal_channels, growth_channels * 2,
                               kernel_size, bias=True, padding='same', padding_mode='reflect')
        self.activation2 = nn.ReLU(inplace=True)
        internal_channels += growth_channels * 2
        self.conv3 = nn.Conv1d(internal_channels, growth_channels * 3,
                               kernel_size, bias=True, padding='same', padding_mode='reflect')
        self.activation3 = nn.ReLU(inplace=True)
        internal_channels += growth_channels * 3

        self.bottleneck = nn.Conv1d(
            internal_channels, output_channels, 1, bias=True)
        self.activation_bottleneck = nn.PReLU(num_parameters=output_channels)

        self.pool = nn.MaxPool1d(kernel_size=scale, stride=scale)

    def _forward(self, x):
        y1 = self.activation1(self.conv1(x))
        y2 = self.activation2(self.conv2(torch.concat((x, y1), dim=1)))
        y3 = self.activation3(self.conv3(torch.concat((x, y1, y2), dim=1)))

        y = self.activation_bottleneck(self.bottleneck(
            torch.concat((x, y1, y2, y3), dim=1)))

        y = self.pool(y)

        return y

    def forward(self, *inputs):
        # if False:
        if self.training:
            # Apply checkpointing during training.
            return torch.utils.checkpoint.checkpoint(self._forward, *inputs, preserve_rng_state=False)
        else:
            # Standard forward pass during inference.
            return self._forward(*inputs)


class Decoder(nn.Module):
    def __init__(self,  input_channels, growth_channels,
                 output_channels, kernel_size=3, upsample_kernel_size=None, scale=2):
        super(Decoder, self).__init__()
        if upsample_kernel_size is None:
            if kernel_size == 3:
                upsample_kernel_size = 9
            elif kernel_size == 9:
                upsample_kernel_size = 17
            else:
                raise ValueError("Cannot automatically determine kernel size for upsample encoder"
                                 "sub-block using main block's kernel size!")
        self.upsample = nn.Upsample(scale_factor=scale, mode='nearest')
        self.upsample_conv = nn.Conv1d(input_channels, input_channels, upsample_kernel_size,
                                       groups=input_channels, bias=False,
                                       padding='same', padding_mode='reflect')
        self.pointwise = nn.Conv1d(
            input_channels, input_channels, 1, bias=True)
        self.activation_pointwise = nn.ReLU(inplace=True)

        internal_channels = input_channels
        self.conv1 = nn.Conv1d(input_channels, growth_channels, kernel_size,
                               bias=True, padding='same', padding_mode='reflect')
        self.activation1 = nn.ReLU(inplace=True)
        internal_channels += growth_channels
        self.conv2 = nn.Conv1d(internal_channels, growth_channels * 2, kernel_size,
                               bias=True, padding='same', padding_mode='reflect')
        self.activation2 = nn.ReLU(inplace=True)
        internal_channels += growth_channels * 2
        self.conv3 = nn.Conv1d(internal_channels, growth_channels * 3, kernel_size,
                               bias=True, padding='same', padding_mode='reflect')
        self.activation3 = nn.ReLU(inplace=True)
        internal_channels += growth_channels * 3

        self.bottleneck = nn.Conv1d(
            internal_channels, output_channels, 1, bias=True)
        self.activation_bottleneck = nn.PReLU(num_parameters=output_channels)

    def _upsample(self, x):
        y = self.upsample_conv(self.upsample(x))
        y = self.activation_pointwise(self.pointwise(y))
        return y

    def _forward(self, x):
        y1 = self.activation1(self.conv1(x))
        y2 = self.activation2(self.conv2(torch.concat((x, y1), dim=1)))
        y3 = self.activation3(self.conv3(torch.concat((x, y1, y2), dim=1)))

        y = self.activation_bottleneck(self.bottleneck(
            torch.concat((x, y1, y2, y3), dim=1)))

        return y

    def forward(self, x):
        # if False:
        if self.training:
            # Apply checkpointing during training.
            up_x = torch.utils.checkpoint.checkpoint(
                self._upsample, x, preserve_rng_state=False)
            return torch.utils.checkpoint.checkpoint(self._forward, up_x, preserve_rng_state=False)
        else:
            # Standard forward pass during inference.
            return self._forward(self._upsample(x))


def make_pair(input_channels, growth_channels,
              output_channels, kernel_size=3, scale=2):
    encoder = Encoder(input_channels, growth_channels,
                      output_channels, kernel_size, scale)
    decoder = Decoder(output_channels, growth_channels,
                      input_channels, kernel_size, scale)
    return (encoder, decoder)


def make_heads(input_channels, output_channels, kernel_size=21):

    return (
        nn.Conv1d(input_channels, output_channels, kernel_size,
                  padding='same', padding_mode='reflect'),
        nn.Conv1d(output_channels, input_channels, kernel_size,
                  padding='same', padding_mode='reflect')
    )


def make_tails(input_channels, output_channels):
    return (
        nn.Conv1d(input_channels, output_channels, 1),
        nn.Conv1d(output_channels, input_channels, 1)
    )


class Model(nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        layers = [
            make_heads(1,  16),
            make_pair(16,  8, 32),
            make_pair(32, 12, 64),

            make_pair(64, 24, 128),
            make_pair(128, 24, 192),
            make_pair(192, 24, 256),

            make_pair(256, 32, 256),
            make_pair(256, 32, 256),
            make_pair(256, 32, 384),

            make_pair(384, 64, 512),
            make_pair(512, 64, 512),
            make_pair(512, 64, 512)
        ]
        self.encoders = nn.ModuleList([enc for enc, dec in layers])
        self.decoders = nn.ModuleList([dec for enc, dec in reversed(layers)])

    def encode(self, x):
        for i, enc in enumerate(self.encoders):
            x = enc(x)
        return x

    def decode(self, x):
        for i, dec in enumerate(self.decoders):
            x = dec(x)
        return x

    def forward(self, x):
        z = self.encode(x)
        y = self.decode(z)
        return y


class Model2(nn.Module):
    def __init__(self):
        super(Model2, self).__init__()
        layers = [
            make_heads(1,  8),
            make_pair(8,  8, 16),
            make_pair(16, 8, 32),

            make_pair(32,  16, 64),
            make_pair(64,  16, 128),
            make_tails(128, 1)
        ]
        self.encoders = nn.ModuleList([enc for enc, dec in layers])
        self.decoders = nn.ModuleList([dec for enc, dec in reversed(layers)])

    def encode(self, x):
        for i, enc in enumerate(self.encoders):
            x = enc(x)
        return x

    def decode(self, x):
        for i, dec in enumerate(self.decoders):
            x = dec(x)
        return x

    def forward(self, x):
        z = self.encode(x)
        y = self.decode(z)
        return y


class Model2Ex(nn.Module):
    def __init__(self):
        super().__init__()
        layers = [
            make_heads(1,  8, kernel_size=21),

            make_pair(8,  8, 16, kernel_size=9),
            make_pair(16, 8, 32, kernel_size=9),

            make_pair(32,  16, 64, kernel_size=9),
            make_pair(64,  16, 128, kernel_size=9),

            make_tails(128, 1)
        ]
        self.encoders = nn.ModuleList([enc for enc, dec in layers])
        self.decoders = nn.ModuleList([dec for enc, dec in reversed(layers)])

    def encode(self, x):
        for i, enc in enumerate(self.encoders):
            x = enc(x)
        return x

    def decode(self, x):
        for i, dec in enumerate(self.decoders):
            x = dec(x)
        return x

    def forward(self, x):
        z = self.encode(x)
        y = self.decode(z)
        return y


class Trainer(pl.LightningModule):
    def __init__(self, model_type, loss_args, optim_class, optim_args):
        super().__init__()
        loss = AstralLoss(**loss_args)
        self.name = model_type + '_' + loss.name + '_' + optim_class.__name__.lower()
        if model_type == 'medium':
            self.model = Model()
        elif model_type == 'light':
            self.model = Model2()
        elif model_type == 'light++':
            self.model = Model2Ex()
        else:
            raise ValueError(f"Unknown model type: {model_type}.")

        self.train_time_length = None
        self._window = None
        self.loss = loss
        self.optim_class = optim_class
        self.optim_args = optim_args

        self.metrics = nn.ModuleDict({
            'snr': audiometric.snr.SignalNoiseRatio(zero_mean=False),
            'sdr': audiometric.sdr.SignalDistortionRatio(use_cg_iter=10, zero_mean=False, load_diag=1e-8),
            'sisnr': audiometric.ScaleInvariantSignalNoiseRatio(),
            'sisdr': audiometric.ScaleInvariantSignalDistortionRatio(zero_mean=False)
        })

    def training_step(self, sample, batch_idx, logger=None):
        if logger is None:
            logger = self.log

        B, N = sample.a.shape

        if self.train_time_length is None:
            self.train_time_length = N
        elif self.train_time_length != N:
            raise RuntimeError("All training batches must "
                               "match on the time dimension!")

        # Get tensors from batched Sample.
        x_a = sample.a.to(device=self.device,
                          non_blocking=True).view((B, 1, N))
        x_b = sample.b.to(device=self.device,
                          non_blocking=True).view((B, 1, N))
        x_c = sample.c.to(device=self.device,
                          non_blocking=True).view((B, 1, N))

        x = torch.concat((x_a, x_b, x_c), dim=0)

        # Apply autoencoder.
        z = self.model.encode(x)
        y = self.model.decode(z)

        # Apply loss.
        l, lpack = self.loss(y * self.window, x * self.window)

        # Logging
        logger(self.name + "/train/loss", l)
        lpack = dict(lpack)
        for loss_name, loss_value in lpack.items():
            logger(self.name + "/train/loss/" + loss_name, loss_value.mean())

        return l

    def validation_step(self, sample, batch_idx, logger=None):
        if logger is None:
            logger = self.log

        B, N = sample.a.shape

        # Get data stored in Sample.
        x_a = sample.a.to(device=self.device,
                          non_blocking=True).view((B, 1, N))
        x_b = sample.b.to(device=self.device,
                          non_blocking=True).view((B, 1, N))
        x_c = sample.c.to(device=self.device,
                          non_blocking=True).view((B, 1, N))

        x = torch.concat((x_a, x_b, x_c), dim=0)
        del x_a
        del x_b
        del x_c

        # Apply autoencoder.
        z = self.model.encode(x)
        y = self.model.decode(z)

        # Calculate losses
        l, lpack = self.loss(y, x)

        # Calculate metrics
        for metric in self.metrics.values():
            metric.update(y, x)

        # Logging
        logger(self.name + "/val/loss", l, batch_size=B)
        lpack = dict(lpack)
        for loss_name, loss_value in lpack.items():
            logger(self.name + "/val/loss/" + loss_name,
                   loss_value.mean(), batch_size=B)
        for metric_name, metric in self.metrics.items():
            logger(self.name + "/val/metric/" + metric_name,
                   metric.compute(), batch_size=B)
            metric.reset()

    def visualize_step(self, sample, sample_idx, logger, step):
        latents = []
        preds = []
        for x in sample.get_batches():
            B, N = x.shape
            x = x.to(device=self.device,
                     non_blocking=True).view((B, 1, N))
            z = self.model.encode(x)
            y = self.model.decode(z)
            latents.append(z)
            preds.append(y)
        latent = torch.concat(latents).view((-1, 1))
        pred = torch.concat(preds).view((-1, 1))
        latent_sampling_rate = sample.sampling_rate // 16
        logger.experiment.add_audio(self.name + '/vis/latent/' + sample.name, latent, sample_rate=latent_sampling_rate, global_step=step)
        logger.experiment.add_audio(self.name + '/vis/' + sample.name, pred, sample_rate=sample.sampling_rate, global_step=step)

    @property
    def window(self):
        if self._window is None:
            N = self.train_time_length
            self._window = torch.ones(N, device=self.device)
            self._window[0:64] = 0
            self._window[N-64:N] = 0
        return self._window

    def configure_optimizers(self):
        optimizer = self.optim_class(self.parameters(), **self.optim_args)
        return optimizer
