from functorch.compile import memory_efficient_fusion
import torch.nn as nn
import torch
import speu


class CheckpointWrapper(speu.Module):
    def __init__(self, module):
        super(CheckpointWrapper, self).__init__()
        self.module = module
    
    def forward(self, *inputs):
        def _forward(*inputs):
            return self.module(*inputs)

        if self.training:
            # Apply checkpointing during training.
            return torch.utils.checkpoint.checkpoint(_forward, *inputs, preserve_rng_state=False)
        else:
            # Standard forward pass during inference.
            return self.module(*inputs)


class Encoder(speu.Module):
    def __init__(self,  input_channels, internal_channels,
                 output_channels,  scale=2):
        super(Encoder, self).__init__()
        self.conv1 = nn.Conv1d(input_channels, internal_channels,
                               9, bias=True, padding=4, padding_mode='reflect')
        self.activation1 = nn.PReLU(num_parameters=internal_channels)
        self.conv2 = nn.Conv1d(internal_channels, internal_channels,
                               9, bias=True, padding=4, padding_mode='reflect')
        self.activation2 = nn.PReLU(num_parameters=internal_channels)
        self.pointwise = nn.Conv1d(
            internal_channels, output_channels, 1, bias=True)
        self.activation_pointwise = nn.PReLU(num_parameters=output_channels)
        self.pool = nn.AvgPool1d(kernel_size=scale, stride=scale)

    def _forward(self, x):
        y = self.activation1(self.conv1(x))
        y = self.activation2(self.conv2(y))
        y = self.activation_pointwise(self.pointwise(y))
        y = self.pool(y)
        return y
    
    def forward(self, *inputs):
        if self.training:
            # Apply checkpointing during training.
            return torch.utils.checkpoint.checkpoint(self._forward, *inputs, preserve_rng_state=False)
        else:
            # Standard forward pass during inference.
            return self._forward(*inputs)


class Decoder(speu.Module):
    def __init__(self,  input_channels, internal_channels,
                 output_channels,  scale=2):
        super(Decoder, self).__init__()
        self.upsample = nn.Upsample(scale_factor=scale, mode='nearest')
        self.upsample_conv = nn.Conv1d(input_channels, input_channels, 3, groups=input_channels // 4, bias=False, padding=1, padding_mode='reflect')
        self.pointwise = nn.Conv1d(input_channels, internal_channels, 1, bias=True)
        self.activation_pointwise = nn.PReLU(num_parameters=internal_channels)
        self.conv1 = nn.Conv1d(internal_channels, internal_channels,
                               9, bias=True, padding=4, padding_mode='reflect')
        self.activation1 = nn.PReLU(num_parameters=internal_channels)
        self.conv2 = nn.Conv1d(internal_channels, output_channels,
                               9, bias=True, padding=4, padding_mode='reflect')
        self.activation2 = nn.PReLU(num_parameters=output_channels)

    def _forward(self, x):
        y = self.upsample_conv(self.upsample(x))
        y = self.activation_pointwise(self.pointwise(y))
        y = self.activation1(self.conv1(y))
        y = self.activation2(self.conv2(y))
        return y

    def forward(self, *inputs):
        if self.training:
            # Apply checkpointing during training.
            return torch.utils.checkpoint.checkpoint(self._forward, *inputs, preserve_rng_state=False)
        else:
            # Standard forward pass during inference.
            return self._forward(*inputs)


def make_pair(input_channels, internal_channels,
              output_channels,  scale=2):
    encoder = Encoder(input_channels, internal_channels, output_channels, scale)
    decoder = Decoder(output_channels, internal_channels, input_channels, scale)
    return (encoder, decoder)

def make_heads(input_channels, output_channels):
    return (
        nn.Conv1d(input_channels, output_channels, 21, padding=10, padding_mode='reflect'),
        nn.Conv1d(output_channels, input_channels, 21, padding=10, padding_mode='reflect')
    )

class Model(speu.models.BaseModel):
    def __init__(self):
        super(Model, self).__init__()
        layers = [
            make_heads( 1,  12),
            make_pair( 12,  32,  24),
            make_pair( 24,  64,  48),

            make_pair( 48,  96,  64),
            make_pair( 64, 128,  96),
            make_pair( 96, 160, 128),

            make_pair(128, 192, 128),
            make_pair(128, 256, 192),
            make_pair(192, 320, 256),

            make_pair(256, 384, 320),
            make_pair(320, 448, 384),
            make_pair(384, 512, 256)
        ]
        self.encoders = nn.ModuleList([enc for enc, dec in layers])
        self.decoders = nn.ModuleList([dec for enc, dec in reversed(layers)])

    def encode(self, x):
        for i, enc in enumerate(self.encoders):
            x = enc(x)
        return x

    def decode(self, x):
        for i, dec in enumerate(self.decoders):
            x = dec(x)
        return x

    def forward(self, x):
        z = self.encode(x)
        y = self.decode(z)
        return y
