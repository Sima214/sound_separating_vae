from asteroid_filterbanks import ParamSincFB, AnalyticFreeFB
from asteroid_filterbanks import Encoder as EncoderFB, Decoder as DecoderFB
from models.common import get_padding_for_same_conv, get_config_hash_string, musdb18hq_triplet_sample_to_batch
from models.common import Scaler, Conv1DActivation, ConvTranspose1DActivation, CheckpointableModule
from models.loss import AstralLoss
from typing import Union, List

import torchmetrics.audio as audiometric
import pytorch_lightning as pl
import torch.nn.functional as F
import torch.nn.init as init
import torch.nn as nn
import torch
import math


class EncoderInputCodec(CheckpointableModule):
    __constants__ = ['input_channels', 'hidden_channels', 'output_channels',
                     'scale']

    def __init__(self, in_ch, hd_ch, out_ch, kernel_size=31, scale=1, checkpointed=False):
        super().__init__(checkpointed=checkpointed)
        self.input_channels = in_ch
        self.hidden_channels = hd_ch
        self.output_channels = out_ch
        self.scale = scale

        # Filters are shared for each input channel.
        fb = ParamSincFB(hd_ch, kernel_size, stride=scale,
                         sample_rate=48000, min_low_hz=0, min_band_hz=8)
        fb_padding = get_padding_for_same_conv(kernel_size, stride=scale)
        self.filter_pad = nn.ReflectionPad1d(fb_padding)
        self.filter = EncoderFB(fb)

        # Combine shared channels.
        self.bottleneck = Conv1DActivation(
            hd_ch * in_ch, out_ch, 1,
            activation=nn.PReLU, num_parameters=out_ch, init=1e-2
        )
        init.kaiming_normal_(self.bottleneck.conv.weight, a=1e-2)

    def _forward(self, x: torch.Tensor):
        B, C, N = x.shape
        assert C == self.input_channels
        x = x.view((B*C, 1, N))

        x = self.filter_pad(x)
        x = self.filter(x)
        x = x.view((B, self.hidden_channels * C, N // self.scale))

        x = self.bottleneck(x)
        return x


class EncoderStratumBlock(CheckpointableModule):
    def __init__(self, in_ch: int, feats: List[int], dilations: List[int], kernel_size=9, checkpointed=False, out_ch=None):
        assert len(feats) == len(dilations)
        if out_ch is None:
            out_ch = in_ch
        super().__init__(checkpointed=checkpointed)

        self.filters = nn.ModuleList((Conv1DActivation(in_ch, feats[i], kernel_size, dilation=dilations[i],
                                                       padding='same', padding_mode='reflect',
                                                       activation=nn.PReLU, num_parameters=feats[i], init=0.1)
                                      for i in range(len(feats))))
        for cva in self.filters:
            init.kaiming_normal_(cva.conv.weight, a=0.1)

        self.bottleneck = Conv1DActivation(
            sum(feats), out_ch, 1,
            activation=nn.PReLU, num_parameters=out_ch, init=1e-2
        )
        init.kaiming_normal_(self.bottleneck.conv.weight, a=1e-2)

        self.scaler = Scaler(init=1/3)

    def _forward(self, x: torch.Tensor):
        fts = [cva(x) for cva in self.filters]
        x = torch.cat(fts, dim=1)
        x = self.bottleneck(x)
        x = self.scaler(x)
        return x


class EncoderDownsampleExtendBlock(nn.Module):
    __constants__ = ['scale', 'pad_channels']

    def __init__(self, in_ch: int, out_ch: int, scale: int):
        super().__init__()
        assert out_ch >= in_ch
        self.scale = scale
        self.pad_channels = out_ch - in_ch

    def forward(self, x: torch.Tensor):
        if self.scale != 1:
            scaled = F.interpolate(x, scale_factor=1/self.scale, mode='linear',
                                   align_corners=True, recompute_scale_factor=True, antialias=False)
        else:
            scaled = x

        if self.pad_channels != 0:
            padded = F.pad(scaled, pad=(0, 0, 0, self.pad_channels, 0, 0),
                           mode='constant', value=0)
        else:
            padded = scaled

        return padded, scaled


class EncoderInputBlock(CheckpointableModule):
    def __init__(self, in_ch, hd_ch, out_ch, scale,
                 stratum_repeats, stratum_feats, stratum_dilations,
                 stratum_kernel_size=9, codec_kernel_size=31,
                 codec_checkpointed=False, stratum_checkpointed=False, checkpointed=False):
        super().__init__(checkpointed=checkpointed)

        self.codec = EncoderInputCodec(in_ch, hd_ch, out_ch,
                                       codec_kernel_size, scale, codec_checkpointed)

        self.stack = nn.ModuleList((EncoderStratumBlock(out_ch, stratum_feats, stratum_dilations,
                                                        stratum_kernel_size, stratum_checkpointed)
                                    for i in range(stratum_repeats)))

    def _forward(self, x: torch.Tensor):
        x = self.codec(x)
        for i, stratum in enumerate(self.stack):
            r = stratum(x)
            x = x + r
        return x


class EncoderBlock(CheckpointableModule):
    def __init__(self, input_channels: int, output_channels: int, output_scale: int, stratum_repeats: int,
                 stratum_feats: List[int], stratum_dilations: List[int], stratum_kernel_size=9,
                 stratum_checkpointed=False, checkpointed=False):
        assert stratum_repeats >= 1
        super().__init__(checkpointed=checkpointed)

        self.sampler = EncoderDownsampleExtendBlock(
            input_channels, output_channels, output_scale)

        self.stack = nn.ModuleList([EncoderStratumBlock(input_channels, stratum_feats, stratum_dilations,
                                                        stratum_kernel_size, stratum_checkpointed, output_channels)])
        self.stack.extend((EncoderStratumBlock(output_channels, stratum_feats, stratum_dilations,
                                               stratum_kernel_size, stratum_checkpointed)
                           for i in range(stratum_repeats - 1)))

    def _forward(self, x: torch.Tensor):
        x, unpadded = self.sampler(x)
        for i, stratum in enumerate(self.stack):
            r = stratum(x if i != 0 else unpadded)
            x = x + r
        return x


class Encoder(nn.ModuleList):
    def __init__(self, checkpointing=True):
        super().__init__()
        self.append(EncoderInputBlock(1, 128, 32, 4, 3, (24, 24, 16), (1, 2, 3),  # x8
                                      codec_checkpointed=checkpointing, stratum_checkpointed=checkpointing))
        self.append(EncoderBlock(32, 64, 4, 3, (48, 48, 32), (1, 2, 3),  # x4
                                 stratum_checkpointed=checkpointing))
        self.append(EncoderBlock(64, 128, 4, 3, (96, 96, 64), (1, 2, 3),  # x2
                                 stratum_checkpointed=checkpointing))
        self.append(EncoderBlock(128, 256, 4, 3, (192, 192, 128), (1, 2, 3),  # x1
                                 stratum_checkpointed=checkpointing))

    def forward(self, x: torch.Tensor):
        for i, m in enumerate(self):
            x = m(x)
        return x


class DecoderReconstructionBlock(CheckpointableModule):
    def __init__(self, in_ch, out_ch, scale, feats, dilations, kernel_size=9, checkpointed=False):
        assert len(feats) == len(dilations)
        super().__init__(checkpointed=checkpointed)

        self.filters = nn.ModuleList((ConvTranspose1DActivation(in_ch, feats[i], kernel_size,
                                                                stride=scale, dilation=dilations[i], output_padding=1,
                                                                padding=get_padding_for_same_conv(
                                                                    kernel_size, scale, dilations[i]),
                                                                activation=nn.PReLU, num_parameters=feats[i], init=0.1)
                                      for i in range(len(feats))))
        for cva in self.filters:
            init.kaiming_normal_(cva.conv.weight, a=0.1)

        self.bottleneck = Conv1DActivation(
            sum(feats), out_ch, 1,
            activation=nn.PReLU, num_parameters=out_ch, init=1e-2
        )
        init.kaiming_normal_(self.bottleneck.conv.weight, a=1e-2)

    def _forward(self, x: torch.Tensor):
        fts = [cva(x) for cva in self.filters]
        x = torch.cat(fts, dim=1)
        x = self.bottleneck(x)
        return x


class DecoderDenoiserStratumBlock(CheckpointableModule):
    def __init__(self, in_ch: int, feats: List[int], dilations: List[int], kernel_size=9, checkpointed=False, out_ch=None):
        assert len(feats) == len(dilations)
        if out_ch is None:
            out_ch = in_ch
        super().__init__(checkpointed=checkpointed)

        self.filters = nn.ModuleList((Conv1DActivation(in_ch, feats[i], kernel_size, dilation=dilations[i],
                                                       padding='same', padding_mode='reflect',
                                                       activation=nn.PReLU, num_parameters=feats[i], init=0.1)
                                      for i in range(len(feats))))
        for cva in self.filters:
            init.kaiming_normal_(cva.conv.weight, a=0.1)

        self.bottleneck = Conv1DActivation(
            sum(feats), out_ch, 1,
            activation=nn.PReLU, num_parameters=out_ch, init=1e-2
        )
        init.kaiming_normal_(self.bottleneck.conv.weight, a=1e-2)

        self.scaler = Scaler(init=1/3)

    def _forward(self, x: torch.Tensor):
        fts = [cva(x) for cva in self.filters]
        x = torch.cat(fts, dim=1)
        x = self.bottleneck(x)
        x = self.scaler(x)
        return x


class DecoderUpsampleBottleneckBlock(CheckpointableModule):
    def __init__(self, in_ch, out_ch, scale, checkpointed=False):
        super().__init__(checkpointed=checkpointed)

        self.scaler = nn.Upsample(scale_factor=scale, mode='nearest',
                                  recompute_scale_factor=True)

        self.bottleneck = nn.Conv1d(in_ch, out_ch, 1)
        init.kaiming_normal_(self.bottleneck.weight, a=math.sqrt(5))

    def _forward(self, x: torch.Tensor):
        x = self.scaler(x)
        x = self.bottleneck(x)
        return x


class DecoderDenoiserBlock(CheckpointableModule):
    def __init__(self, in_ch, out_ch, scale, stratum_repeats,
                 stratum_feats, stratum_dilations, stratum_kernel_size=9,
                 stratum_checkpointed=False, sampler_checkpointed=False, checkpointed=False):
        super().__init__(checkpointed=checkpointed)

        self.stack = nn.ModuleList((
            DecoderDenoiserStratumBlock(in_ch, stratum_feats,
                                        stratum_dilations, stratum_kernel_size,
                                        checkpointed=stratum_checkpointed)
            for i in range(stratum_repeats)
        ))
        self.sampler = DecoderUpsampleBottleneckBlock(
            in_ch, out_ch, scale, sampler_checkpointed)

    def _forward(self, x: torch.Tensor):
        for i, stratum in enumerate(self.stack):
            r = stratum(x)
            x = x + r
        x = self.sampler(x)
        return x


class DecoderOutputCodec(CheckpointableModule):
    __constants__ = ['input_channels', 'hidden_channels',
                     'output_channels', 'scale']

    def __init__(self, in_ch, hd_ch, out_ch, scale, kernel_size=31, checkpointed=False):
        super().__init__(checkpointed=checkpointed)
        self.input_channels = in_ch
        self.hidden_channels = hd_ch
        self.output_channels = out_ch
        self.scale = scale

        self.expansion = Conv1DActivation(
            in_ch, hd_ch * out_ch, 1,
            activation=nn.Identity
        )
        init.kaiming_normal_(self.expansion.conv.weight, a=1e-2)

        fb = ParamSincFB(hd_ch, kernel_size, stride=scale,
                         sample_rate=48000, min_low_hz=0, min_band_hz=8)
        fb_padding = get_padding_for_same_conv(kernel_size, stride=scale)
        self.filter = DecoderFB(fb, padding=fb_padding,
                                output_padding=scale - 1)

    def _forward(self, x: torch.Tensor):
        # B, Cin, N -> B, Cout, Chidden, N
        B, C, N = x.shape
        x = self.expansion(x)
        x = x.view((B, self.output_channels, self.hidden_channels, N))

        # B, Cout, Chidden, N -> B, Cout, N
        x = self.filter(x)

        return x


class DecoderDenoiserOutputBlock(CheckpointableModule):
    def __init__(self, in_ch, hd_ch, out_ch, scale, stratum_repeats,
                 stratum_feats, stratum_dilations, stratum_kernel_size=9, stratum_checkpointed=False,
                 codec_kernel_size=31, codec_checkpointed=False, checkpointed=False):
        super().__init__(checkpointed=checkpointed)
        self.stack = nn.ModuleList((
            DecoderDenoiserStratumBlock(in_ch, stratum_feats,
                                        stratum_dilations, stratum_kernel_size,
                                        checkpointed=stratum_checkpointed)
            for i in range(stratum_repeats)
        ))
        self.codec = DecoderOutputCodec(in_ch, hd_ch, out_ch, scale,
                                        codec_kernel_size, codec_checkpointed)

    def _forward(self, x: torch.Tensor):
        for i, stratum in enumerate(self.stack):
            r = stratum(x)
            x = x + r
        x = self.codec(x)
        return x


class Decoder(nn.Module):
    def __init__(self, checkpointing=True):
        super().__init__()

        self.reconstructors = nn.ModuleList()
        self.reconstructors.append(DecoderReconstructionBlock(256, 192, 2, (144, 144, 96), (1, 2, 3),  # x1.5
                                                              checkpointed=checkpointing))
        self.reconstructors.append(DecoderReconstructionBlock(192, 128, 2, (96, 96, 64), (1, 2, 3),  # x2
                                                              checkpointed=checkpointing))
        self.reconstructors.append(DecoderReconstructionBlock(128, 96, 2, (72, 72, 48), (1, 2, 3),  # x3
                                                              checkpointed=checkpointing))
        self.reconstructors.append(DecoderReconstructionBlock(96, 64, 2, (48, 48, 32), (1, 2, 3),  # x4
                                                              checkpointed=checkpointing))

        self.denoisers = nn.ModuleList()
        self.denoisers.append(DecoderDenoiserBlock(64, 32, 4, 5, (48, 48, 32), (1, 2, 3),
                                                   sampler_checkpointed=checkpointing,
                                                   stratum_checkpointed=checkpointing))
        self.denoisers.append(DecoderDenoiserOutputBlock(32, 128, 1, 4, 3, (24, 24, 16), (1, 2, 3),
                                                         codec_checkpointed=checkpointing,
                                                         stratum_checkpointed=checkpointing))

    def forward(self, x: torch.Tensor):
        for recon in self.reconstructors:
            x = recon(x)
        # Bx64x1024
        for denoiser in self.denoisers:
            x = denoiser(x)
        return x


class AETrainer(pl.LightningModule):
    def __init__(self, model_name, model_seed=None, checkpointing=False,
                 loss_args={}, optim_class=torch.optim.SGD, optim_args={}):
        super().__init__()
        # Build model.
        self._name = model_name
        old_trng_state = torch.get_rng_state()
        if model_seed is not None:
            self.seed = model_seed
            torch.manual_seed(model_seed)
        else:
            self.seed = torch.seed()
        self.encoder = Encoder(checkpointing)
        self.decoder = Decoder(checkpointing)
        torch.set_rng_state(old_trng_state)

        # Store objects used for training.
        self.loss = AstralLoss(**loss_args)
        self.optim_class = optim_class
        self.optim_args = optim_args

        self.metrics = nn.ModuleDict({
            'snr': audiometric.snr.SignalNoiseRatio(zero_mean=False),
            'sdr': audiometric.sdr.SignalDistortionRatio(use_cg_iter=10, zero_mean=False, load_diag=1e-8),
            'sisnr': audiometric.ScaleInvariantSignalNoiseRatio(),
            'sisdr': audiometric.ScaleInvariantSignalDistortionRatio(zero_mean=False)
        })

    def training_step(self, sample, batch_idx, logger=None):
        if logger is None:
            logger = self.log
        # Get tensors from batched Sample.
        x = musdb18hq_triplet_sample_to_batch(sample, self.device)
        # Apply autoencoder.
        z = self.encoder(x)
        y = self.decoder(z)
        # Apply loss.
        l, lpack = self.loss(y, x)
        # Logging
        logger(self.name + "/train/loss", l)
        lpack = dict(lpack)
        for loss_name, loss_value in lpack.items():
            logger(self.name + "/train/loss/" + loss_name, loss_value.mean())
        return l

    def validation_step(self, sample, batch_idx, logger=None):
        if logger is None:
            logger = self.log
        # Get tensors from batched Sample.
        x = musdb18hq_triplet_sample_to_batch(sample, self.device)
        B, C, N = x.shape
        # Apply autoencoder.
        z = self.encoder(x)
        y = self.decoder(z)
        # Calculate losses
        l, lpack = self.loss(y, x)
        # Calculate metrics
        for metric in self.metrics.values():
            metric.update(y, x)

        # Logging
        logger(self.name + "/val/loss", l, batch_size=B)
        lpack = dict(lpack)
        for loss_name, loss_value in lpack.items():
            logger(self.name + "/val/loss/" + loss_name,
                   loss_value.mean(), batch_size=B)
        for metric_name, metric in self.metrics.items():
            logger(self.name + "/val/metric/" + metric_name,
                   metric.compute(), batch_size=B)
            metric.reset()

    def visualize_step(self, sample, sample_idx, logger, step):
        latents = []
        preds = []
        for x in sample.get_batches():
            # Get batch.
            B, N = x.shape
            x = x.to(device=self.device,
                     non_blocking=True).view((B, 1, N))
            # Apply autoencoder.
            z = self.encoder(x)
            y = self.decoder(z)
            # Append results
            latents.append(z)
            preds.append(y)

        # Combine results.
        # latent = torch.concat(latents).view((-1, 1))
        pred = torch.concat(preds).view((-1, 1))
        # Log results.
        latent_sampling_rate = sample.sampling_rate // 256
        # logger.experiment.add_audio(self.name + '/vis/latent/' + sample.name,
        #                             latent, sample_rate=latent_sampling_rate, global_step=step)
        logger.experiment.add_audio(self.name + '/vis/' + sample.name,
                                    pred, sample_rate=sample.sampling_rate, global_step=step)

    def configure_optimizers(self):
        optimizer = self.optim_class(self.parameters(), **self.optim_args)
        return optimizer

    def compile(self):
        self.encoder = torch.compile(self.encoder, backend="inductor")
        self.decoder = torch.compile(self.decoder, backend="inductor")
        # self.loss = torch.compile(self.loss, backend="inductor")

    @property
    def optimizer_name(self):
        return self.optim_class.__name__.lower() + '#' + get_config_hash_string(self.optim_args)

    @property
    def name(self):
        model_name = self._name + '#' + f"{self.seed:#0{18}x}"
        loss_name = self.loss.name
        optim_name = self.optimizer_name
        return model_name + '_' + loss_name + '_' + optim_name
