from asteroid.losses.multi_scale_spectral import SingleSrcMultiScaleSpectral
from asteroid.losses.sdr import SingleSrcNegSDR
from collections import namedtuple
from models.common import get_config_hash_string
import torch.nn as nn


class AstralLoss(nn.Module):

    def __init__(self, a, b, b_a=1, c=0, norm_type='l2', reduction='mean'):
        super().__init__()
        self.reduction = reduction
        if a != 0:
            self.sdr_factor = a
            self.sdr = SingleSrcNegSDR('sisdr', zero_mean=False, reduction='none')
        else:
            self.sdr_factor = None

        if b != 0:
            self.sftp_factor = b
            self.sftp = SingleSrcMultiScaleSpectral(
                n_filters=   [8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16],
                windows_size=[8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16],
                hops_size=   [4096, 2048, 1024,  512, 256, 128,  64, 32, 16,  8],
                alpha=b_a
            )
        else:
            self.sftp_factor = None

        if c != 0:
            self.norm_factor = c
            if norm_type == 'l2' or norm_type == 'mse':
                self.norm = nn.MSELoss(reduction='none')
            elif norm_type == 'l1':
                self.norm = nn.L1Loss(reduction='none')
            else:
                raise ValueError(f"Unknown type string for norm loss: {norm_type}.")
        else:
            self.norm_factor = None

    def forward(self, x, y):
        B, C, N = y.shape

        x = x.view((B, N))
        y = y.view((B, N))

        loss = None
        extra = {}

        if self.sdr_factor is not None:
            l_sdr = self.sdr(x, y)
            extra['sdr'] = l_sdr
            if loss is None:
                loss = l_sdr * self.sdr_factor
            else:
                loss += l_sdr * self.sdr_factor

        if self.sftp_factor is not None:
            l_sftp = self.sftp(x, y) / 2e3
            extra['sftp'] = l_sftp
            if loss is None:
                loss = l_sftp * self.sftp_factor
            else:
                loss += l_sftp * self.sftp_factor

        if self.norm_factor is not None:
            l_norm = self.norm(x, y).mean(dim=-1) * 100
            extra['norm'] = l_norm
            if loss is None:
                loss = l_norm * self.norm_factor
            else:
                loss += l_norm * self.norm_factor

        if self.reduction == 'mean':
            return loss.mean(), extra
        elif self.reduction == 'sum':
            return loss.sum(), extra
        else: # none
            return loss, extra

    @property
    def config(self):
        conf = {}
        if self.sdr_factor is not None:
            conf['sdr'] = self.sdr_factor
        if self.sftp_factor is not None:
            conf['sftp'] = self.sftp_factor
            conf['sftp_alpha'] = self.sftp.alpha
        if self.norm_factor is not None:
            conf['norm'] = self.norm_factor
            conf['norm_class'] = type(self.norm).__name__
        return conf

    @property
    def name(self):
        return f"astral#{get_config_hash_string(self.config)}"


class VaeAstralLoss(AstralLoss):

    def __init__(self, a, b, b_a=1, c=0, norm_type='l2', k=1, reduction='mean'):
        super().__init__(a, b, b_a, c, norm_type, reduction='none')
        self.vae_reduction = reduction
        if k != 0:
            self.kld_factor = k
        else:
            self.kld_factor = None

    def forward(self, x, y):
        loss, extra = super().forward(x, y)

        if self.vae_reduction == 'mean':
            return loss.mean(), extra
        elif self.vae_reduction == 'sum':
            return loss.sum(), extra
        else: # none
            return loss, extra

    @property
    def name(self):
        s = super().name
        if self.kld_factor is not None:
            s += ("_kld_(%.2f)" % (self.kld_factor)).replace('.', '_')
        return s
    

class CompoundLoss(nn.Module):
    def __init__(self, losses):
        super().__init__()
        self.losses = []
        self.factors = []
        for loss, factor in losses:
            self.losses.append(loss)
            self.factors.append(factor)
        self.losses = nn.ModuleList(self.losses)
    
    def forward(self, x, y):
        l = None
        for loss, factor in zip(self.losses, self.factors):
            if l is None:
                l = factor * loss(x, y)
            else:
                l += factor * loss(x, y)
        return l
