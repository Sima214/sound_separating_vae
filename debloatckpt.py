import torch
import sys


def debloat_state(full_state):
    state_dict = full_state['state_dict']
    keys_to_del = []
    for key in state_dict.keys():
        if '.sftp.encoders.' in key:
            keys_to_del.append(key)
    for key in keys_to_del:
        del state_dict[key]


if __name__ == '__main__':
    path = sys.argv[1]
    full_state = torch.load(path, map_location='cpu')
    debloat_state(full_state)
    torch.save(full_state, path)
